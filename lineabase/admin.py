# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from .models import *
from .forms import EntrevistadosForm, EntrevistadoForm
import nested_admin

from import_export.admin import ImportExportModelAdmin

class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'Perfil usuario'

class UserAdmin(BaseUserAdmin):
    inlines = (UserProfileInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

class EncuestadoresAdmin(admin.ModelAdmin):
    search_fields = ['nombre']

class EntrevistadosAdmin(ImportExportModelAdmin):
    form = EntrevistadosForm
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(EntrevistadosAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(EntrevistadosAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    list_display = ('nombre', 'sexo', 'pais', 'organizacion_pertenece','organizacion_apoyo', 'user')
    list_filter = ('pais','organizacion_pertenece', 'organizacion_apoyo')
    search_fields = ['nombre','id',]

class InlineComposicionFamilia(nested_admin.NestedStackedInline):
    model = ComposicionFamilia
    fields = (('varones_adultos', 'mujeres_adultas','varones_jovenes','mujeres_jovenes'),
                     ('ninos','ninas','viven_finca','viven_pueblo'),
                     ('viven_ciudad','viven_finca_casa', 'permanentes_hombre', 'permanentes_mujeres'),
                     ('temporal_hombre','temporal_mujeres','tecnicos_hombre','tecnicos_mujeres'),
                     'nivel_educacion',
                     )
    extra = 1
    max_num = 1

class InlineServiciosFinca(nested_admin.NestedTabularInline):
    model = ServiciosFinca
    extra = 1
    max_num = 1

class InlineTenencia(nested_admin.NestedTabularInline):
    model = Tenencia
    extra = 1
    max_num = 1

class InlineSeguridadAlimentario(nested_admin.NestedTabularInline):
    model = SeguridadAlimentario
    extra = 1
    max_num = 1

class InlineUsoTierra(nested_admin.NestedTabularInline):
    model = UsoTierra
    extra = 1
    max_num = 8

class InlineRubrosFinca(nested_admin.NestedTabularInline):
    model = RubrosFinca
    extra = 1

class InlineAreasCacao(nested_admin.NestedTabularInline):
    model = AreasCacao
    extra = 1

class InlineProduccionCacao(nested_admin.NestedTabularInline):
    model = ProduccionCacao
    extra = 1

class InlineFactores(nested_admin.NestedTabularInline):
    model = Factores
    extra = 1
    max_num = 1

class InlineLaProduccion(nested_admin.NestedTabularInline):
    model = LaProduccion
    extra = 1
    max_num = 1

class InlineCalidadManejo(nested_admin.NestedTabularInline):
    model = CalidadManejo
    extra = 1
    max_num = 1

class InlineNivelManejo(nested_admin.NestedTabularInline):
    model = NivelManejo
    extra = 1
    max_num = 1

class InlineMesLabores(nested_admin.NestedTabularInline):
    model = MesLabores
    extra = 1
    max_num = 1

class InlineRealizanLabores(nested_admin.NestedTabularInline):
    model = RealizanLabores
    extra = 1
    max_num = 1

class InlineOpcionesAgroecologicas(nested_admin.NestedTabularInline):
    model = OpcionesAgroecologicas
    extra = 1

class InlineCosecha(nested_admin.NestedTabularInline):
    model = Cosecha
    extra = 1
    max_num = 1

class InlineOpcionesCadena(nested_admin.NestedTabularInline):
    model = OpcionesCadena
    extra = 1
    max_num = 1

class InlineComercializacion(nested_admin.NestedTabularInline):
    model = Comercializacion
    extra = 1
    max_num = 3

class InlineCredito(nested_admin.NestedTabularInline):
    model = Credito
    extra = 1
    max_num = 1

class InlineObtieneCredito(nested_admin.NestedTabularInline):
    model = ObtieneCredito
    extra = 1
    max_num = 1

class InlineMitigacionRiesgo(nested_admin.NestedStackedInline):
    model = MitigacionRiesgo
    extra = 1
    max_num = 1

class InlineCapacitacionTecnicas(nested_admin.NestedTabularInline):
    model = CapacitacionTecnicas
    extra = 1

class InlineCapacitacionSocial(nested_admin.NestedTabularInline):
    model = CapacitacionSocial
    extra = 1

class InlineDetallePlantios(nested_admin.NestedStackedInline):
    model = DetallePlantios
    fields = ('nombre_plantio',('area','unidad'),('tipo_planta','edad'),'variedades',
                    ('produccion','unidad_pro'),'nivel',('tipo_sombra','nivel_sombra',
                    'tipo_fertilizacion','tipo_fungicida'),#'area_ha','produccion_kg','rendimientos',
                  )
    extra = 1

class InlineDetalleSeleccion(nested_admin.NestedStackedInline):
    model = DetalleSeleccion
    extra = 1

class EncuestaAdmin(nested_admin.NestedModelAdmin):
    form = EntrevistadoForm
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(EncuestaAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(EncuestaAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user',)
    list_display = ('entrevistado', 'fecha','get_sexo' ,'get_pais','encuestador',
                            'get_organizacion_pertenece', 'get_organizacion_apoya','user')
    search_fields = ['entrevistado__nombre']
    list_filter = ['year','entrevistado__organizacion_apoyo','entrevistado__pais']
    inlines = [InlineComposicionFamilia,InlineServiciosFinca,InlineTenencia,
                 InlineSeguridadAlimentario,InlineUsoTierra,InlineRubrosFinca,
                 InlineAreasCacao,InlineProduccionCacao,InlineFactores,InlineLaProduccion,
                 InlineCalidadManejo,InlineNivelManejo,InlineMesLabores,InlineRealizanLabores,
                 InlineOpcionesAgroecologicas,InlineCosecha,InlineOpcionesCadena,InlineComercializacion,
                 InlineCredito,InlineObtieneCredito,InlineMitigacionRiesgo,InlineCapacitacionTecnicas,
                 InlineCapacitacionSocial,InlineDetallePlantios,InlineDetalleSeleccion]

    def get_pais(self, obj):
        return obj.entrevistado.pais
    get_pais.short_description = 'Pais'
    get_pais.admin_order_field = 'entrevistado__pais'

    def get_sexo(self, obj):
        return obj.entrevistado.get_sexo_display()
    get_sexo.short_description = 'Sexo'
    get_sexo.admin_order_field = 'entrevistado__sexo'

    def get_organizacion_pertenece(self, obj):
        return obj.entrevistado.organizacion_pertenece
    get_organizacion_pertenece.short_description = 'Organizacion que pertenece'
    get_organizacion_pertenece.admin_order_field = 'entrevistado__organizacion_pertenece'

    def get_organizacion_apoya(self, obj):
        return obj.entrevistado.organizacion_apoyo
    get_organizacion_apoya.short_description = 'Organizacion que apoya'
    get_organizacion_apoya.admin_order_field = 'entrevistado__organizacion_apoyo'
# Register your models here.
admin.site.register(Entrevistados, EntrevistadosAdmin)
admin.site.register(Encuestadores, EncuestadoresAdmin)
admin.site.register(Organizacion)
admin.site.register(Encuesta, EncuestaAdmin)
admin.site.register(Proyectos)
admin.site.register(Rubros)
admin.site.register(OrganizacionApoyo)
