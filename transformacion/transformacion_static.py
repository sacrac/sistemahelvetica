# -*- coding: utf-8 -*-

CHOICE_UNIDAD_TIERRA = (
                (1, 'Mz'),
                (2, 'Ha'),
                (3, 'Tarea DOM'),
                (4, 'Mt2'),
                (5, 'Tarea GUATE'),
              )

CHOICE_TIPO_PUNTO = (
    (1, 'Perennifolia'),
    (2, 'Caducifolia'),
)

CHOICE_TIPO_USO_PUNTO = (
    (1, 'Leña'),
    (2, 'Fruta'),
    (3, 'Madera'),
    (4, 'Sombra'),
    (5, 'Nutrientes'),
)

CHOICE_TIPO_ESPECIES_SECUNDARIAS = (
    (1, 'Cacao'),
    (2, 'Musaceas'),
    (3, 'Frutales'),
    (4, 'Maderables'),
    (5, 'Servicios'),
)

UNIDAD_ESPECIES_COSECHA = (
    (1, 'Racimo'),
    (2, 'Número'),
    (3, 'cm'),
    (4, 'Cabeza'),
    (5, 'Docena'),
    (6, 'm3'),
    (7,'pt'),
    (8,'Unidad')
)

CHOICE_MADERABLES_SERVICIOS_COSECHA_SECUNDARIAS = (
    (1, 'Cedro DAP cm'),
    (2, 'Caoba DAP cm'),
    (3, 'Laurel DAP cm'),
    (4, 'Funera DAP cm'),
    (5, 'Balsamo DAP cm'),
    (6, 'Conacaste DAP cm'),
    (7, 'Cortez blanco DAP cm'),
    (8, 'Inga/Guaba DAP cm'),
    (9, 'Madrecacao DAP cm'),
)

CHOICE_TIPO_COPA_PUNTO = (
    (1, 'Copa ancha'),
    (2, 'Copa media'),
    (3, 'Copa angosta'),

)

CHOICE_SI_NO = (
    (1, 'Si'),
    (2, 'No'),
)

CHOICE_PRODUCCION = (
    (1, 'Alta'),
    (2, 'Media'),
    (3, 'Baja'),
)

CHOICE_PLANTAS1 = (
    (1, 'Altura en mt'),
    (2, 'Ancho de copa mt'),
    (3, 'Largo de madera productiva mt'),
)

CHOICE_PLANTAS2 = (
    (1, 'Formación de horqueta'),
    (2, 'Ramas en contacto '),
    (3, 'Ramas entrecruzadas'),
    (4, 'Ramas cercanas al suelo'),
    (5, 'Chupones'),
    (6, 'Penetración de Luz'),
    )

CHOICE_PLANTAS3 = (
    (1, 'Nivel de producción'),
)

CHOICE_ACCIONES_SOMBRA = (
    (1, 'Reducir la sombra'),
    (2, 'Aumentar la sombra'),
    (3, 'Ninguna'),
)

CHOICE_PODA = (
    (1, 'Si'),
    (2, 'No'),
)

CHOICE_TODO = (
    (1, 'En todo la parcela '),
    (2, 'Solo en una parte de la parcela'),
)

CHOICES_PROBLEMA_PLANTA = (('A', 'Altura'),
                           ('B', 'Ancho'),
                           ('C', 'Ramas'),
                           ('D', 'Horqueta'),
                           ('E', 'Chupones'),
                           ('F', 'Poca entrada de Luz'),
                           ('G', 'Baja productividad'),
                           ('H', 'Ninguna'),
                           )
CHOICES_TIPO_PODA = (('A', 'Poda de copa'),
                     ('B', 'Poda de ramas'),
                     ('C', 'Ramas'),
                     ('D', 'Formar horquetas'),
                     ('E', 'Deschuponar'),
                     ('F', 'Ninguna'),
                     )

CHOICE_REALIZA_PODA = (
    (1, 'En toda la parcela'),
    (2, 'En Varios partes'),
    (3, 'En algunas partes'), )
CHOICE_VIGOR = (
    (1, 'Todas'),
    (2, 'Algunas'),
    (3, 'Ninguna'), )
CHOICE_ENTRADA_LUZ = (
    (1, 'Poda de copa'),
    (2, 'Quitar ramas entrecruzadas'),
    (3, 'Arreglar la sombra'),
)

CHOICES_FECHA_PODA = (('A', 'Enero'),
                      ('B', 'Febrero'),
                      ('C', 'Marzo'),
                      ('D', 'Abril'),
                      ('E', 'Mayo'),
                      ('F', 'Junio'),
                      ('G', 'Julio'),
                      ('H', 'Agosto'),
                      ('I', 'Septiembre'),
                      ('J', 'Octubre'),
                      ('K', 'Noviembre'),
                      ('L', 'Diciembre'),
                      )

CHOICES_EQUIPO_PODA = (
                     ('A', 'Serrucho cola de zorro'),
                     ('B', 'Tijera'),
                     ('C', 'Mazo'),
                     ('D', 'Cutacha'),
                     ('E', 'Desjarretadora'),
                     ('F', 'Escalera'),
                     ('G', 'Mecate'),
                     ('H', 'Pasta cicatrizante'),
                     )


CHOICE_SUELO_USO_PARCELA = (
                            (1, 'Bosque'),
                            (2, 'Potrero'),
                            (3, 'Granos básicos'),
                            (4, 'Tacotal'),
                            (5, 'Cacaotal viejo'),
                            )
CHOICE_SUELO_LIMITANTES = (
                            ('A', 'Acidez / pH del suelo '),
                            ('B', 'Encharcamiento / Mal Drenaje'),
                            ('C', 'Enfermedades de raíces '),
                            ('D', 'Deficiencia de nutrientes'),
                            ('E', 'Baja materia orgánica'),
                            ('F', 'Baja actividad biológica y presencia de lombrices'),
                            ('G', 'Erosión'),
                            ('H', 'Compactación e infiltración de agua'),
                            )

CHOICE_SUELO_ORIENTACION = (
                            ('A', 'Técnico'),
                            ('B', 'Casa comercial'),
                            ('C', 'Cooperativa'),
                            ('D', 'Otros productores'),
                            ('E', 'Experiencia propia/costumbres'),
                            ('F', 'Otros medio de comunicación'),
                            ('G', 'Análisis de suelo '),
                            ('H', 'Otros '),
                            )

CHOICE_SUELO_ABONOS = (
                            ('A', 'Hecho en finca (compost, estiércol)'),
                            ('B', 'Regalados de otra finca (compost, estiércol)'),
                            ('C', 'Comprados de otra finca (compost, estiércol)'),
                            ('D', 'Comprado de casa comercial'),
                            ('E', 'Con crédito de la cooperativa'),
                            ('F', 'Incentivos/Regalados'),
                            ('G', 'No aplica'),
                            )

CHOICE_SUELO_EROSION_OPCION = (
                            (1, 'Deslizamientos'),
                            (2, 'Evidencia de erosión'),
                            (3, 'Cárcavas'),
                            (4, 'Área de acumulación de sedimentos'),
                            (5, 'Pedregosidad'),
                            (6, 'Raíces desnudos'),
                        )

CHOICE_SUELO_EROSION_RESPUESTA = (
                            (1, 'No presente'),
                            (2, 'Algo'),
                            (3, 'Severo'),
                        )

CHOICE_SUELO_CONSERVACION_OPCION = (
                            (1, 'Barrera muertas'),
                            (2, 'Barrera Viva'),
                            (3, 'Siembra en Curvas a Nivel'),
                            (4, 'Terrazas'),
                            (5, 'Cobertura de piso'),
                        )

CHOICE_SUELO_CONSERVACION_RESPUESTA = (
                            (1, 'No presente'),
                            (2, 'En mal estado'),
                            (3, 'En buen estado'),
                        )

CHOICE_SUELO_DRENAJE_OPCION = (
                            (1, 'Encharcamientos'),
                            (2, 'Amarillamiento/mal crecimiento'),
                            (3, 'Enfermedades (phytophthora)'),
                        )

CHOICE_SUELO_DRENAJE_OPCION2 = (
                            (1, 'Acequias'),
                            (2, 'Canales de drenaje a lo largo y ancho de la parcela'),
                            (3, 'Canales de drenaje alrededor de las plantas'),
                            (4, 'Canales a lado de la parcela'),
                            (5, 'Cobertura de piso'),
                        )

CHOICE_SUELO_OPCION_PUNTOS = (
                            (1, 'Severidad de daño de nematodos'),
                            (2, 'Severidad de daño de hongos'),
                        )

CHOICE_SUELO_RESPUESTA_PUNTOS = (
                            (1, 'No Afectado menos del 1%'),
                            (2, 'Poco Afectado 1 - 20%'),
                            (3, 'Afectados 20-30%'),
                            (4, 'Muy afectados 30-50%'),
                            (4, 'Severamente afectados más de 50%'),
                        )

CHOICE_PUNTO9_LIMITACION_1 = (
                                (1, 'Erosión de Suelo'),
                            )

CHOICE_PUNTO9_LIMITACION_1_ACCION = (
                                ('A', 'Barrera viva'),
                                ('B', 'Cobertura de suelo'),
                                ('C', 'Barrera Muerta'),
                                ('D', 'Siembra en Curvas a Nivel'),
                                ('E', 'Terrazas'),
                            )

CHOICE_PUNTO9_LIMITACION_2 = (
                                (1, 'Mal drenaje y encharamientos'),
                            )
CHOICE_PUNTO9_LIMITACION_2_ACCION = (
                                ('A', 'Acequias'),
                                ('B', 'Canales de drenaje de larga'),
                                ('C', 'Canales de drenaje alrededor de la parcela'),
                            )

CHOICE_PUNTO9_LIMITACION_3 = (
                                (1, 'Deficiencia de Nutrientes'),
                            )
CHOICE_PUNTO9_LIMITACION_3_ACCION = (
                                ('A', 'Aplicar abonos orgánicos'),
                                ('B', 'Aplicar abonos minerales'),
                            )

CHOICE_PUNTO9_LIMITACION_4 = (
                                (1, 'Exceso de nutrientes'),
                            )
CHOICE_PUNTO9_LIMITACION_4_ACCION = (
                                ('A', 'Bajar nivel de fertilización'),
                            )

CHOICE_PUNTO9_LIMITACION_5 = (
                                (1, 'Desbalance de nutrientes'),
                            )
CHOICE_PUNTO9_LIMITACION_5_ACCION = (
                                ('A', 'Ajustar programa de fertilización '),
                            )
CHOICE_PUNTO9_LIMITACION_6 = (
                                (1, 'Enfermedades y plagas de raíces'),
                            )
CHOICE_PUNTO9_LIMITACION_6_ACCION = (
                                ('A', 'Abonos orgánicos'),
                                ('B', 'Obras de drenaje'),
                                ('C', 'Aplicación de ceniza'),
                            )

CHOICE_PUNTO9_DONDE = (
                                (1, 'En todo parcela'),
                                (2, 'En algunas partes'),
                            )

CHOICE_SUELO_PRODUCTO_COSECHA = (
                            (1, 'Cacao Grano Seco - (qq/mz/año)'),
                            (2, 'Leña - (cargas de 125lb /mz/año)'),
                            (3, 'Cabezas de Banano - (cabezas/mz/año)'),
                        )

CHOICE_COSECHA_ANALISIS_1 = (
                                ('A', 'Pocas plantas productivas'),
                                ('B', 'Muchas mazorcas enfermas'),
                                ('C', 'Muchas mazorcas dañadas'),
                                ('D', 'Muchas mazorcas pequeñas'),
                                ('E', 'Muchas mazorcas con pocos granos'),
                                ('F', 'Muchos granos pequeños'),
                            )

CHOICE_COSECHA_ANALISIS_2 = (
                                ('A', 'Mazorcas enfermas'),
                                ('B', 'Mazorcas dañadas'),
                                ('C', 'Mazorcas pequeñas'),
                            )

CHOICE_COSECHA_ANALISIS_3 = (
                                ('A', 'Remover las mazorcas enfermas a tiempo'),
                                ('B', 'Establecer control de las ardillas'),
                                ('C', 'Mejorar la nutrición de las plantas'),
                                ('D', 'Realizar poda de las plantas de cacao'),
                                ('E', 'Regular la sombra'),
                                ('F', 'Cosechar a tiempo'),
                                ('G', 'Reponer las plantas no productivas con plantas productivas'),
                            )

CHOICE_ENFERMEDADES_CACAOTALES = (
        (1, 'Monilia'),
        (2, 'Mazorca negra'),
        (3, 'Mal de machete'),
        (4, 'Mal de talluelo en el vivero'),
        (12,'Buba'),
        (5, 'Barrenadores de tallo'),
        (6, 'Zompopos'),
        (7, 'Chupadores o áfidos'),
        (8, 'Escarabajos'),
        (9, 'Comején'),
        (10, 'Ardillas'),
        (11, 'Otros'),
    )

CHOICE_ENFERMEDADES_CACAOTALES_MULTI = (
        ('A', 'Monilia'),
        ('B', 'Mazorca negra'),
        ('C', 'Mal de machete'),
        ('D', 'Mal de talluelo en el vivero'),
        ('E','Buba'),
        ('F', 'Barrenadores de tallo'),
        ('G', 'Zompopos'),
        ('H', 'Chupadores o áfidos'),
        ('I', 'Escarabajos'),
        ('J', 'Comején'),
        ('K', 'Ardillas'),
        ('L', 'Otros'),
    )

CHOICE_ACCIONES_ENFERMEDADES = (
        (1, 'Recuento de plagas'),
        (2, 'Cortar las mazorcas enfermas'),
        (3, 'Abonar las plantas'),
        (4, 'Aplicar Caldos'),
        (5, 'Aplicar Fungicidas'),
        (6, 'Manejo de sombra'),
        (7, 'Podar las plantas de cacao'),
        (8, 'Aplicar venenos para Zompopo'),
        (9, 'Control de Comején'),
        (10, 'Ahuyar Ardillas'),
        (11, 'Otras'),
    )

CHOICE_PISO1 = (
        ("A", 'Zacates o matas de hoja angosta'),
        ("B", 'Arbustos o plantas de hoja ancha'),
        ("C", 'Coyol o Coyolillo'),
        ("D", 'Bejucos'),
        ("E", 'Tanda o Plantas parasíticas'),
        ("F", 'Cobertura de hoja ancha'),
        ("G", 'Cobertura de hoja angosta'),
    )

CHOICE_PISO3 = (
        (1, 'Recuento de malezas'),
        (2, 'Chapoda tendida'),
        (3, 'Chapoda selectiva'),
        (4, 'Aplicar herbicidas total'),
        (5, 'Aplicar herbicidas en parches'),
        (6, 'Manejo de bejuco'),
        (7, 'Manejo de tanda'),
        (8, 'Regulación de sombra'),
    )

CHOICE_PISO4 = (
        ("A", 'Técnico'),
        ("B", 'Casa comercial'),
        ("C", 'Cooperativa'),
        ("D", 'Otros productores'),
        ("E", 'Experiencia propia/costumbres'),
        ("F", 'Otros medio de comunicación'),
    )

CHOICE_OBSERVACION_PUNTO1 = (
        (1, 'Monilia'),
        (2, 'Mazorca Negra'),
        (3, 'Mal de machete'),
        (4, 'Buba'),
        (5, 'Daño de ardilla'),
        (6, 'Daño de barrenador'),
        (7, 'Chupadores'),
        (8, 'Daño de zompopo'),
        (9, 'Bejuco'),
        (10, 'Tanda o plantas parasíticas'),
        (11, 'Daño de comején'),
        (12, 'Daño de minador de la hoja'),
        (13, 'Daño por lana'),
        (15,'Deficiencia Nutricional'),
        (14, 'Otros'),
    )

CHOICE_PISO5 = (
        (1, 'Zacate anual'),
        (2, 'Zacate perene'),
        (3, 'Hoja ancha anual'),
        (4, 'Hoja ancha perenne'),
        (5, 'Ciperácea o Coyolillo'),
        (6, 'Bejucos en suelo'),
        (7, 'Cobertura hoja ancha'),
        (8, 'Cobertura hoja angosta'),
        (9, 'Hojarasca'),
        (10, 'Mulch de maleza'),
        (11, 'Suelo desnudo')
    )

CHOICE_ENFERMEDADES = (
        ("A", 'Monilia'),
        ("B", 'Mazorca negra'),
        ("C", 'Mal de machete'),
        ("D", 'Mal de talluelo en el vivero'),
        ("E", 'Barrenadores de tallo'),
        ("F", 'Zompopos'),
        ("G", 'Chupadores o áfidos'),
        ("H", 'Escarabajos'),
        ("J", 'Comején'),
        ("K", 'Minador de la hoja'),
        ("L", 'Lana'),
        ("M", 'Ardillaa'),
        ("N", 'Bejuco'),
        ("O", 'Tanda'),
        ("P", 'Buba'),
    )

CHOICE_SITUACION_PLAGAS = (
        (1, 'Varias plagas en todos los puntos'),
        (2, 'Varias plagas en algunos puntos'),
        (3, 'Pocas plagas en todos los puntos'),
        (4, 'Pocas plagas en algunos puntos'),
        (5, 'Una plaga en todos los puntos'),
        (6, 'Una plaga en algunos puntos'),
    )

CHOICE_ENFERMEDADES_PUNTO6_1 = (
        ("A", 'Suelo erosionado'),
        ("B", 'Suelo poco fértil'),
        ("C", 'Mucha competencia'),
        ("D", 'Mal drenaje'),
        ("E", 'Falta obras de conservación'),
        ("F", 'Suelo compacto'),
        ("G", 'Suelo con poca MO'),
        ("H", 'No usa abono o fertilizante'),
    )

CHOICE_ENFERMEDADES_PUNTO6_2 = (
        (1, 'Sombra muy densa'),
        (2, 'Sombra muy rala'),
        (3, 'Sombra mal distribuida'),
        (4, 'Arboles de sombra no adecuada'),
        (5, 'Mucha auto-sombra'),
        (6, 'Mucho banano'),
    )

CHOICE_ENFERMEDADES_PUNTO6_3 = (
        ("A", 'Poda no adecuada'),
        ("B", 'Piso no manejado'),
        ("C", 'No eliminan mazorcas enfermas'),
        ("D", 'No hay manejo de plagas'),
        ("E", 'Plantas desnutridas'),
        ("F", 'Plantación vieja'),
        ("G", 'Variedades susceptibles'),
        ("H", 'Variedades no productivas'),
    )

CHOICE_ACCIONES_PUNTO7_1 = (
        (1, 'Recuento de plagas'),
        (2, 'Cortar las mazorcas enfermas'),
        (3, 'Abonar las plantas'),
        (4, 'Aplicar Caldos'),
        (5, 'Aplicar Fungicidas'),
        (6, 'Manejo de sombra'),
        (7, 'Podar las plantas de cacao'),
        (8, 'Aplicar venenos para Zompopo'),
        (9, 'Control de Comején'),
    )

CHOICE_ACCIONES_PUNTO7_2 = (
        (1, 'Toda la parcela'),
        (2, 'Alguna parte de la parcela'),
    )

CHOICE_ENFERMEDADES_PUNTO8 = (
        ("A", 'Medial Luna'),
        ("B", 'Tijera'),
        ("C", 'Serrucho'),
        ("D", 'Bomba de mochila'),
        ("E", 'Barril'),
        ("F", 'Cutacha'),
        ("G", 'Coba'),
        ("H", 'No tiene'),
    )

CHOICE_PISO6_1 = (
        ("A", 'Sin competencia'),
        ("B", 'Media competencia'),
        ("C", 'Alta competencia'),
    )
CHOICE_PISO6_2 = (
        (1, 'Piso cubierto pero compite'),
        (2, 'Piso medio cubierto y compite'),
        (3, 'Piso no cubierto'),
        (4, 'Piso con mucho bejuco'),
        (5, 'Plantas con bejuco'),
        (6, 'Plantas con tanda'),
    )
CHOICE_PISO6_3 = (
        ("A", 'Zacate anual'),
        ("B", 'Zacate perene'),
        ("C", 'Hoja ancha anual'),
        ("D", 'Hoja ancha perenne'),
        ("E", 'Ciperácea o Coyolillo'),
        ("F", 'Bejucos'),
    )

CHOICE_PISO7_1 = (
        ("A", 'Suelo erosionado'),
        ("B", 'Suelo poco fértil'),
        ("C", 'Mal drenaje'),
        ("D", 'Suelo compacto'),
        ("E", 'Suelo con poca MO'),
        ("F", 'No usa abono o fertilizante'),
    )

CHOICE_PISO7_2 = (
        ("A", 'Sombra muy rala'),
        ("B", 'Sombra mal distribuida'),
        ("C", 'Arboles de sombra no adecuada'),
        ("D", 'Poco banano'),
    )

CHOICE_PISO7_3 = (
        ("A", 'Chapoda no adecuada'),
        ("B", 'Chapoda tardía'),
        ("C", 'No hay manejo selectivo'),
        ("D", 'Plantas desnutridas'),
        ("E", 'Plantación vieja'),
        ("F", 'Mala selección de herbicidas'),
    )

CHOICE_PISO8 = (
        (1, 'Recuento de malezas'),
        (2, 'Chapoda tendida'),
        (3, 'Chapoda selectiva'),
        (4, 'Aplicar herbicidas total'),
        (5, 'Aplicar herbicidas en parches'),
        (6, 'Manejo de bejuco'),
        (7, 'Manejo de tanda'),
        (8, 'Regulación de sombra'),
    )

CHOICE_PISO10 = (
        ("A", 'Machete'),
        ("B", 'Pico'),
        ("C", 'Pala'),
        ("D", 'Bomba de mochila'),
        ("E", 'Barril'),
        ("F", 'Cutacha'),
        ("G", 'Coba'),
        ("H", 'No tiene'),
    )

CHOICE_COSECHA_ESTIMADO_PUNTOS = (
                                (1, 'Número de mazorcas sanas'),
                                (2, 'Número de mazorcas enfermas'),
                                (3, 'Número de mazorcas dañadas'),
                            )

CHOICE_COSECHA_10_COSECHA = (
                                (1, 'No hay Cosecha'),
                                (2, 'Poca cosecha'),
                                (3, 'Algo de cosecha'),
                                (4, 'Mucha cosecha'),
                            )

CHOICE_COSECHA_9_MESES = (
                                (1, 'Enero'),
                                (2, 'Febrero'),
                                (3, 'Marzo'),
                                (4, 'Abril'),
                                (5, 'Mayo'),
                                (6, 'Junio'),
                                (7, 'Julio'),
                                (8, 'Agosto'),
                                (9, 'Septiembre'),
                                (10, 'Octubre'),
                                (11, 'Noviembre'),
                                (12, 'Diciembre'),
                            )

CHOICE_COSECHA_9_FLORACION = (
                                (1, 'No hay flores'),
                                (2, 'Poca flores'),
                                (3, 'Algo de flores'),
                                (4, 'Mucha flores'),
                            )

CHOICE_COSECHA_CONVERSACION_5 = (
                                ('A', 'Bolsa plástica'),
                                ('B', 'Bidón o Balde'),
                                ('C', 'Saco Macen'),
                                ('D', 'Saco de yute'),
                                ('E', 'Cajón de madera'),
                            )

CHOICE_COSECHA_CONVERSACION_7 = (
                                ('A', 'Entierra las mazorcas'),
                                ('B', 'Botan las mazorcas sin enterrar'),
                                ('C', 'Queman las mazorcas'),
                            )

CHOICE_COSECHA_CONVERSACION_8 = (
                                (1, 'Cada mes'),
                                (2, 'Cada quince días'),
                                (3, 'Depende de la maduración'),
                            )

CHOICE_COSECHA_CONVERSACION_1 = (
                                ('A', 'Por el color'),
                                ('B', 'Por el tamaño'),
                                ('C', 'Por la textura'),
                                ('D', 'Por la fecha'),
                            )

CHOICE_COSECHA_CONVERSACION_2 = (
                                ('A', 'Media Luna'),
                                ('B', 'Cutacha'),
                                ('C', 'Machete'),
                                ('D', 'Tijera'),
                            )

CHOICE_COSECHA_CONVERSACION_3 = (
                                ('A', 'Rechazar mazorcas enfermas'),
                                ('B', 'Rechazar mazorcas dañadas'),
                                ('C', 'Rechazar mazorcas sobremaduras'),
                                ('D', 'Rechazar mazorcas inmaduras'),
                                ('E', 'Rechazar mazorcas pequeñas'),
                                ('F', 'Seleccionar mazorcas maduras'),
                                ('G', 'Seleccionar mazorcas de buena calidad'),
                            )
CHOICE_COSECHA_CONVERSACION_4 = (
                                ('A', 'Media Luna'),
                                ('B', 'Cutacha'),
                                ('C', 'Machete'),
                                ('D', 'Maso'),
                            )

CHOICE_CIERRE_1_1_IMPACTO = (
                    ('A', 'Tipo de árboles y cantidad'),
                    ('B', 'Mucha sombra de los árboles'),
                    ('C', 'Poca sombra de los árboles'),
                    ('D', 'Efecto de sombra sobre las plagas y enfermedades'),
                    ('E', 'Efecto de sombra sobre la producción'),
                    ('F', 'Ninguna'),
                )

CHOICE_CIERRE_1_1_PLANIFICADA = (
                    ('A', 'Regulación de sombra'),
                    ('B', 'Eliminación de árboles'),
                    ('C', 'Sembrar árboles'),
                    ('D', 'Eliminar musaceas'),
                    ('E', 'Sembrar musaceas y sombra temporal'),
                    ('F', 'Ninguna'),
                    ('G', 'Ninguna por falta de recursos'),
                )

CHOICE_CIERRE_1_1_REALIZADA = (
                    ('A', 'Regulación de sombra'),
                    ('B', 'Eliminación de árboles'),
                    ('C', 'Sembrar árboles'),
                    ('D', 'Eliminar musaceas'),
                    ('E', 'Sembrar musaceas y sombra temporal'),
                    ('F', 'Ninguna'),
                    ('G', 'Ninguna por falta de recursos'),
                )

CHOICE_CIERRE_1_1_RESULTADOS = (
                    ('A', 'Aumento de producción'),
                    ('B', 'Mejor control de malas hierbas'),
                    ('C', 'Reducción de enfermedades'),
                    ('D', 'Eliminar musaceas'),
                    ('E', 'Ninguna'),
                )

CHOICE_CIERRE_1_2_IMPACTO = (
                    ('A', 'Altura y ancho de plantas de cacao'),
                    ('B', 'Falta de horquetas'),
                    ('C', 'Muchas ramas bajeras y entrecruzadas'),
                    ('D', 'Poca penetración de luz'),
                    ('E', 'Relación entre poda y productividad'),
                    ('F', 'Ninguna'),
                )

CHOICE_CIERRE_1_2_PLANIFICADA = (
                    ('A', 'Descope de las plantas'),
                    ('B', 'Poda de las ramas  entrecruzadas'),
                    ('C', 'Eliminar los chupones'),
                    ('D', 'Formar horquetas'),
                    ('E', 'Eliminar ramas bajeras'),
                    ('F', 'Ninguna'),
                    ('G', 'Ninguna por falta de recursos'),
                )

CHOICE_CIERRE_1_2_REALIZADA = (
                    ('A', 'Descope de las plantas'),
                    ('B', 'Poda de las ramas  entrecruzadas'),
                    ('C', 'Eliminar los chupones'),
                    ('D', 'Formar horquetas'),
                    ('E', 'Eliminar ramas bajeras'),
                    ('F', 'Ninguna'),
                    ('G', 'Ninguna por falta de recursos'),
                )

CHOICE_CIERRE_1_2_RESULTADOS = (
                    ('A', 'Aumento de producción'),
                    ('B', 'Mejor entrada de luz'),
                    ('C', 'Reducción de enfermedades'),
                    ('D', 'Ninguna'),
                )

CHOICE_CIERRE_1_3_IMPACTO = (
                    ('A', 'Falta de obra de conservación'),
                    ('B', 'Falta de obra de drenaje'),
                    ('C', 'Deficiencia o desbalance de nutrientes'),
                    ('D', 'Estado de fertilidad de suelo'),
                    ('E', 'Relación entre suelo, fertilidad y la productividad'),
                    ('F', 'Ninguna'),
                )

CHOICE_CIERRE_1_3_PLANIFICADA = (
                    ('A', 'Aplicar abono orgánicos'),
                    ('B', 'Aplicar abono mineral'),
                    ('C', 'Aplicar Cal o Ceniza'),
                    ('D', 'Abonar según datos de análisis'),
                    ('E', 'Sembrar abono verde y cobertura'),
                    ('F', 'Ninguna'),
                    ('G', 'Ninguna por falta de recursos'),
                )

CHOICE_CIERRE_1_3_REALIZADA = (
                    ('A', 'Aplicar abono orgánicos'),
                    ('B', 'Aplicar abono mineral'),
                    ('C', 'Aplicar Cal o Ceniza'),
                    ('D', 'Abonar según datos de análisis'),
                    ('E', 'Sembrar abono verde y cobertura'),
                    ('F', 'Ninguna'),
                    ('G', 'Ninguna por falta de recursos'),
                )

CHOICE_CIERRE_1_3_RESULTADOS = (
                    ('A', 'Aumento de producción'),
                    ('B', 'Aumento de la floración'),
                    ('C', 'Reducción de enfermedades'),
                    ('D', 'Abonar según datos de análisis'),
                    ('E', 'Ninguna'),
                )

CHOICE_CIERRE_1_4_IMPACTO = (
                    ('A', 'Variedad de plagas y enfermedades'),
                    ('B', 'Nivel de daño de plagas y enfermedades'),
                    ('C', 'Relación entre poda , plagas y enfermedades'),
                    ('D', 'Relación entre sombra y plagas y enfermedades'),
                    ('E', 'Impacto de plagas y enfermedades sobre producción'),
                    ('F', 'Ninguna'),
                )

CHOICE_CIERRE_1_4_PLANIFICADA = (
                    ('A', 'Realizar recuentos'),
                    ('B', 'Mejorar la sombra'),
                    ('C', 'Mejorar la poda'),
                    ('D', 'Eliminar mazorcas enfermas'),
                    ('E', 'Aplicar caldo sulfo-calcico'),
                    ('F', 'Aplicar bio-fermentados'),
                    ('G', 'Ninguna'),
                    ('H', 'Ninguna por falta de recursos'),
                )

CHOICE_CIERRE_1_4_REALIZADA = (
                    ('A', 'Realizar recuentos'),
                    ('B', 'Mejorar la sombra'),
                    ('C', 'Mejorar la poda'),
                    ('D', 'Eliminar mazorcas enfermas'),
                    ('E', 'Aplicar caldo sulfo-calcico'),
                    ('F', 'Aplicar bio-fermentados'),
                    ('G', 'Ninguna'),
                    ('H', 'Ninguna por falta de recursos'),
                )

CHOICE_CIERRE_1_4_RESULTADOS = (
                    ('A', 'Aumento de producción'),
                    ('B', 'Reducción de daño de plagas'),
                    ('C', 'Reducción de enfermedades'),
                    ('D', 'Ninguna'),
                )

CHOICE_CIERRE_1_5_IMPACTO = (
                    ('A', 'Variedad de mala hierbas'),
                    ('B', 'Nivel de daño de mala hierbas'),
                    ('C', 'Relación entre chapoda y composición del piso'),
                    ('D', 'Relación entre herbicidas y composición del piso'),
                    ('E', 'Cantidad de bejucos en el piso y plantas'),
                    ('F', 'Ninguna'),
                    ('G', 'Falta de materia organica'),
                )

CHOICE_CIERRE_1_5_PLANIFICADA = (
                    ('A', 'Realizar conteo'),
                    ('B', 'Mejorar la sombra'),
                    ('C', 'Eliminar bejucos'),
                    ('D', 'Eliminar tanda'),
                    ('E', 'Realizar manejo selectivo'),
                    ('F', 'Ninguna'),
                    ('G', 'Ninguna por falta de recursos'),
                    ('H', 'Repartir hojarasca'),
                )

CHOICE_CIERRE_1_5_REALIZADA = (
                    ('A', 'Realizar conteo'),
                    ('B', 'Mejorar la sombra'),
                    ('C', 'Eliminar bejucos'),
                    ('D', 'Eliminar tanda'),
                    ('E', 'Realizar manejo selectivo'),
                    ('F', 'Ninguna'),
                    ('G', 'Ninguna por falta de recursos'),
                    ('H', 'Repartir hojarasca'),
                )

CHOICE_CIERRE_1_5_RESULTADOS = (
                    ('A', 'Aumento de producción'),
                    ('B', 'Reducción de malas hierbas dañinas'),
                    ('C', 'Aumento de cobertura'),
                    ('D', 'Eliminar tanda'),
                    ('E', 'Ninguna'),
                )

CHOICE_CIERRE_1_6_IMPACTO = (
                    ('A', 'Tipo de cacao que estamos sembrando'),
                    ('B', 'Auto-incompatibilidad de las semillas'),
                    ('C', 'La calidad de semillas'),
                    ('D', 'Incidencia de plagas y enfermedades en vivero'),
                    ('E', 'Calidad de plantas'),
                    ('F', 'Ninguna'),
                )

CHOICE_CIERRE_1_6_PLANIFICADA = (
                    ('A', 'Seleccionar mazorcas y mezclar para conseguir semilla'),
                    ('B', 'Utilizar mejor calidad de semillas'),
                    ('C', 'Mejorar el sustrato'),
                    ('D', 'Mejorar el tamaño de bolsa'),
                    ('E', 'Mejorar manejo de enfermedades y plagas'),
                    ('F', 'Ninguna'),
                    ('G', 'Ninguna por falta de recursos'),
                )

CHOICE_CIERRE_1_6_REALIZADA = (
                    ('A', 'Seleccionar mazorcas y mezclar para conseguir semilla'),
                    ('B', 'Utilizar mejor calidad de semillas'),
                    ('C', 'Mejorar el sustrato'),
                    ('D', 'Mejorar el tamaño de bolsa'),
                    ('E', 'Mejorar manejo de enfermedades y plagas'),
                    ('F', 'Ninguna'),
                    ('G', 'Ninguna por falta de recursos'),
                )

CHOICE_CIERRE_1_6_RESULTADOS = (
                    ('A', 'Mejor vigor de las plantas'),
                    ('B', 'Menos daño de plagas'),
                    ('C', 'Menos daño de enfermedades'),
                    ('D', 'Ninguna'),
                )

CHOICE_CIERRE_1_7_IMPACTO = (
                    ('A', 'Cantidad de planta productiva'),
                    ('B', 'Numero de mazorcas sanas'),
                    ('C', 'Numero de mazorcas dañadas'),
                    ('D', 'Nivel de cosecha de la parcela'),
                    ('E', 'Ninguna'),
                    ('F', 'Efecto de sombra sobre la producción'),
                    ('G', 'Efecto de poda sobre la producción'),
                )

CHOICE_CIERRE_1_7_PLANIFICADA = (
                    ('A', 'Mejorar la poda y sombra'),
                    ('B', 'Mejorar la fertilización'),
                    ('C', 'Mejorar manejo de plagas'),
                    ('D', 'Eliminar planta poca productivas'),
                    ('E', 'Sembrar plantas más productivas'),
                    ('F', 'Ninguna'),
                    ('G', 'Ninguna por falta de recursos'),
                )

CHOICE_CIERRE_1_7_REALIZADA = (
                    ('A', 'Mejorar la poda y sombra'),
                    ('B', 'Mejorar la fertilización'),
                    ('C', 'Mejorar manejo de plagas'),
                    ('D', 'Eliminar planta poca productivas'),
                    ('E', 'Sembrar plantas más productivas'),
                    ('F', 'Ninguna'),
                    ('G', 'Ninguna por falta de recursos'),
                )

CHOICE_CIERRE_1_7_RESULTADO = (
                    ('A', 'Aumento de la cosecha'),
                    ('B', 'Aumento de plantas productivas'),
                    ('C', 'Mejor calidad de mazorcas'),
                    ('D', 'Mejor calidad de granos'),
                    ('E', 'Ninguna'),
                )

CHOICE_CIERRE_COSTO_1 = (
                    ('A', 'Cacao Criollo'),
                    ('B', 'Cacao Trinitario'),
                    ('C', 'Cacao Forastero'),
                    ('D', 'Cacao híbrido'),
                    ('E', 'Clones de cacao'),
                    ('F', 'Mezcla de tipos'),
                )

CHOICE_MONEDA_LOCAL = (
    (1, 'NIO'),
    (2, 'HNL'),
    (3, 'USD'),
    (4, 'DOP'),
    (5, 'GTQ'),
    )

CHOICE_PRODUCTOS = (
    (1, 'Cacao en baba'),
    (2, 'Cacao en grano seco'),
    (3, 'Cosecha de granos'),
    (4, 'Musaceas-Banano'),
    (5, 'Musaceas-Plátano'),
    (6, 'Frutales-Zapote'),
    (7, 'Frutales-Cítricos'),
    (8, 'Frutales-Aguacate'),
    (9, 'Frutales-Coco'),
    (10, 'Madera-Caoba'),
    (11, 'Madera-Cortez'),
    (12, 'Madera-Roble'),
    (13, 'Madera-Melina'),
    (14, 'Madera-Granadillo'),
    (15, 'Madera-Leña'),
    (16, 'Granos básicos-Maíz'),
    (17, 'Granos básicos-Frijol'),
    (18, 'Gandul'),
    (19, 'Guaba'),
    (20, 'Café Uva'),
    (21, 'Café Pergamino'),
    (22, 'Café Tostado'),
    )

CHOICE_UNIDAD_DATOS = (
    (1, 'qq'),
    (2, 'Cabeza'),
    (3, 'Lb'),
    (4, 'Kg'),
    (5, 'Docena'),
    (6, 'Cien'),
    (7, 'Saco'),
    (8, 'M3'),
    (9, 'Pt'),
    (10, 'Unidad'),
    (11, 'Carga'),
    )

CHOICE_COMPRADOR_DATOS = (
    ('A', 'Intermediario'),
    ('B', 'Cooperativa'),
    ('C', 'Asociación'),
    ('D', 'Empresa'),
    )

CHOICE_CIERRE_CICLO_TRABAJO1_RESPUESTA = (
    (1, 'Mucho'),
    (2, 'Algo'),
    (3, 'Poco'),
    (4, 'Nada '),
    )

CHOICE_CIERRE_CICLO_TRABAJO2_RESPUESTA = (
    (1, 'Todas'),
    (2, 'Algunas'),
    (3, 'Pocas'),
    (4, 'Ninguna'),
    )

CHOICE_CIERRE_CICLO_TRABAJO3_RESPUESTA = (
    (1, 'Demasiada visitas'),
    (2, 'Adecuadas visitas'),
    (3, 'Pocas visitas'),
    )

CHOICE_CIERRE_CICLO_TRABAJO4_RESPUESTA = (
    (1, 'Demasiada larga'),
    (2, 'Adecuado tiempo '),
    (3, 'Muy corta'),
    )

CHOICE_CIERRE_CICLO_TRABAJO5_RESPUESTA = (
    (1, 'Si y con mucho ánimo'),
    (2, 'Si pero con poco ánimo'),
    (3, 'Si porque siento obligado'),
    (4, 'No quiero seguir'),
    )
CHOICE_COSTO_CACAO_ACTIVIDAD = (
    (1, 'Siembra/resiembra'),
    (2, 'Poda de cacao'),
    (3, 'Deschuponado'),
    (4, 'Poda de árboles'),
    (5, 'Aplicación de abono'),
    (6, 'Aplicación de insecticida'),
    (7, 'Aplicación de fungicida'),
    (8, 'Manejo de piso'),
    (9, 'Aplicación de riego'),
    (10, 'Obras de drenaje'),
    (11, 'Obras de conservación'),
    (12, 'Cosecha y Corte'),
    (13, 'Transporte de cosecha'),
    )

CHOICE_COSTO_MUSACEAS_ACTIVIDAD = (
    (1, 'Siembra/resiembra'),
    (2, 'Deshije y deshoja'),
    (3, 'Manejo de cabezas'),
    (4, 'Aplicación de abono'),
    (5, 'Aplicación de insecticida'),
    (6, 'Aplicación de fungicida'),
    (7, 'Cosecha y Corte'),
    (8, 'Transporte de cosecha'),
    )

CHOICE_COSTO_FRUTALES_ACTIVIDAD = (
    (1, 'Siembra/resiembra'),
    (2, 'Poda de frutales'),
    (3, 'Aplicación de abono'),
    (4, 'Aplicación de insecticida'),
    (5, 'Aplicación de fungicida'),
    (6, 'Cosecha y Corte'),
    (7, 'Transporte de cosecha'),
    )

CHOICE_COSTO_MADERABLES_ACTIVIDAD = (
    (1, 'Siembra/resiembra'),
    (2, 'Poda de maderables'),
    (3, 'Aplicación de abono'),
    (4, 'Aplicación de insecticida'),
    (5, 'Aplicación de fungicida'),
    (6, 'Cosecha y Corte'),
    (7, 'Transporte de cosecha'),
    )

CHOICE_COSTO_SOMBRA_TEMPORAL_ACTIVIDAD = (
    (1, 'Siembra/resiembra'),
    (2, 'Cosecha y Corte'),
    (3, 'Transporte de cosecha'),
    )

CHOICE_COSTO_CAFEL_ACTIVIDAD = (
    (1, 'Siembra/resiembra'),
    (2, 'Poda de Café'),
    (3, 'Poda de árboles'),
    (4, 'Aplicación de abono'),
    (5, 'Aplicación de insecticida'),
    (6, 'Aplicación de fungicida'),
    (7, 'Manejo de piso (maleza)'),
    (8, 'Aplicación de riego'),
    (9, 'Obras de drenaje'),
    (10, 'Obras de conservación'),
    (11, 'Cosecha y Corte'),
    (12, 'Transporte de cosecha'),
    )
