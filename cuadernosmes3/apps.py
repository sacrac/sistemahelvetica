# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class Cuadernosmes3Config(AppConfig):
    name = 'cuadernosmes3'
    verbose_name = "Cuadernos mes 3"
