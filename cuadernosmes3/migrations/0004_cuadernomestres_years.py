# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-01-19 01:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cuadernosmes3', '0003_auto_20190118_0023'),
    ]

    operations = [
        migrations.AddField(
            model_name='cuadernomestres',
            name='years',
            field=models.IntegerField(default=0, editable=False, verbose_name='A\xf1o'),
            preserve_default=False,
        ),
    ]
