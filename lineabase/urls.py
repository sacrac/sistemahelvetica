from django.conf.urls import url
from django.views.generic import TemplateView
from .views import *

urlpatterns =  [
    url(r'^inicio/', homePageViewLineaBase, name='index-linea-base'),
    url(r'^consultar/', consulta_herramienta, name='linea-consulta'),
    url(r'^consultar-establecimiento/', consulta_establecimiento, name='consulta-establecimiento'),
    url(r'^fichas/', TemplateView.as_view(template_name="fichas_cuadernos.html")),
    url(r'^videos-tutoriales/', TemplateView.as_view(template_name="video_tutorial.html")),
    url(r'^acerca-practicas/', TemplateView.as_view(template_name="acerca_practicas.html")),
    url(r'^mapa-datos/', TemplateView.as_view(template_name="mapa_datos.html")),
    url(r'^entrevistados-autocomplete/',
        EntrevistadosAutocomplete.as_view(),
        name='entrevistados-autocomplete',),
    url(
        r'^departamento-autocomplete/',
        DepartamentoAutocomplete.as_view(create_field='nombre'),
        name='depart-autocomplete',
    ),
    url(
        r'^municipio-autocomplete/',
        MunicipioAutocomplete.as_view(create_field='nombre'),
        name='municipio-autocomplete',
    ),
    url(
        r'^comunidad-autocomplete/',
        ComunidadAutocomplete.as_view(create_field='nombre'),
        name='comunidad-autocomplete',
    ),
    url(r'^api/productor/', get_productor, name='productor-search'),
    url(r'^ajax/load-departamentos/', load_departamentos, name='ajax_load_departamentos'),
    url(r'^ajax/load-municipios/', load_municipios, name='ajax_load_municipios'),
    url(r'^ajax/load-comunidad/', load_comunidades, name='ajax_load_comunidad'),
    url(r'^mapaherramienta/', obtener_lista_mapa_cacao, name='obtener-lista-mapa-cacao'),
    url(r'^(?P<vista>\w+)/$', get_view, name='get-view'),
]
