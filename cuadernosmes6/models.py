# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.contrib.auth.models import User
from lineabase.models import (Entrevistados,
                                                         Encuestadores,
                                                         Organizacion)
from cuadernosmes0.choices_static import *
from cuadernosmes0.models import (ClonesVariedades,
                                                           EspeciesFrutales,
                                                           EspeciesMaderables,
                                                           TiposMusaceas,
                                                           EspeciesArbolesServicios,
                                                           Years,
                                                           Ciclo)
from multiselectfield import MultiSelectField
from djmoney.money import Money
from djmoney.contrib.exchange.models import convert_money

# Create your models here.

@python_2_unicode_compatible
class CuadernoMesSeis(models.Model):
    year = models.ForeignKey(Years, on_delete=models.CASCADE, verbose_name='Año')
    fecha = models.DateField()
    ciclo = models.ForeignKey(Ciclo, on_delete=models.CASCADE)
    entrevistado = models.ForeignKey(Entrevistados,
                                                                   on_delete=models.CASCADE,
                                                                   verbose_name='Nombre productor/a')
    encuestador = models.ForeignKey(Encuestadores,
                                                                   on_delete=models.CASCADE,
                                                                   verbose_name='Nombre del técnico')
    # organizacion = models.ForeignKey(Organizacion, on_delete=models.CASCADE,
    #                                                                 verbose_name='Nombre de la organización',
    #                                                                 null=True, blank=True)
    years = models.IntegerField(editable=False, verbose_name='Año')
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.years = self.fecha.year
        super(CuadernoMesSeis, self).save(*args, **kwargs)

    def __str__(self):
        return self.entrevistado.nombre

    class Meta:
        verbose_name = 'Cuaderno Mes 6'
        verbose_name_plural = 'Cuadernos de los meses 6'

class CacaoPunto1(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    clones_variedad = models.ForeignKey(ClonesVariedades,
                                                                          on_delete=models.CASCADE,
                                                                          related_name='cacao_punto1')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    ramificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena ramificación")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Cacao punto 1"

class MusaceasPunto1(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    musaceas = models.ForeignKey(TiposMusaceas,
                                                                on_delete=models.CASCADE,
                                                                related_name='musaceas_punto1')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    foliar = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena área foliar y hijos")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Musaceas punto 1"

class FrutalesPunto1(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesFrutales,
                                                            on_delete=models.CASCADE,
                                                            related_name='frutales_punto1')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena crecimiento")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Frutales punto 1"

class MaderablesPunto1(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesMaderables,
                                                            on_delete=models.CASCADE,
                                                            related_name='maderable_punto1')
    altura = models.FloatField('Altura en mt')
    grosor = models.FloatField('Grosor (DAP) cm')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buen fuste y  crecimiento")

    class Meta:
        verbose_name_plural = "Maderables punto 1"

class ArbolesPunto1(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesArbolesServicios,
                                                            on_delete=models.CASCADE,
                                                            related_name='arboles_punto1')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buen fuste y  crecimiento")

    class Meta:
        verbose_name_plural = "Arboles de servicio punto 1"


#Estacion punto 2

class CacaoPunto2(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    clones_variedad = models.ForeignKey(ClonesVariedades,
                                                                          on_delete=models.CASCADE,
                                                                          related_name='cacao_punto2')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    ramificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena ramificación")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Cacao punto 2"

class MusaceasPunto2(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    musaceas = models.ForeignKey(TiposMusaceas,
                                                                on_delete=models.CASCADE,
                                                                related_name='musaceas_punto2')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    foliar = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena área foliar y hijos")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Musaceas punto 2"

class FrutalesPunto2(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesFrutales,
                                                            on_delete=models.CASCADE,
                                                            related_name='frutales_punto2')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena crecimiento")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Frutales punto 2"

class MaderablesPunto2(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesMaderables,
                                                            on_delete=models.CASCADE,
                                                            related_name='maderables_punto2')
    altura = models.FloatField('Altura en mt')
    grosor = models.FloatField('Grosor (DAP) cm')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buen fuste y  crecimiento")

    class Meta:
        verbose_name_plural = "Maderables punto 2"

class ArbolesPunto2(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesArbolesServicios,
                                                            on_delete=models.CASCADE,
                                                            related_name='arboles_punto2')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buen fuste y  crecimiento")

    class Meta:
        verbose_name_plural = "Arboles de servicio punto 2"

#Estacion punto 3

class CacaoPunto3(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    clones_variedad = models.ForeignKey(ClonesVariedades,
                                                                          on_delete=models.CASCADE,
                                                                          related_name='cacao_punto3')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    ramificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena ramificación")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Cacao punto 3"

class MusaceasPunto3(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    musaceas = models.ForeignKey(TiposMusaceas,
                                                                on_delete=models.CASCADE,
                                                                related_name='musaceas_punto3')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    foliar = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena área foliar y hijos")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Musaceas punto 3"

class FrutalesPunto3(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesFrutales,
                                                            on_delete=models.CASCADE,
                                                            related_name='frutales_punto3')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena crecimiento")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Frutales punto 3"

class MaderablesPunto3(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesMaderables,
                                                            on_delete=models.CASCADE,
                                                            related_name='maderables_punto3')
    altura = models.FloatField('Altura en mt')
    grosor = models.FloatField('Grosor (DAP) cm')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buen fuste y  crecimiento")

    class Meta:
        verbose_name_plural = "Maderables punto 3"

class ArbolesPunto3(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesArbolesServicios,
                                                            on_delete=models.CASCADE,
                                                            related_name='arboles_punto3')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buen fuste y  crecimiento")

    class Meta:
        verbose_name_plural = "Arboles de servicio punto 3"

#plagas y enfermedades del momento
class PlagasEnfermedades(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    cacao = MultiSelectField(choices=CHOICE_PLAGA_ENFERMEDAD_CACA0)
    musaceas = MultiSelectField(choices=CHOICE_PLAGA_ENFERMEDAD_MUSACEAS)
    frutales = MultiSelectField(choices=CHOICE_PLAGA_ENFERMEDAD_FRUTALES)
    maderables = MultiSelectField(choices=CHOICE_PLAGA_ENFERMEDAD_MADERABLES)
    arboles_servicios = MultiSelectField(choices=CHOICE_ARBOLES_MADERABLES)

    class Meta:
        verbose_name_plural = 'Plagas y enfermedades principales del momento'


class DatosPiso1(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    potencialmente = models.IntegerField(choices=CHOICE_UNIDAD_DATOS_POTENCIALMENTE,
                                    verbose_name='Malezas Potencialmente Dañinos')
    rayas = models.FloatField()
    conteo = models.FloatField()
    cobertura = models.FloatField('% de cobertura')

    class Meta:
        verbose_name_plural = 'Malezas Potencialmente Dañinos'

class DatosPiso2(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    cobertura_noble = models.IntegerField(choices=CHOICE_UNIDAD_DATOS_COBERTURA,
                                    verbose_name='Malezas de Cobertura Nobles')
    rayas = models.FloatField()
    conteo = models.FloatField()
    cobertura = models.FloatField('% de cobertura')

    class Meta:
        verbose_name_plural = 'Malezas de Cobertura Nobles'

class DatosPiso3(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    mulch = models.IntegerField(choices=CHOICE_UNIDAD_DATOS_MULCH,
                                    verbose_name='Mulch de maleza')
    rayas = models.FloatField()
    conteo = models.FloatField()
    cobertura = models.FloatField('% de cobertura')

    class Meta:
        verbose_name_plural = 'Mulch de maleza'

class AccionesManejoPiso(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    manejo_piso = models.IntegerField(choices=CHOICE_MANEJO_PISO)
    meses = MultiSelectField('En qué meses vamos a realizar el manejo(MC)',
                    choices=CHOICE_MESES_ANIO)

    class Meta:
        verbose_name_plural = 'Acciones a realizar para el manejo de piso de cacaotales '

class CostoManoObraDia(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    valor_mo_dia = models.FloatField('Valor de mano de obra por día')
    valor_mo_dia_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.valor_mo_dia_usd = convert_money(Money(self.valor_mo_dia, str(self.get_unidad_display())), 'USD')
        super(CostoManoObraDia, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Costo de mano de obra por dia"

class CostoCacao(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_CACAO_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoCacao, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de cacao'

class CostoMusaceas(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_MUSACEAS_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoMusaceas, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de musaceas'

class CostoFrutales(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_FRUTALES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoFrutales, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de frutales'

class CostoMaderables(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_MADERABLES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoMaderables, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de maderables'

class CostoSombraTemporal(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_SOMBRA_TEMPORAL_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(CostoSombraTemporal, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de árboles servicios'

class DatosCosecha(models.Model):
    cuaderno_seis = models.ForeignKey(CuadernoMesSeis,
                                                                      on_delete=models.CASCADE)
    fecha = models.DateField()
    productos = models.IntegerField(choices=CHOICE_PRODUCTOS)
    cantidad = models.FloatField()
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_DATOS)
    precio = models.FloatField('Precio/Unidad')
    precio_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL)
    comprador = MultiSelectField('Comprador',
                            choices=CHOICE_COMPRADOR_DATOS)

    cantidad_g = models.FloatField(editable=False, null=True, blank=True)
    precio_usd_g = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.precio_usd = convert_money(Money(self.precio, str(self.get_moneda_display())), 'USD')
        #musaceas-banano id(4) unidad cabeza(2)  1 cabeza= 6 kg banano
        if self.productos == 4 and self.unidad == 2:
            self.cantidad_g = self.cantidad * 6
            self.precio_usd_g = float(self.precio_usd) / 6.0
        #musaceas-platano id(5) unidad cabeza(2)  1 Cabeza = 5.2 kg de plátano
        if self.productos == 5 and self.unidad == 2:
            self.cantidad_g = self.cantidad * 5.2
            self.precio_usd_g = float(self.precio_usd) / 5.2
        #madera-caoba id(10) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 10 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-cortez id(11) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 11 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-roble id(12) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 12 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-melina id(13) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 13 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-granadillo id(14) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 14 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-leña id(14) unidad Carga (11) Conversión 1 carga =*0.1 m3
        if self.productos == 15 and self.unidad == 11:
            self.cantidad_g = self.cantidad * 0.1
            self.precio_usd_g = float(self.precio_usd) / 0.1
        super(DatosCosecha, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos de cosecha'
