# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db.models import Avg, Sum, F, Value as V
from django.db.models import Q
from django.core.mail import EmailMessage
from django.template import Context
from django.template.loader import get_template
from django.contrib import messages
from django.db.models.functions import Coalesce

from .forms import ConsultaTransformacionForm, FormularioColabora
from .models import *
from .transformacion_static import *
#from mapeo.models import Persona
import json as simplejson
from itertools import chain
import numpy as np
from collections import OrderedDict, Counter


# Create your views here.

def _queryset_filtrado_sombra(request):
    params = {}

    if 'ciclo' in request.session:
        params['ciclo__nombre'] = request.session['ciclo']

    if 'productor' in request.session:
        params['productor__nombre'] = request.session['productor']

    if 'organizacion_pertenece' in request.session:
        params['productor__organizacion_pertenece'] = request.session['organizacion_pertenece']

    if 'organizacion_apoyo' in request.session:
        params['productor__organizacion_apoyo__in'] = request.session['organizacion_apoyo']

    if 'pais' in request.session:
        params['productor__pais'] = request.session['pais']

    if 'departamento' in request.session:
        params['productor__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['productor__municipio'] = request.session['municipio']

    # if 'comunidad' in request.session:
    #     params['productor__comunidad'] = request.session['comunidad']

    if 'sexo' in request.session:
        params['productor__sexo'] = request.session['sexo']

    if 'parcela' in request.session:
        params['productor__tamanio_parcela__in'] = request.session['parcela']

    # if 'tipologia' in request.session:
    #     params['productor__productor__tipologia'] = request.session['tipologia']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    print params

    return FichaPodaSombra.objects.filter(**params)

def consulta_transformacion(request, template='transformacion/consultaTransformacion.html'):
    if request.method == 'POST':
        form = ConsultaTransformacionForm(request.POST)
        if form.is_valid():
            request.session['ciclo'] = form.cleaned_data['ciclo']
            request.session['productor'] = form.cleaned_data['productor']
            request.session['organizacion_pertenece'] = form.cleaned_data['organizacion_pertenece']
            request.session['organizacion_apoyo'] = form.cleaned_data['organizacion_apoyo']
            request.session['pais'] = form.cleaned_data['pais']
            request.session['departamento'] = form.cleaned_data['departamento']
            request.session['municipio'] = form.cleaned_data['municipio']
            #request.session['comunidad'] = form.cleaned_data['comunidad']
            request.session['sexo'] = form.cleaned_data['sexo']
            request.session['parcela'] = form.cleaned_data['parcela']
            centinela = 1
        else:
            centinela = 0
    else:
        form = ConsultaTransformacionForm()
        centinela = 0

        if 'ciclo' in request.session:
            try:
                #del request.session['fecha']
                del request.session['ciclo']
                del request.session['productor']
                del request.session['organizacion_pertenece']
                del request.session['organizacion_apoyo']
                del request.session['pais']
                del request.session['departamento']
                del request.session['municipio']
                #del request.session['comunidad']
                del request.session['sexo']
                del request.session['parcela']
            except:
                pass

    return render(request, template, {'form': form, 'centinela': centinela})


#---------------- salidas sombra -----------------------------

def analisis_sombra(request, template="transformacion/sombra/analisis_sombra.html"):
    filtro = _queryset_filtrado_sombra(request)

    CHOICE_DENSIDAD = ( (1,'Alta'),
                        (2,'Adecuada'),
                        (3,'Baja'),
                      )

    CHOICE_COPA = ((1,'Ancha'),
                   (2,'Adecuada'),
                   (3,'Angosta'),
                )

    CHOICE_ARREGLO = ((1, 'Uniforme'), (2, 'Desuniforme'))
    CHOICE_HOJARASCA = ((1, 'Suficiente'), (2, 'No Suficiente'))
    CHOICE_HOJARASCA_CALIDAD = ((1, 'Rico en nutrientes'), (2, 'Pobre en nutriente'))
    CHOICE_COMPETENCIA = ((1, 'Fuerte'), (2, 'Mediana'), (3, 'Leve'))
    CHOICE_PROBLEMA = (
                        (1,'Cobertura'),
                        (2,'Mal arreglo'),
                        (3,'Competencia'),
                        (4,'Densidad Tipo de árboles'),
                        (5,'Ninguno')
                    )

    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()

        dict_densidad = {}
        for densidad in CHOICE_DENSIDAD:
            conteo = filtro.filter(anio = x[0],analisissombra__densidad=densidad[0]).count()
            dict_densidad[densidad[1]] = conteo

        dict_copa = {}
        for copa in CHOICE_COPA:
            conteo = filtro.filter(anio = x[0],analisissombra__forma_copa=copa[0]).count()
            dict_copa[copa[1]] = conteo

        dict_arreglo = {}
        for arreglo in CHOICE_ARREGLO:
            conteo = filtro.filter(anio = x[0],analisissombra__arreglo=arreglo[0]).count()
            dict_arreglo[arreglo[1]] = conteo

        dict_hojarasca = {}
        for hoja in CHOICE_HOJARASCA:
            conteo = filtro.filter(anio = x[0],analisissombra__hojarasca=hoja[0]).count()
            dict_hojarasca[hoja[1]] = conteo

        dict_calidad_hojarasca = {}
        for hoja in CHOICE_HOJARASCA_CALIDAD:
            conteo = filtro.filter(anio = x[0],analisissombra__calidad_hojarasca=hoja[0]).count()
            dict_calidad_hojarasca[hoja[1]] = conteo

        dict_competencia = {}
        for obj in CHOICE_COMPETENCIA:
            conteo = filtro.filter(anio = x[0],analisissombra__competencia=obj[0]).count()
            dict_competencia[obj[1]] = conteo

        dict_problema = {}
        for obj in CHOICE_PROBLEMA:
            conteo = filtro.filter(anio = x[0],analisissombra__Problema=obj[0]).count()
            dict_problema[obj[1]] = conteo

        years[x[1]] = [dict_densidad,dict_copa,dict_arreglo,dict_hojarasca,dict_calidad_hojarasca,
                        dict_competencia,dict_problema,numero_parcelas]


    return render(request, template, locals())


def cobertura_sombra(request, template="transformacion/sombra/cobertura_sombra.html"):
    filtro = _queryset_filtrado_sombra(request)

    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()

        punto1 = Cobertura1.objects.filter(ficha__in=filtro,ficha__anio = x[0]).values_list('cobertura', flat=True)
        punto2 = Cobertura2.objects.filter(ficha__in=filtro,ficha__anio = x[0]).values_list('cobertura', flat=True)
        punto3 = Cobertura3.objects.filter(ficha__in=filtro,ficha__anio = x[0]).values_list('cobertura', flat=True)

        l = list(chain(punto1, punto2, punto3))

        #promedio_todo_puntos = reduce(lambda x, y: x + y, l) / len(l)

        # media arítmetica
        promedio2 = np.mean(l)
        # mediana
        mediana2 = np.median(l)
        # Desviación típica
        desviacion2 = np.std(l)
        #minimo y maximo
        minimo2 = min(l)
        maximo2 = max(l)
        #TODO

        grafo_cobertura = crear_rangos(request, l, minimo2, maximo2, step=10)

        years[x[1]] = numero_parcelas,promedio2,mediana2,desviacion2,minimo2,maximo2,punto1,punto2,punto3,grafo_cobertura

    return render(request, template, locals())

def riqueza_sombra(request, template="transformacion/sombra/sombra_riqueza.html"):
    filtro = _queryset_filtrado_sombra(request)

    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()

        puntos = []
        for obj in filtro.filter(anio=x[0]):
            cnt1 = Punto1.objects.filter(ficha=obj,ficha__anio = x[0]).values_list('especie__id', flat=True)
            cnt2 = Punto2.objects.filter(ficha=obj,ficha__anio = x[0]).values_list('especie__id', flat=True)
            cnt3 = Punto3.objects.filter(ficha=obj,ficha__anio = x[0]).values_list('especie__id', flat=True)
            lista = list(cnt1) + list(cnt2) + list(cnt3)

            reducida_lista = list(set(lista))
            formula_riqueza = len(reducida_lista) #(len(reducida_lista) * 1000) / float(1890)
            puntos.append(formula_riqueza)

        # media arítmetica
        promedio2 = np.mean(puntos)
        # mediana
        mediana2 = np.median(puntos)
        # Desviación típica
        desviacion2 = np.std(puntos)
        #minimo
        minimo = min(puntos)
        #maximo
        maximo = max(puntos)

        #rangos
        grafo_riqueza = crear_rangos(request, puntos, minimo, maximo, step=2)

        years[x[1]] = numero_parcelas,promedio2,mediana2,desviacion2,minimo,maximo,puntos,grafo_riqueza

    return render(request, template, locals())


def densidad_sombra(request, template="transformacion/sombra/densidad_sombra.html"):
    filtro = _queryset_filtrado_sombra(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()

        total_puntos = []
        for obj in filtro.filter(anio=x[0]):
            total1 = Punto1.objects.exclude(especie__id__in=[11,60]).filter(ficha=obj,ficha__anio = x[0]).aggregate(pi=Sum('pequena'),
                                                                       mi=Sum('mediana'),
                                                                       gi=Sum('grande'), )
            try:
                suma_total1 = sum(total1.itervalues())
            except:
                pass

            total2 = Punto2.objects.exclude(especie__id__in=[11,60]).filter(ficha=obj,ficha__anio = x[0]).aggregate(pi=Sum('pequena'),
                                                                       mi=Sum('mediana'),
                                                                       gi=Sum('grande'), )
            try:
                suma_total2 = sum(total2.itervalues())
            except:
                pass

            total3 = Punto3.objects.exclude(especie__id__in=[11,60]).filter(ficha=obj,ficha__anio = x[0]).aggregate(pi=Sum('pequena'),
                                                                       mi=Sum('mediana'),
                                                                       gi=Sum('grande'), )
            try:
                suma_total3 = sum(total3.itervalues())
            except:
                suma_total3 = 0

            try:
                gran_suma = suma_total1 + suma_total2 + suma_total3
            except:
                gran_suma = 0

            try:
                densidad_total = (gran_suma  * float(10000)) / float(1890)
            except:
                densidad_total = 0
            #print "gran suma: %s - encuesta: %s " % (densidad_total, obj)

            total_puntos.append(densidad_total)
        # media arítmetica
        promedio2 = np.mean(total_puntos)
        # mediana
        mediana2 = np.median(total_puntos)
        # Desviación típica
        desviacion2 = np.std(total_puntos)
        #minimo
        minimo = min(total_puntos)
        #maximo
        maximo = max(total_puntos)
        #rangos
        grafo_densidad = crear_rangos(request, total_puntos, minimo, maximo, step=15)

        years[x[1]] = numero_parcelas,promedio2,mediana2,desviacion2,minimo,maximo,total_puntos,grafo_densidad

    return render(request, template, locals())

def acciones_sombra(request, template="transformacion/sombra/acciones_sombra.html"):
    filtro = _queryset_filtrado_sombra(request)

    CHOICE_ACCIONES_SOMBRA = (
        (1, 'Reducir la sombra'),
        (2, 'Aumentar la sombra'),
        (3, 'Ninguna'),)

    CHOICE_PODA = (
        (1, 'Si'),

    )

    CHOICE_HERRAMIENTA = (
        (1, 'Si'),
        (2, 'No'),

    )

    CHOICE_TODO = (
        (1, 'En todo la parcela '),
        (2, 'Solo en una parte de la parcela'),
    )

    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        dict_acciones = {}
        for obj in CHOICE_ACCIONES_SOMBRA:
            cnt = filtro.filter(accionessombra__accion=obj[0],anio = x[0]).count()
            dict_acciones[obj[1]] = (cnt/float(len(filtro))) * 100


        dict_reducir_poda = {}
        dict_reducir_eliminando = {}
        VAR_REDUCIR = filtro.filter(accionessombra__accion=1,anio = x[0]).count()
        for obj in CHOICE_PODA:
            cnt_poda = filtro.filter(reducirsombra__poda=obj[0],anio = x[0]).count()
            cnt_elim = filtro.filter(reducirsombra__eliminando=obj[0],anio = x[0]).count()
            try:
                dict_reducir_poda[obj[1]] = (float(cnt_poda)/float(VAR_REDUCIR)) * 100
            except ZeroDivisionError:
                dict_reducir_poda[obj[1]] = 0
            try:
                dict_reducir_eliminando[obj[1]] = (cnt_elim/float(VAR_REDUCIR)) * 100
            except ZeroDivisionError:
                dict_reducir_eliminando[obj[1]] = 0

        dict_todo = {}
        for obj in CHOICE_TODO:
            cnt = filtro.filter(reducirsombra__todo=obj[0],anio = x[0]).count()
            try:
                dict_todo[obj[1]] = (cnt/float(VAR_REDUCIR)) * 100
            except ZeroDivisionError:
                dict_todo[obj[1]] = 0

        dict_aumentar_poda = {}
        dict_aumentar_eliminando = {}
        VAR_AUMENTAR = filtro.filter(accionessombra__accion=2).count()
        for obj in CHOICE_PODA:
            cnt_sembra = filtro.filter(aumentarsombra__sembrando=obj[0],anio = x[0]).count()
            cnt_cambia = filtro.filter(aumentarsombra__cambiando=obj[0],anio = x[0]).count()

            try:
                dict_aumentar_poda[obj[1]] = (cnt_sembra/float(VAR_AUMENTAR)) * 100
            except:
                dict_aumentar_poda[obj[1]] = 0
            try:
                dict_aumentar_eliminando[obj[1]] = (cnt_cambia/float(VAR_AUMENTAR)) * 100
            except:
                dict_aumentar_eliminando[obj[1]] = 0

        dict_aumentar_todo = {}
        for obj in CHOICE_TODO:
            cnt = filtro.filter(aumentarsombra__todo=obj[0],anio = x[0]).count()
            try:
                dict_aumentar_todo[obj[1]] = (cnt/float(VAR_REDUCIR)) * 100
            except ZeroDivisionError:
                dict_aumentar_todo[obj[1]] = 0

        dict_manejo_herramienta = {}
        dict_manejo_formacion = {}
        for obj in CHOICE_HERRAMIENTA:
            cnt_herra = filtro.filter(manejosombra__herramientas=obj[0],anio = x[0]).count()
            cnt_forma = filtro.filter(manejosombra__formacion=obj[0],anio = x[0]).count()

            dict_manejo_herramienta[obj[1]] = (cnt_herra/float(len(filtro))) * 100
            dict_manejo_formacion[obj[1]] = (cnt_forma/float(len(filtro))) * 100

        years[x[1]] = [dict_acciones,dict_reducir_poda,dict_reducir_eliminando,dict_todo,dict_aumentar_poda,
                        dict_aumentar_eliminando,dict_aumentar_todo,dict_manejo_herramienta,dict_manejo_formacion]

    return render(request, template, locals())

def change(f):
    if f is None:
        return 0
    else:
        return f

def caracterizacion_sombra(request, template="transformacion/sombra/caracterizacion_sombra.html"):
    filtro = _queryset_filtrado_sombra(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()

        #calculos sobre tipo de especies
        dict_todo_tipo = {}
        for obj in CHOICE_TIPO_PUNTO:
            p1_tipo = Punto1.objects.exclude(especie__id__in=[11,60]).filter(ficha__in=filtro, tipo=obj[0],ficha__anio = x[0]).aggregate(
                                                                                                        pi=Sum('pequena'),
                                                                                                        mi=Sum('mediana'),
                                                                                                        gi=Sum('grande'))
            p2_tipo = Punto2.objects.exclude(especie__id__in=[11,60]).filter(ficha__in=filtro, tipo=obj[0],ficha__anio = x[0]).aggregate(
                                                                                                        pi=Sum('pequena'),
                                                                                                        mi=Sum('mediana'),
                                                                                                        gi=Sum('grande'))
            p3_tipo = Punto3.objects.exclude(especie__id__in=[11,60]).filter(ficha__in=filtro, tipo=obj[0],ficha__anio = x[0]).aggregate(
                                                                                                        pi=Sum('pequena'),
                                                                                                        mi=Sum('mediana'),
                                                                                                        gi=Sum('grande'))
            dict_todo_tipo[obj[1]] = [p1_tipo,p2_tipo,p3_tipo]

        todo_tipo = []
        for k, myLIst in dict_todo_tipo.items():
            pe = [item['pi'] for item in myLIst if item['pi'] is not None]
            me = [item['mi'] for item in myLIst if item['mi'] is not None]
            ga = [item['gi'] for item in myLIst if item['gi'] is not None]
            todo_tipo.append([sum(pe),sum(me),sum(ga)])

        #calculos sobre tipo de copas
        dict_todo_copa = {}
        for obj in CHOICE_TIPO_COPA_PUNTO:
            p1_copa = Punto1.objects.exclude(especie__id__in=[11,60]).filter(ficha__in=filtro, tipo_de_copa=obj[0],ficha__anio = x[0]).aggregate(
                                                                                                        pi=Sum('pequena'),
                                                                                                        mi=Sum('mediana'),
                                                                                                        gi=Sum('grande'))
            p2_copa = Punto2.objects.exclude(especie__id__in=[11,60]).filter(ficha__in=filtro, tipo_de_copa=obj[0],ficha__anio = x[0]).aggregate(
                                                                                                        pi=Sum('pequena'),
                                                                                                        mi=Sum('mediana'),
                                                                                                        gi=Sum('grande'))
            p3_copa = Punto3.objects.exclude(especie__id__in=[11,60]).filter(ficha__in=filtro, tipo_de_copa=obj[0],ficha__anio = x[0]).aggregate(
                                                                                                        pi=Sum('pequena'),
                                                                                                        mi=Sum('mediana'),
                                                                                                        gi=Sum('grande'))
            dict_todo_copa[obj[1]] = [p1_copa,p2_copa,p3_copa]

        todo_copa = []
        for k, myLIst in dict_todo_copa.items():
            pe = [item['pi'] for item in myLIst if item['pi'] is not None]
            me = [item['mi'] for item in myLIst if item['mi'] is not None]
            ga = [item['gi'] for item in myLIst if item['gi'] is not None]
            todo_copa.append([sum(pe),sum(me),sum(ga)])


        #calculos sobre tipo de uso
        dict_todo_uso = {}
        for obj in CHOICE_TIPO_USO_PUNTO:
            p1_uso = Punto1.objects.exclude(especie__id__in=[11,60]).filter(ficha__in=filtro, uso=obj[0],ficha__anio = x[0]).aggregate(
                                                                                                        pi=Sum('pequena'),
                                                                                                        mi=Sum('mediana'),
                                                                                                        gi=Sum('grande'))
            p2_uso = Punto2.objects.exclude(especie__id__in=[11,60]).filter(ficha__in=filtro, uso=obj[0],ficha__anio = x[0]).aggregate(
                                                                                                        pi=Sum('pequena'),
                                                                                                        mi=Sum('mediana'),
                                                                                                        gi=Sum('grande'))
            p3_uso = Punto3.objects.exclude(especie__id__in=[11,60]).filter(ficha__in=filtro, uso=obj[0],ficha__anio = x[0]).aggregate(
                                                                                                        pi=Sum('pequena'),
                                                                                                        mi=Sum('mediana'),
                                                                                                        gi=Sum('grande'))
            dict_todo_uso[obj[1]] = [p1_uso,p2_uso,p3_uso]

        todo_uso = []
        for k, myLIst in dict_todo_uso.items():
            pe = [item['pi'] for item in myLIst if item['pi'] is not None]
            me = [item['mi'] for item in myLIst if item['mi'] is not None]
            ga = [item['gi'] for item in myLIst if item['gi'] is not None]
            todo_uso.append([sum(pe),sum(me),sum(ga)])

        years[x[1]] = [numero_parcelas,dict_todo_tipo,todo_tipo,dict_todo_copa,todo_copa,
                        dict_todo_uso,todo_uso]

    return render(request, template, locals())

def dominancia_sombra(request, template="transformacion/sombra/dominancia_sombra.html"):
    filtro = _queryset_filtrado_sombra(request)

    CUANTO_ESPECIES = Especies.objects.exclude(id__in=[11,60]).count()

    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        dict_especie_todo = OrderedDict()

        for obj in Especies.objects.exclude(id__in=[11,60]):
            cnt_p1 = filtro.filter(punto1__especie=obj,anio = x[0]).aggregate(pi=Sum('punto1__pequena'),
                                                                   mi=Sum('punto1__mediana'),
                                                                   gi=Sum('punto1__grande'))

            cnt_p2 = filtro.filter(punto2__especie=obj,anio = x[0]).aggregate(pi=Sum('punto2__pequena'),
                                                                  mi=Sum('punto2__mediana'),
                                                                  gi=Sum('punto2__grande'))

            cnt_p3 = filtro.filter(punto3__especie=obj,anio = x[0]).aggregate(pi=Sum('punto3__pequena'),
                                                                mi=Sum('punto3__mediana'),
                                                                gi=Sum('punto3__grande'))

            dict_especie_todo[obj] = [cnt_p1,cnt_p2,cnt_p3]

        todo = {}
        SUMA_TOTAL_ESPECIE = 0
        suma_total = 0
        for k, myLIst in dict_especie_todo.items():
            pe = [item['pi'] for item in myLIst if item['pi'] is not None]
            me = [item['mi'] for item in myLIst if item['mi'] is not None]
            ga = [item['gi'] for item in myLIst if item['gi'] is not None]
            suma_total = sum([sum(pe),sum(me),sum(ga)])
            if suma_total > 0:
                todo[k] = suma_total
                SUMA_TOTAL_ESPECIE += suma_total

        dominancia_todo = sorted(todo.iteritems(), key=lambda (k,v): (v,k), reverse=True)

        years[x[1]] = SUMA_TOTAL_ESPECIE,dominancia_todo

    # for obj in dominancia_todo:
    #     print "%s --> %s" % (obj[0], obj[1])

    return render(request, template, locals())

def dimensiones_sombra(request, template="transformacion/sombra/dimenciones_especies_sombra.html"):
    filtro = _queryset_filtrado_sombra(request)

    if request.GET.get('usos'):
        uso = request.GET['usos']
        MODELO_ESPECIES = Especies.objects.exclude(id__in=[11,60]).filter(tipo_uso__contains=uso)
    else:
        MODELO_ESPECIES = Especies.objects.exclude(id__in=[11,60])

    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()

        altura_p1 = []
        diametro_p1 = []
        anchura_p1 = []

        for obj in MODELO_ESPECIES:
            conteo = filtro.filter(punto1__especie=obj,anio = x[0]).count()
            cnt_p1 = filtro.filter(punto1__especie=obj,anio = x[0]).aggregate(pi=Sum('punto1__pequena'),
                                                                   mi=Sum('punto1__mediana'),
                                                                   gi=Sum('punto1__grande'))
            if conteo > 0:
                for k,v in cnt_p1.items():
                    if v > 0:
                        if k == 'pi':
                            alti = [obj.p_altura] * int(v)
                            diam = [obj.p_diametro] * int(v)
                            anch = [obj.p_ancho] * int(v)
                            altura_p1.append(alti)
                            diametro_p1.append(diam)
                            anchura_p1.append(anch)
                        if k == 'mi':
                            alti = [obj.m_altura] * int(v)
                            diam = [obj.m_diametro] * int(v)
                            anch = [obj.m_ancho] * int(v)
                            altura_p1.append(alti)
                            diametro_p1.append(diam)
                            anchura_p1.append(anch)
                        if k == "gi":
                            alti = [obj.g_altura] * int(v)
                            diam = [obj.g_diametro] * int(v)
                            anch = [obj.g_ancho] * int(v)
                            altura_p1.append(alti)
                            diametro_p1.append(diam)
                            anchura_p1.append(anch)

        altura_p2 = []
        diametro_p2 = []
        anchura_p2 = []

        for obj in MODELO_ESPECIES:
            conteo = filtro.filter(punto2__especie=obj,anio = x[0]).count()
            cnt_p2 = filtro.filter(punto2__especie=obj,anio = x[0]).aggregate(pi=Sum('punto2__pequena'),
                                                                   mi=Sum('punto2__mediana'),
                                                                   gi=Sum('punto2__grande'))
            if conteo > 0:
                for k,v in cnt_p2.items():
                    if v > 0:
                        if k == 'pi':
                            alti = [obj.p_altura] * int(v)
                            diam = [obj.p_diametro] * int(v)
                            anch = [obj.p_ancho] * int(v)
                            altura_p2.append(alti)
                            diametro_p2.append(diam)
                            anchura_p2.append(anch)
                        if k == 'mi':
                            alti = [obj.m_altura] * int(v)
                            diam = [obj.m_diametro] * int(v)
                            anch = [obj.m_ancho] * int(v)
                            altura_p2.append(alti)
                            diametro_p2.append(diam)
                            anchura_p2.append(anch)
                        if k == "gi":
                            alti = [obj.g_altura] * int(v)
                            diam = [obj.g_diametro] * int(v)
                            anch = [obj.g_ancho] * int(v)
                            altura_p2.append(alti)
                            diametro_p2.append(diam)
                            anchura_p2.append(anch)

        altura_p3 = []
        diametro_p3 = []
        anchura_p3 = []

        for obj in MODELO_ESPECIES:
            conteo = filtro.filter(punto3__especie=obj,anio = x[0]).count()
            cnt_p3 = filtro.filter(punto3__especie=obj,anio = x[0]).aggregate(pi=Sum('punto3__pequena'),
                                                                   mi=Sum('punto3__mediana'),
                                                                   gi=Sum('punto3__grande'))
            if conteo > 0:
                for k,v in cnt_p3.items():
                    if v > 0:
                        if k == 'pi':
                            alti = [obj.p_altura] * int(v)
                            diam = [obj.p_diametro] * int(v)
                            anch = [obj.p_ancho] * int(v)
                            altura_p3.append(alti)
                            diametro_p3.append(diam)
                            anchura_p3.append(anch)
                        if k == 'mi':
                            alti = [obj.m_altura] * int(v)
                            diam = [obj.m_diametro] * int(v)
                            anch = [obj.m_ancho] * int(v)
                            altura_p3.append(alti)
                            diametro_p3.append(diam)
                            anchura_p3.append(anch)
                        if k == "gi":
                            alti = [obj.g_altura] * int(v)
                            diam = [obj.g_diametro] * int(v)
                            anch = [obj.g_ancho] * int(v)
                            altura_p3.append(alti)
                            diametro_p3.append(diam)
                            anchura_p3.append(anch)

        altura_total = altura_p1 + altura_p2 + altura_p3
        diametro_total = diametro_p1 + diametro_p2 + diametro_p3
        anchura_total = anchura_p1 + anchura_p2 + anchura_p3

        #con esto trabajo estan las listas completas
        try:
            todas_alturas = list(chain.from_iterable(altura_total))
        except:
            todas_alturas = [0]
        try:
            todas_diametro = list(chain.from_iterable(diametro_total))
        except:
            todas_diametro = [0]
        try:
            todas_anchura = list(chain.from_iterable(anchura_total))
        except:
            todas_anchura = [0]

        #promedio, rango, desviacion estandar, media de altura
        try:
            promedio_altura = np.mean(todas_alturas)
        except:
            promedio_altura = np.mean([0])
        try:
            desviacion_altura = np.std(todas_alturas)
        except:
            desviacion_altura = np.std([0])
        try:
            media_altura = np.median(todas_alturas)
        except:
            media_altura = np.median([0])
        try:
            minimo_altura = min(todas_alturas)
        except:
            minimo_altura = min([0])
        try:
            maximo_altura = max(todas_alturas)
        except:
            maximo_altura = max([0])

        try:
            grafo_altura = crear_rangos(request, todas_alturas, minimo_altura, maximo_altura, step=3)
        except:
            grafo_altura = 0

        #promedio, rango, desviacion estandar, media de diametro
        try:
            promedio_diametro = np.mean(todas_diametro)
            desviacion_diametro = np.std(todas_diametro)
            media_diametro = np.median(todas_diametro)
            minimo_diametro = min(todas_diametro)
            maximo_diametro = max(todas_diametro)

            grafo_diametro = crear_rangos(request, todas_diametro, minimo_diametro, maximo_diametro, step=16)
        except:
            promedio_diametro = 0
            desviacion_diametro = 0
            media_diametro = 0
            minimo_diametro = 0
            maximo_diametro = 0
            grafo_diametro = crear_rangos(request, todas_diametro, minimo_diametro, maximo_diametro, step=16)
        #promedio, rango, desviacion estandar, media de anchura
        try:
            promedio_anchura = np.mean(todas_anchura)
            desviacion_anchura = np.std(todas_anchura)
            media_anchura = np.median(todas_anchura)
            minimo_anchura = min(todas_anchura)
            maximo_anchura = max(todas_anchura)

            grafo_anchura = crear_rangos(request, todas_anchura, minimo_anchura, maximo_anchura, step=2)
        except:
            promedio_anchura = 0
            desviacion_anchura = 0
            media_anchura = 0
            minimo_anchura = 0
            maximo_anchura = 0
            grafo_anchura = crear_rangos(request, todas_anchura, minimo_anchura, maximo_anchura, step=2)

        years[x[1]] = [numero_parcelas,promedio_altura,desviacion_altura,media_altura,minimo_altura,maximo_altura,
                        promedio_diametro,desviacion_diametro,media_diametro,minimo_diametro,maximo_diametro,
                        promedio_anchura,desviacion_anchura,media_anchura,minimo_anchura,maximo_anchura,
                        grafo_altura,grafo_diametro,grafo_anchura]


    return render(request, template, locals())
#----------------- fin salidas de sombra -------------------------

#----------------- salidas de poda -------------------------

def altura_poda(request, template="transformacion/poda/altura_poda.html"):
    filtro = _queryset_filtrado_sombra(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = {}

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()

        altura1 = []
        altura2 = []
        altura3 = []
        for obj in filtro.filter(anio=x[0]):
            cont1 = PodaPunto1.objects.filter(ficha=obj,plantas=1,ficha__anio = x[0]).aggregate(uno=Sum('uno'),
                                                                   dos=Sum('dos'),
                                                                   tres=Sum('tres'),
                                                                   cuatro=Sum('cuatro'),
                                                                   cinco=Sum('cinco'),
                                                                   seis=Sum('seis'),
                                                                   siete=Sum('siete'),
                                                                   ocho=Sum('ocho'),
                                                                   nueve=Sum('nueve'),
                                                                   dies=Sum('diez'),
                                                                   ).values()
            try:
                suma_cont1 = sum(cont1) / float(10)
                altura1.append(suma_cont1)
            except:
                pass
                #suma_cont1 = 0
            #altura2
            cont2 = PodaPunto2.objects.filter(ficha=obj,plantas=1,ficha__anio = x[0]).aggregate(uno=Sum('uno'),
                                                                   dos=Sum('dos'),
                                                                   tres=Sum('tres'),
                                                                   cuatro=Sum('cuatro'),
                                                                   cinco=Sum('cinco'),
                                                                   seis=Sum('seis'),
                                                                   siete=Sum('siete'),
                                                                   ocho=Sum('ocho'),
                                                                   nueve=Sum('nueve'),
                                                                   dies=Sum('diez'),
                                                                   ).values()
            try:
                suma_cont2 = sum(cont2) / float(10)
                altura2.append(suma_cont2)
            except:
                pass
                #suma_cont2 = 0

            
            #altura 3
            cont3 = PodaPunto3.objects.filter(ficha=obj,plantas=1,ficha__anio = x[0]).aggregate(uno=Sum('uno'),
                                                                   dos=Sum('dos'),
                                                                   tres=Sum('tres'),
                                                                   cuatro=Sum('cuatro'),
                                                                   cinco=Sum('cinco'),
                                                                   seis=Sum('seis'),
                                                                   siete=Sum('siete'),
                                                                   ocho=Sum('ocho'),
                                                                   nueve=Sum('nueve'),
                                                                   dies=Sum('diez'),
                                                                   ).values()
            try:
                suma_cont3 = sum(cont3) / float(10)
                altura3.append(suma_cont3)
            except:
                pass
                #suma_cont3 = 0

            

        altura_total = altura1 + altura2 + altura3
        #promedio, rango, desviacion estandar, media de altura
        promedio_altura = np.mean(altura_total)
        desviacion_altura = np.std(altura_total)
        media_altura = np.median(altura_total)
        minimo_altura = min(altura_total)
        maximo_altura = max(altura_total)

        grafo_altura = crear_rangos(request, altura_total, minimo_altura, maximo_altura, step=2)

        years[x[1]] = [numero_parcelas,promedio_altura,media_altura,desviacion_altura,minimo_altura,maximo_altura,
                        altura1,altura2,altura3,grafo_altura]

    return render(request, template, locals())

def largo_poda(request, template="transformacion/poda/largo_poda.html"):
    filtro = _queryset_filtrado_sombra(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = {}

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()

        altura1 = []
        altura2 = []
        altura3 = []
        for obj in filtro.filter(anio=x[0]):
            cont1 = PodaPunto1.objects.filter(ficha=obj,plantas=3,ficha__anio = x[0]).aggregate(uno=Sum('uno'),
                                                                   dos=Sum('dos'),
                                                                   tres=Sum('tres'),
                                                                   cuatro=Sum('cuatro'),
                                                                   cinco=Sum('cinco'),
                                                                   seis=Sum('seis'),
                                                                   siete=Sum('siete'),
                                                                   ocho=Sum('ocho'),
                                                                   nueve=Sum('nueve'),
                                                                   dies=Sum('diez'),
                                                                   ).values()
            try:
                suma_cont1 = sum(cont1) / float(10)
                altura1.append(suma_cont1)
            except:
                pass
                #suma_cont1 = 0
            #altura2
            cont2 = PodaPunto2.objects.filter(ficha=obj,plantas=3,ficha__anio = x[0]).aggregate(uno=Sum('uno'),
                                                                   dos=Sum('dos'),
                                                                   tres=Sum('tres'),
                                                                   cuatro=Sum('cuatro'),
                                                                   cinco=Sum('cinco'),
                                                                   seis=Sum('seis'),
                                                                   siete=Sum('siete'),
                                                                   ocho=Sum('ocho'),
                                                                   nueve=Sum('nueve'),
                                                                   dies=Sum('diez'),
                                                                   ).values()
            try:
                suma_cont2 = sum(cont2) / float(10)
                altura2.append(suma_cont2)
            except:
                pass
                #suma_cont2 = 0

            
            #altura 3
            cont3 = PodaPunto3.objects.filter(ficha=obj,plantas=3,ficha__anio = x[0]).aggregate(uno=Sum('uno'),
                                                                   dos=Sum('dos'),
                                                                   tres=Sum('tres'),
                                                                   cuatro=Sum('cuatro'),
                                                                   cinco=Sum('cinco'),
                                                                   seis=Sum('seis'),
                                                                   siete=Sum('siete'),
                                                                   ocho=Sum('ocho'),
                                                                   nueve=Sum('nueve'),
                                                                   dies=Sum('diez'),
                                                                   ).values()
            try:
                suma_cont3 = sum(cont3) / float(10)
                altura3.append(suma_cont3)
            except:
                pass
                #suma_cont3 = 0

            

        altura_total = altura1 + altura2 + altura3
        #promedio, rango, desviacion estandar, media de altura
        promedio_altura = np.mean(altura_total)
        desviacion_altura = np.std(altura_total)
        media_altura = np.median(altura_total)
        minimo_altura = min(altura_total)
        maximo_altura = max(altura_total)

        grafo_altura = crear_rangos(request, altura_total, minimo_altura, maximo_altura, step=2)

        years[x[1]] = [numero_parcelas,promedio_altura,media_altura,desviacion_altura,minimo_altura,maximo_altura,
                        altura1,altura2,altura3,grafo_altura]

    return render(request, template, locals())

def ancho_poda(request, template="transformacion/poda/ancho_poda.html"):
    filtro = _queryset_filtrado_sombra(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()

        altura1 = []
        altura2 = []
        altura3 = []
        for obj in filtro.filter(anio=x[0]):
            cont1 = PodaPunto1.objects.filter(ficha=obj,plantas=2,ficha__anio = x[0]).aggregate(uno=Sum('uno'),
                                                                   dos=Sum('dos'),
                                                                   tres=Sum('tres'),
                                                                   cuatro=Sum('cuatro'),
                                                                   cinco=Sum('cinco'),
                                                                   seis=Sum('seis'),
                                                                   siete=Sum('siete'),
                                                                   ocho=Sum('ocho'),
                                                                   nueve=Sum('nueve'),
                                                                   dies=Sum('diez'),
                                                                   ).values()
            try:
                suma_cont1 = sum(cont1) / float(10)

            except:
                suma_cont1 = 0

            altura1.append(suma_cont1)
            #altura2
            cont2 = PodaPunto2.objects.filter(ficha=obj,plantas=2,ficha__anio = x[0]).aggregate(uno=Sum('uno'),
                                                                   dos=Sum('dos'),
                                                                   tres=Sum('tres'),
                                                                   cuatro=Sum('cuatro'),
                                                                   cinco=Sum('cinco'),
                                                                   seis=Sum('seis'),
                                                                   siete=Sum('siete'),
                                                                   ocho=Sum('ocho'),
                                                                   nueve=Sum('nueve'),
                                                                   dies=Sum('diez'),
                                                                   ).values()
            try:
                suma_cont2 = sum(cont2) / float(10)

            except:
                suma_cont2 = 0

            altura2.append(suma_cont2)
            #altura 3
            cont3 = PodaPunto3.objects.filter(ficha=obj,plantas=2,ficha__anio = x[0]).aggregate(uno=Sum('uno'),
                                                                   dos=Sum('dos'),
                                                                   tres=Sum('tres'),
                                                                   cuatro=Sum('cuatro'),
                                                                   cinco=Sum('cinco'),
                                                                   seis=Sum('seis'),
                                                                   siete=Sum('siete'),
                                                                   ocho=Sum('ocho'),
                                                                   nueve=Sum('nueve'),
                                                                   dies=Sum('diez'),
                                                                   ).values()
            try:
                suma_cont3 = sum(cont3) / float(10)

            except:
                suma_cont3 = 0

            altura3.append(suma_cont3)

        altura_total = altura1 + altura2 + altura3
        #promedio, rango, desviacion estandar, media de altura
        promedio_altura = np.mean(altura_total)
        desviacion_altura = np.std(altura_total)
        media_altura = np.median(altura_total)
        minimo_altura = min(altura_total)
        maximo_altura = max(altura_total)

        grafo_altura = crear_rangos(request, altura_total, minimo_altura, maximo_altura, step=2)

        years[x[1]] = [numero_parcelas,promedio_altura,media_altura,desviacion_altura,minimo_altura,maximo_altura,
                        altura1,altura2,altura3,grafo_altura]


    return render(request, template, locals())

def produccion_poda(request, template="transformacion/poda/produccion_poda.html"):
    filtro = _queryset_filtrado_sombra(request)

    CHOICE_PRODUCCION = (
        (1, 'Alta'),
        (2, 'Media'),
        (3, 'Baja'),
    )

    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()
        nivel = {}
        for obj in CHOICE_PRODUCCION:
            uno1 = filtro.filter(podapunto1c__plantas=1,podapunto1c__uno=obj[0],anio = x[0]).count()
            dos1 = filtro.filter(podapunto1c__plantas=1,podapunto1c__dos=obj[0],anio = x[0]).count()
            tres1 = filtro.filter(podapunto1c__plantas=1,podapunto1c__tres=obj[0],anio = x[0]).count()
            cuatro1 = filtro.filter(podapunto1c__plantas=1,podapunto1c__cuatro=obj[0],anio = x[0]).count()
            cinco1 = filtro.filter(podapunto1c__plantas=1,podapunto1c__cinco=obj[0],anio = x[0]).count()
            seis1 = filtro.filter(podapunto1c__plantas=1,podapunto1c__seis=obj[0],anio = x[0]).count()
            siete1 = filtro.filter(podapunto1c__plantas=1,podapunto1c__siete=obj[0],anio = x[0]).count()
            ocho1 = filtro.filter(podapunto1c__plantas=1,podapunto1c__ocho=obj[0],anio = x[0]).count()
            nueve1 = filtro.filter(podapunto1c__plantas=1,podapunto1c__nueve=obj[0],anio = x[0]).count()
            diez1 = filtro.filter(podapunto1c__plantas=1,podapunto1c__diez=obj[0],anio = x[0]).count()
            suma_punto1 = uno1 + dos1 + tres1 + cuatro1 + cinco1 + seis1 + siete1 + ocho1 + nueve1 + diez1

            uno2 = filtro.filter(podapunto2c__plantas=1,podapunto2c__uno=obj[0],anio = x[0]).count()
            dos2 = filtro.filter(podapunto2c__plantas=1,podapunto2c__dos=obj[0],anio = x[0]).count()
            tres2 = filtro.filter(podapunto2c__plantas=1,podapunto2c__tres=obj[0],anio = x[0]).count()
            cuatro2 = filtro.filter(podapunto2c__plantas=1,podapunto2c__cuatro=obj[0],anio = x[0]).count()
            cinco2 = filtro.filter(podapunto2c__plantas=1,podapunto2c__cinco=obj[0],anio = x[0]).count()
            seis2 = filtro.filter(podapunto2c__plantas=1,podapunto2c__seis=obj[0],anio = x[0]).count()
            siete2 = filtro.filter(podapunto2c__plantas=1,podapunto2c__siete=obj[0],anio = x[0]).count()
            ocho2 = filtro.filter(podapunto2c__plantas=1,podapunto2c__ocho=obj[0],anio = x[0]).count()
            nueve2 = filtro.filter(podapunto2c__plantas=1,podapunto2c__nueve=obj[0],anio = x[0]).count()
            diez2 = filtro.filter(podapunto2c__plantas=1,podapunto2c__diez=obj[0],anio = x[0]).count()
            suma_punto2 = uno2 + dos2 + tres2 + cuatro2 + cinco2 + seis2 + siete2 + ocho2 + nueve2 + diez2

            uno3 = filtro.filter(podapunto3c__plantas=1,podapunto3c__uno=obj[0],anio = x[0]).count()
            dos3 = filtro.filter(podapunto3c__plantas=1,podapunto3c__dos=obj[0],anio = x[0]).count()
            tres3 = filtro.filter(podapunto3c__plantas=1,podapunto3c__tres=obj[0],anio = x[0]).count()
            cuatro3 = filtro.filter(podapunto3c__plantas=1,podapunto3c__cuatro=obj[0],anio = x[0]).count()
            cinco3 = filtro.filter(podapunto3c__plantas=1,podapunto3c__cinco=obj[0],anio = x[0]).count()
            seis3 = filtro.filter(podapunto3c__plantas=1,podapunto3c__seis=obj[0],anio = x[0]).count()
            siete3 = filtro.filter(podapunto3c__plantas=1,podapunto3c__siete=obj[0],anio = x[0]).count()
            ocho3 = filtro.filter(podapunto3c__plantas=1,podapunto3c__ocho=obj[0],anio = x[0]).count()
            nueve3 = filtro.filter(podapunto3c__plantas=1,podapunto3c__nueve=obj[0],anio = x[0]).count()
            diez3 = filtro.filter(podapunto3c__plantas=1,podapunto3c__diez=obj[0],anio = x[0]).count()
            suma_punto3 = uno3 + dos3 + tres3 + cuatro3 + cinco3 + seis3 + siete3 + ocho3 + nueve3 + diez3

            totales_suma = suma_punto1 + suma_punto2 + suma_punto3

            nivel[obj[1]] = totales_suma

        years[x[1]] = numero_parcelas,nivel


    return render(request, template, locals())

def atributos_poda(request, template="transformacion/poda/atributos_poda.html"):
    filtro = _queryset_filtrado_sombra(request)

    CHOICE_SI_NO = (
        (1, 'Si'),
        (2, 'No'),
    )

    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        atributos = {}
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        for obj in CHOICE_PLANTAS2:
            atributos[obj[1]] = {}

            for x in CHOICE_SI_NO:
                uno1 = filtro.filter(podapunto1b__plantas=obj[0],podapunto1b__uno=x[0],anio = anio[0]).count()
                dos1 = filtro.filter(podapunto1b__plantas=obj[0],podapunto1b__dos=x[0],anio = anio[0]).count()
                tres1 = filtro.filter(podapunto1b__plantas=obj[0],podapunto1b__tres=x[0],anio = anio[0]).count()
                cuatro1 = filtro.filter(podapunto1b__plantas=obj[0],podapunto1b__cuatro=x[0],anio = anio[0]).count()
                cinco1 = filtro.filter(podapunto1b__plantas=obj[0],podapunto1b__cinco=x[0],anio = anio[0]).count()
                seis1 = filtro.filter(podapunto1b__plantas=obj[0],podapunto1b__seis=x[0],anio = anio[0]).count()
                siete1 = filtro.filter(podapunto1b__plantas=obj[0],podapunto1b__siete=x[0],anio = anio[0]).count()
                ocho1 = filtro.filter(podapunto1b__plantas=obj[0],podapunto1b__ocho=x[0],anio = anio[0]).count()
                nueve1 = filtro.filter(podapunto1b__plantas=obj[0],podapunto1b__nueve=x[0],anio = anio[0]).count()
                diez1 = filtro.filter(podapunto1b__plantas=obj[0],podapunto1b__diez=x[0],anio = anio[0]).count()
                suma_punto1 = uno1 + dos1 + tres1 + cuatro1 + cinco1 + seis1 + siete1 + ocho1 + nueve1 + diez1

                uno2 = filtro.filter(podapunto2b__plantas=obj[0],podapunto2b__uno=x[0],anio = anio[0]).count()
                dos2 = filtro.filter(podapunto2b__plantas=obj[0],podapunto2b__dos=x[0],anio = anio[0]).count()
                tres2 = filtro.filter(podapunto2b__plantas=obj[0],podapunto2b__tres=x[0],anio = anio[0]).count()
                cuatro2 = filtro.filter(podapunto2b__plantas=obj[0],podapunto2b__cuatro=x[0],anio = anio[0]).count()
                cinco2 = filtro.filter(podapunto2b__plantas=obj[0],podapunto2b__cinco=x[0],anio = anio[0]).count()
                seis2 = filtro.filter(podapunto2b__plantas=obj[0],podapunto2b__seis=x[0],anio = anio[0]).count()
                siete2 = filtro.filter(podapunto2b__plantas=obj[0],podapunto2b__siete=x[0],anio = anio[0]).count()
                ocho2 = filtro.filter(podapunto2b__plantas=obj[0],podapunto2b__ocho=x[0],anio = anio[0]).count()
                nueve2 = filtro.filter(podapunto2b__plantas=obj[0],podapunto2b__nueve=x[0],anio = anio[0]).count()
                diez2 = filtro.filter(podapunto2b__plantas=obj[0],podapunto2b__diez=x[0],anio = anio[0]).count()
                suma_punto2 = uno2 + dos2 + tres2 + cuatro2 + cinco2 + seis2 + siete2 + ocho2 + nueve2 + diez2

                uno3 = filtro.filter(podapunto3b__plantas=obj[0],podapunto3b__uno=x[0],anio = anio[0]).count()
                dos3 = filtro.filter(podapunto3b__plantas=obj[0],podapunto3b__dos=x[0],anio = anio[0]).count()
                tres3 = filtro.filter(podapunto3b__plantas=obj[0],podapunto3b__tres=x[0],anio = anio[0]).count()
                cuatro3 = filtro.filter(podapunto3b__plantas=obj[0],podapunto3b__cuatro=x[0],anio = anio[0]).count()
                cinco3 = filtro.filter(podapunto3b__plantas=obj[0],podapunto3b__cinco=x[0],anio = anio[0]).count()
                seis3 = filtro.filter(podapunto3b__plantas=obj[0],podapunto3b__seis=x[0],anio = anio[0]).count()
                siete3 = filtro.filter(podapunto3b__plantas=obj[0],podapunto3b__siete=x[0],anio = anio[0]).count()
                ocho3 = filtro.filter(podapunto3b__plantas=obj[0],podapunto3b__ocho=x[0],anio = anio[0]).count()
                nueve3 = filtro.filter(podapunto3b__plantas=obj[0],podapunto3b__nueve=x[0],anio = anio[0]).count()
                diez3 = filtro.filter(podapunto3b__plantas=obj[0],podapunto3b__diez=x[0],anio = anio[0]).count()
                suma_punto3 = uno3 + dos3 + tres3 + cuatro3 + cinco3 + seis3 + siete3 + ocho3 + nueve3 + diez3

                total = suma_punto1 + suma_punto2 + suma_punto3

                atributos[obj[1]][x[1]] = total


        years[anio[1]] = numero_parcelas,atributos

    return render(request, template, locals())

def analisis_poda(request, template="transformacion/poda/analisis_poda.html"):
    filtro = _queryset_filtrado_sombra(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()

        problema = {}
        for obj in CHOICES_PROBLEMA_PLANTA:
            cont = AnalisisPoda.objects.filter(ficha__in=filtro, campo1__contains=obj[0],ficha__anio = x[0]).count()
            problema[obj[1]] = cont

        years[x[1]] = numero_parcelas,problema

    return render(request, template, locals())

def tipo_poda(request, template="transformacion/poda/tipo_poda.html"):
    filtro = _queryset_filtrado_sombra(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()

        problema = {}
        for obj in CHOICES_TIPO_PODA:
            cont = AnalisisPoda.objects.filter(ficha__in=filtro, campo2__contains=obj[0],ficha__anio = x[0]).count()
            problema[obj[1]] = cont

        donde = {}
        for obj in CHOICE_REALIZA_PODA:
            cont = AnalisisPoda.objects.filter(ficha__in=filtro, campo3=obj[0],ficha__anio = x[0]).count()
            donde[obj[1]] = cont

        vigor = {}
        for obj in CHOICE_VIGOR:
            cont = AnalisisPoda.objects.filter(ficha__in=filtro, campo4=obj[0],ficha__anio = x[0]).count()
            vigor[obj[1]] = cont

        years[x[1]] = numero_parcelas,problema,donde,vigor


    return render(request, template, locals())

def acciones_poda(request, template="transformacion/poda/acciones_poda.html"):
    filtro = _queryset_filtrado_sombra(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()

        luz = OrderedDict()
        for obj in CHOICE_ENTRADA_LUZ:
            cont = AnalisisPoda.objects.filter(ficha__in=filtro, campo5=obj[0],ficha__anio = x[0]).count()
            luz[obj[1]] = cont

        meses = OrderedDict()
        for obj in CHOICES_FECHA_PODA:
            cont = AnalisisPoda.objects.filter(ficha__in=filtro, campo6__contains=obj[0],ficha__anio = x[0]).count()
            meses[obj[1]] = cont

        herramienta = OrderedDict()
        formacion = OrderedDict()
        for obj in CHOICES_EQUIPO_PODA:
            cont = ManejoPoda.objects.filter(ficha__in=filtro, herramientas__contains=obj[0],ficha__anio = x[0]).count()
            herramienta[obj[1]] = cont
        for obj in CHOICE_PODA:
            cont1 = ManejoPoda.objects.filter(ficha__in=filtro, formacion=obj[0],ficha__anio = x[0]).count()
            formacion[obj[1]] = cont1

        years[x[1]] = numero_parcelas,luz,meses,herramienta,formacion

    return render(request, template, locals())
# ------------------ fin de poda ------------------

#----------------- inicio de plaga ----------------

def _queryset_filtrado_plaga(request):
    params = {}

    if 'ciclo' in request.session:
        params['ciclo__nombre'] = request.session['ciclo']

    if 'productor' in request.session:
        params['productor__nombre'] = request.session['productor']

    if 'organizacion_pertenece' in request.session:
        params['productor__organizacion_pertenece'] = request.session['organizacion_pertenece']

    if 'organizacion_apoyo' in request.session:
        params['productor__organizacion_apoyo__in'] = request.session['organizacion_apoyo']

    if 'pais' in request.session:
        params['productor__pais'] = request.session['pais']

    if 'departamento' in request.session:
        params['productor__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['productor__municipio'] = request.session['municipio']

    # if 'comunidad' in request.session:
    #     params['productor__comunidad'] = request.session['comunidad']

    if 'sexo' in request.session:
        params['productor__sexo'] = request.session['sexo']

    if 'parcela' in request.session:
        params['productor__tamanio_parcela__in'] = request.session['parcela']

    # if 'tipologia' in request.session:
    #     params['productor__productor__tipologia'] = request.session['tipologia']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    return FichaPisoPlaga.objects.filter(**params)
#----------------- salidas de plaga -------------------------

def historial_plaga(request, template="transformacion/plaga/historial_plaga.html"):
    filtro = _queryset_filtrado_plaga(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()

        plagas = OrderedDict()
        for obj in CHOICE_ENFERMEDADES_CACAOTALES_MULTI:
            cont_visto = filtro.filter(plagasenfermedadvisto__plagas__contains=obj[0],anio = x[0]).count()
            if cont_visto > 0:
                plagas[obj[1]] = [((float(cont_visto)/float(numero_parcelas))*100)]

        promedio_plagas = OrderedDict()
        for obj in CHOICE_ENFERMEDADES_CACAOTALES:
            cont_avg = filtro.filter(plagasenfermedadafectan__plagas=obj[0],anio = x[0]).count()
            if cont_avg > 0:#aggregate(promedio=Avg('plagasenfermedadafectan__promedio'))['promedio']
                promedio_plagas[obj[1]] = [((float(cont_avg)/float(numero_parcelas))*100)]

        mediana_plagas = OrderedDict()
        for obj in CHOICE_ENFERMEDADES_CACAOTALES:
            numeros = filtro.filter(plagasenfermedadafectan__plagas=obj[0],anio = x[0]).aggregate(promedio=Avg('plagasenfermedadafectan__promedio'))['promedio']
            mediana_plagas[obj[1]] = numeros

        years[x[1]] = numero_parcelas,plagas,promedio_plagas,mediana_plagas

    return render(request, template, locals())


def acciones_plaga(request, template="transformacion/plaga/acciones_plaga.html"):
    filtro = _queryset_filtrado_plaga(request)

    CHOICE_ACCIONES_ENFERMEDADES = (
        (1, 'Recuento de plagas'),
        (2, 'Cortar las mazorcas enfermas'),
        (3, 'Abonar las plantas'),
        (4, 'Aplicar Caldos'),
        (5, 'Aplicar Fungicidas'),
        (6, 'Manejo de sombra'),
        (7, 'Podar las plantas de cacao'),
        (8, 'Aplicar venenos para Zompopo'),
        (9, 'Control de Comején'),
        (10, 'Ahuyar Ardillas'),
        (11, 'Otras'),
    )

    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for x in anios:
        numero_parcelas = filtro.filter(anio = x[0]).count()

        acciones_plagas = OrderedDict()
        for obj in CHOICE_ACCIONES_ENFERMEDADES:
            conteo_si = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],anio = x[0]).count()
            avg_veces = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],anio = x[0]).aggregate(promedio=Avg('accionesenfermedad__cuantas_veces'))['promedio']
            numeros = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],anio = x[0]).values_list('accionesenfermedad__cuantas_veces', flat=True)
            try:
                acciones_plagas[obj[1]] = [conteo_si,(float(conteo_si)/float(numero_parcelas)*100),avg_veces,np.std(numeros)]
            except:
                acciones_plagas[obj[1]] = 0

        grafo_momento = OrderedDict()
        for obj in CHOICE_ACCIONES_ENFERMEDADES:
            ene = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],
                                             accionesenfermedad__meses__contains='A',anio = x[0]).count()
            feb = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],
                                             accionesenfermedad__meses__contains='B',anio = x[0]).count()
            mar = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],
                                             accionesenfermedad__meses__contains='C',anio = x[0]).count()
            abr = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],
                                             accionesenfermedad__meses__contains='D',anio = x[0]).count()
            may = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],
                                             accionesenfermedad__meses__contains='E',anio = x[0]).count()
            jun = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],
                                             accionesenfermedad__meses__contains='F',anio = x[0]).count()
            jul = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],
                                             accionesenfermedad__meses__contains='G',anio = x[0]).count()
            ago = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],
                                             accionesenfermedad__meses__contains='H',anio = x[0]).count()
            sep = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],
                                             accionesenfermedad__meses__contains='I',anio = x[0]).count()
            octu = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],
                                             accionesenfermedad__meses__contains='J',anio = x[0]).count()
            nov = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],
                                             accionesenfermedad__meses__contains='K',anio = x[0]).count()
            dic = filtro.filter(accionesenfermedad__plagas_acciones=obj[0],
                                             accionesenfermedad__meses__contains='L',anio = x[0]).count()
            grafo_momento[obj[1]] = [ene,feb,mar,abr,may,jun,jul,ago,sep,octu,nov,dic]

        years[x[1]] = numero_parcelas,acciones_plagas,grafo_momento

    return render(request, template, locals())

def fuente_incidencia_plaga(request, template="transformacion/plaga/fuente_incidencia_plaga.html"):
    filtro = _queryset_filtrado_plaga(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        tabla_incidencia = OrderedDict()

        for obj in CHOICE_OBSERVACION_PUNTO1:
            tabla_incidencia[obj[1]] = {}
            lista_arreglo = []
            contador_si = 0
            for x in filtro:
                punto1 = ObservacionPunto1.objects.filter(ficha=x,planta=obj[0],ficha__anio = anio[0]).aggregate(total=Coalesce(Sum('contador'),0))['total']

                punto2 = ObservacionPunto2.objects.filter(ficha=x,planta=obj[0],ficha__anio = anio[0]).aggregate(total=Coalesce(Sum('contador'),0))['total']

                punto3= ObservacionPunto3.objects.filter(ficha=x,planta=obj[0],ficha__anio = anio[0]).aggregate(total=Coalesce(Sum('contador'),0))['total']
                try:
                    suma_total = punto1 + punto2 + punto3
                except:
                   suma_total = 0
                porcentaje_suma_total = (float(suma_total)/30)*100
                if suma_total >=1:
                    contador_si += 1
                lista_arreglo.append(porcentaje_suma_total)

                tabla_incidencia[obj[1]] = (contador_si,np.mean(lista_arreglo),np.std(lista_arreglo),np.median(lista_arreglo),min(lista_arreglo),max(lista_arreglo))

        years[anio[1]] = numero_parcelas,tabla_incidencia,

    return render(request, template, locals())

def produccion_rendimiento_plaga(request, template="transformacion/plaga/produccion_rendimiento_plaga.html"):
    filtro = _queryset_filtrado_plaga(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        grafo_nivel_produccion = OrderedDict()
        alto1 = filtro.filter(anio = anio[0]).aggregate(total=Sum('observacionpunto1nivel__alta'))['total'] or 0
        alto2 = filtro.filter(anio = anio[0]).aggregate(total=Sum('observacionpunto2nivel__alta'))['total'] or 0
        alto3 = filtro.filter(anio = anio[0]).aggregate(total=Sum('observacionpunto3nivel__alta'))['total'] or 0

        total_alta = alto1 + alto2 + alto3

        media1 = filtro.filter(anio = anio[0]).aggregate(total=Sum('observacionpunto1nivel__media'))['total'] or 0
        media2 = filtro.filter(anio = anio[0]).aggregate(total=Sum('observacionpunto2nivel__media'))['total'] or 0
        media3 = filtro.filter(anio = anio[0]).aggregate(total=Sum('observacionpunto3nivel__media'))['total'] or 0

        total_media = media1 + media2 + media3

        baja1 = filtro.filter(anio = anio[0]).aggregate(total=Sum('observacionpunto1nivel__baja'))['total'] or 0
        baja2 = filtro.filter(anio = anio[0]).aggregate(total=Sum('observacionpunto2nivel__baja'))['total'] or 0
        baja3 = filtro.filter(anio = anio[0]).aggregate(total=Sum('observacionpunto3nivel__baja'))['total'] or 0

        total_baja = baja1 + baja2 + baja3

        try:
            grafo_nivel_produccion['Alta'] = float((total_alta*100))/(float(numero_parcelas)*30)
            grafo_nivel_produccion['Media'] = float((total_media*100))/(float(numero_parcelas)*30)
            grafo_nivel_produccion['Baja'] = float((total_baja*100))/(float(numero_parcelas)*30)
        except ZeroDivisionError:
            grafo_nivel_produccion['Alta'] = 0
            grafo_nivel_produccion['Media'] = 0
            grafo_nivel_produccion['Baja'] = 0

        grafo_monilia = generic_indice_produccion(request, 1,anio[0])
        grafo_mazorca = generic_indice_produccion(request, 2,anio[0])
        grafo_ardillas = generic_indice_produccion(request, 10,anio[0])
        grafo_machete = generic_indice_produccion(request, 3,anio[0])
        grafo_zompopo = generic_indice_produccion(request, 6,anio[0])

        years[anio[1]] = [numero_parcelas,grafo_nivel_produccion,grafo_monilia,grafo_mazorca,grafo_ardillas,
                            grafo_machete,grafo_zompopo]


    return render(request, template, locals())

def generic_indice_produccion(request, tipo=0,anio=0):
    filtro = _queryset_filtrado_plaga(request)
    numero_parcelas = filtro.filter(anio = anio).count()

    grafo_dispercion = []
    grafo_nivel_produccion2 = OrderedDict()
    for x in filtro:
        alto1 = ObservacionPunto1Nivel.objects.filter(ficha=x,ficha__anio = anio).aggregate(total=Sum('alta'))['total'] or 0
        alto2 = ObservacionPunto2Nivel.objects.filter(ficha=x,ficha__anio = anio).aggregate(total=Sum('alta'))['total'] or 0
        alto3 = ObservacionPunto3Nivel.objects.filter(ficha=x,ficha__anio = anio).aggregate(total=Sum('alta'))['total'] or 0
        total_alta = alto1 + alto2 + alto3

        media1 = ObservacionPunto1Nivel.objects.filter(ficha=x,ficha__anio = anio).aggregate(total=Sum('media'))['total'] or 0
        media2 = ObservacionPunto2Nivel.objects.filter(ficha=x,ficha__anio = anio).aggregate(total=Sum('media'))['total'] or 0
        media3 = ObservacionPunto3Nivel.objects.filter(ficha=x,ficha__anio = anio).aggregate(total=Sum('media'))['total'] or 0

        total_media = media1 + media2 + media3

        baja1 = ObservacionPunto1Nivel.objects.filter(ficha=x,ficha__anio = anio).aggregate(total=Sum('baja'))['total'] or 0
        baja2 = ObservacionPunto2Nivel.objects.filter(ficha=x,ficha__anio = anio).aggregate(total=Sum('baja'))['total'] or 0
        baja3 = ObservacionPunto3Nivel.objects.filter(ficha=x,ficha__anio = anio).aggregate(total=Sum('baja'))['total'] or 0

        total_baja = baja1 + baja2 + baja3

        grafo_nivel_produccion2['Alta'] = total_alta
        grafo_nivel_produccion2['Media'] = total_media
        grafo_nivel_produccion2['Baja'] = total_baja

        #------------------------------------------------------
        formula = float((5*grafo_nivel_produccion2['Baja'])+(20*grafo_nivel_produccion2['Media'])+(40*grafo_nivel_produccion2['Alta'])) / float(30)
        #-----------------------------------------------------------

        punto1 = ObservacionPunto1.objects.filter(ficha=x,planta=tipo,ficha__anio = anio).aggregate(total=Sum('contador'))['total'] or 0

        punto2 = ObservacionPunto2.objects.filter(ficha=x,planta=tipo,ficha__anio = anio).aggregate(total=Sum('contador'))['total'] or 0

        punto3= ObservacionPunto3.objects.filter(ficha=x,planta=tipo,ficha__anio = anio).aggregate(total=Sum('contador'))['total'] or 0


        suma_total = punto1 + punto2 + punto3

        #----------------------------------------
        monilia = (float(suma_total)/float(30))*100
        #-----------------------------------------

        grafo_dispercion.append([monilia,formula])

    return grafo_dispercion


def analisis_plaga(request, template="transformacion/plaga/analisis_plaga.html"):
    filtro = _queryset_filtrado_plaga(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        grafo_analisis_plaga = OrderedDict()
        for obj in CHOICE_ENFERMEDADES:
            cont_observada = filtro.filter(problemasprincipales__observadas__contains=obj[0],anio = anio[0]).count()
            cont_principal = filtro.filter(problemasprincipales__principales__contains=obj[0],anio = anio[0]).count()
            grafo_analisis_plaga[obj[1]] = [cont_observada,cont_principal]

        grafo_situacion_plaga = OrderedDict()
        for obj in CHOICE_SITUACION_PLAGAS:
            cont_situacion = filtro.filter(problemasprincipales__situacion=obj[0],anio = anio[0]).count()

            grafo_situacion_plaga[obj[1]] = cont_situacion

        years[anio[1]] = numero_parcelas,grafo_analisis_plaga,grafo_situacion_plaga

    return render(request, template, locals())

def observacion_sombra_poda_plaga(request, template="transformacion/plaga/observacion_sombra_poda_plaga.html"):
    filtro = _queryset_filtrado_plaga(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        grafo_suelo_plaga = OrderedDict()
        for obj in CHOICE_ENFERMEDADES_PUNTO6_1:
            conteo = filtro.filter(punto6plagas__observaciones__contains=obj[0],anio = anio[0]).count()
            grafo_suelo_plaga[obj[1]] = conteo

        grafo_sombra_plaga = OrderedDict()
        for obj in CHOICE_ENFERMEDADES_PUNTO6_2:
            conteo = filtro.filter(punto6plagas__sombra=obj[0],anio = anio[0]).count()
            grafo_sombra_plaga[obj[1]] = conteo

        grafo_poda_plaga = OrderedDict()
        for obj in CHOICE_ENFERMEDADES_PUNTO6_3:
            conteo = filtro.filter(punto6plagas__manejo__contains=obj[0],anio = anio[0]).count()
            grafo_poda_plaga[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,grafo_suelo_plaga,grafo_sombra_plaga,grafo_poda_plaga

    return render(request, template, locals())

def acciones_manejo_plaga(request, template="transformacion/plaga/acciones_manejo_plaga.html"):
    filtro = _queryset_filtrado_plaga(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        tabla_acciones = OrderedDict()
        for obj in CHOICE_ACCIONES_PUNTO7_1:
            conteo_si = filtro.filter(punto7plagas__manejo=obj[0],anio = anio[0]).count()
            conteo_toda = filtro.filter(punto7plagas__manejo=obj[0], punto7plagas__parte=1,anio = anio[0]).count()
            conteo_alguna = filtro.filter(punto7plagas__manejo=obj[0], punto7plagas__parte=2,anio = anio[0]).count()
            tabla_acciones[obj[1]] = [conteo_si, conteo_toda,conteo_alguna]

        grafo_momento = OrderedDict()
        for obj in CHOICE_ACCIONES_PUNTO7_1:
            ene = filtro.filter(punto7plagas__manejo=obj[0],
                                             punto7plagas__meses__contains='A',anio = anio[0]).count()
            feb = filtro.filter(punto7plagas__manejo=obj[0],
                                             punto7plagas__meses__contains='B',anio = anio[0]).count()
            mar = filtro.filter(punto7plagas__manejo=obj[0],
                                             punto7plagas__meses__contains='C',anio = anio[0]).count()
            abr = filtro.filter(punto7plagas__manejo=obj[0],
                                             punto7plagas__meses__contains='D',anio = anio[0]).count()
            may = filtro.filter(punto7plagas__manejo=obj[0],
                                             punto7plagas__meses__contains='E',anio = anio[0]).count()
            jun = filtro.filter(punto7plagas__manejo=obj[0],
                                             punto7plagas__meses__contains='F',anio = anio[0]).count()
            jul = filtro.filter(punto7plagas__manejo=obj[0],
                                             punto7plagas__meses__contains='G',anio = anio[0]).count()
            ago = filtro.filter(punto7plagas__manejo=obj[0],
                                             punto7plagas__meses__contains='H',anio = anio[0]).count()
            sep = filtro.filter(punto7plagas__manejo=obj[0],
                                             punto7plagas__meses__contains='I',anio = anio[0]).count()
            octu = filtro.filter(punto7plagas__manejo=obj[0],
                                             punto7plagas__meses__contains='J',anio = anio[0]).count()
            nov = filtro.filter(punto7plagas__manejo=obj[0],
                                             punto7plagas__meses__contains='K',anio = anio[0]).count()
            dic = filtro.filter(punto7plagas__manejo=obj[0],
                                             punto7plagas__meses__contains='L',anio = anio[0]).count()
            grafo_momento[obj[1]] = [ene,feb,mar,abr,may,jun,jul,ago,sep,octu,nov,dic]

        years[anio[1]] = numero_parcelas,grafo_momento,tabla_acciones

    return render(request, template, locals())

def equipos_formacion_plaga(request, template="transformacion/plaga/equipos_formacion_plaga.html"):
    filtro = _queryset_filtrado_plaga(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        grafo_equipos = OrderedDict()
        for obj in CHOICE_ENFERMEDADES_PUNTO8:
            conteo = filtro.filter(punto8y9plagas__equipos__contains=obj[0],anio = anio[0]).count()
            grafo_equipos[obj[1]] = conteo

        grafo_formacion = OrderedDict()
        for obj in CHOICE_SI_NO:
            conteo = filtro.filter(punto8y9plagas__opcion=obj[0],anio = anio[0]).count()
            grafo_formacion[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,grafo_equipos,grafo_formacion

    return render(request, template, locals())

#----------  fin salidas plagas  --------------------------
#-----------Inicio de piso -----------------------

#----------  SALIDAS DE PISO --------------------------
def estado_piso(request, template="transformacion/piso/estado_piso.html"):
    filtro = _queryset_filtrado_plaga(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        grafo_estado = OrderedDict()
        for obj in CHOICE_PISO1:
            conteo_p1 = filtro.filter(pisopunto1__punto1__contains=obj[0],anio = anio[0]).count()
            conteo_p2 = filtro.filter(pisopunto1__punto2__contains=obj[0],anio = anio[0]).count()
            grafo_estado[obj[1]] = (conteo_p1,conteo_p2)

        grafo_manejo_piso = OrderedDict()
        for obj in CHOICE_PISO3:
            conteo_p1 = filtro.filter(pisopunto3__manejo=obj[0],anio = anio[0]).count()
            grafo_manejo_piso[obj[1]] = conteo_p1

        years[anio[1]] = numero_parcelas,grafo_estado,grafo_manejo_piso

    return render(request, template, locals())

def estado_piso2(request, template="transformacion/piso/estado_manejo_piso.html"):
    filtro = _queryset_filtrado_plaga(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        tabla_manejo_piso = OrderedDict()
        for obj in CHOICE_PISO3:
            generico = filtro.filter(pisopunto3__manejo=obj[0],anio = anio[0])
            if generico.count() >=1:
                arreglo_manejo = generico.values_list('pisopunto3__veces', flat=True)
                tabla_manejo_piso[obj[1]] = (generico.count(),np.mean(arreglo_manejo),
                                                              np.std(arreglo_manejo),np.median(arreglo_manejo),
                                                              min(arreglo_manejo),max(arreglo_manejo))

        grafo_momento = OrderedDict()
        for obj in CHOICE_PISO3:
            ene = filtro.filter(pisopunto3__manejo=obj[0], pisopunto3__meses__contains='A',anio = anio[0]).count()
            feb = filtro.filter(pisopunto3__manejo=obj[0], pisopunto3__meses__contains='B',anio = anio[0]).count()
            mar = filtro.filter(pisopunto3__manejo=obj[0], pisopunto3__meses__contains='C',anio = anio[0]).count()
            abr = filtro.filter(pisopunto3__manejo=obj[0], pisopunto3__meses__contains='D',anio = anio[0]).count()
            may = filtro.filter(pisopunto3__manejo=obj[0], pisopunto3__meses__contains='E',anio = anio[0]).count()
            jun = filtro.filter(pisopunto3__manejo=obj[0], pisopunto3__meses__contains='F',anio = anio[0]).count()
            jul = filtro.filter(pisopunto3__manejo=obj[0], pisopunto3__meses__contains='G',anio = anio[0]).count()
            ago = filtro.filter(pisopunto3__manejo=obj[0], pisopunto3__meses__contains='H',anio = anio[0]).count()
            sep = filtro.filter(pisopunto3__manejo=obj[0], pisopunto3__meses__contains='I',anio = anio[0]).count()
            octu = filtro.filter(pisopunto3__manejo=obj[0], pisopunto3__meses__contains='J',anio = anio[0]).count()
            nov = filtro.filter(pisopunto3__manejo=obj[0], pisopunto3__meses__contains='K',anio = anio[0]).count()
            dic = filtro.filter(pisopunto3__manejo=obj[0], pisopunto3__meses__contains='L',anio = anio[0]).count()
            grafo_momento[obj[1]] = [ene,feb,mar,abr,may,jun,jul,ago,sep,octu,nov,dic]

        grafo_orientacion = OrderedDict()
        for obj in CHOICE_PISO4:
            conteo = filtro.filter(pisopunto4__manejo__contains=obj[0],anio = anio[0]).count()
            grafo_orientacion[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,grafo_momento,grafo_orientacion,tabla_manejo_piso

    return render(request, template, locals())

def orientacion_composicion_piso(request, template="transformacion/piso/orientacion_composicion_piso.html"):
    filtro = _queryset_filtrado_plaga(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        tabla_composicion = OrderedDict()
        for obj in CHOICE_PISO5:
            conteo = filtro.filter(pisopunto5__estado=obj[0],anio = anio[0]).count()
            suma = filtro.filter(pisopunto5__estado=obj[0],anio = anio[0]).aggregate(total=Sum('pisopunto5__conteo'))['total']

            tabla_composicion[obj[1]] = suma

        VAR_TOTAL = 0
        for k,v in tabla_composicion.items():
            try:
                VAR_TOTAL += v
            except:
                VAR_TOTAL = 0

        years[anio[1]] = numero_parcelas,tabla_composicion,VAR_TOTAL

    return render(request, template, locals())

def analisis_piso(request, template="transformacion/piso/analisis_piso.html"):
    filtro = _queryset_filtrado_plaga(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        grafo_competencia = OrderedDict()
        for obj in CHOICE_PISO6_1:
            conteo = filtro.filter(pisopunto6__manejo__contains=obj[0],anio = anio[0]).count()
            grafo_competencia[obj[1]] = conteo

        grafo_cobertura = OrderedDict()
        for obj in CHOICE_PISO6_2:
            conteo = filtro.filter(pisopunto6__estado=obj[0],anio = anio[0]).count()
            grafo_cobertura[obj[1]] = conteo

        grafo_maleza = OrderedDict()
        for obj in CHOICE_PISO6_3:
            conteo = filtro.filter(pisopunto6__maleza__contains=obj[0],anio = anio[0]).count()
            grafo_maleza[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,grafo_competencia,grafo_cobertura,grafo_maleza

    return render(request, template, locals())

def suelo_piso(request, template="transformacion/piso/suelo_piso.html"):
    filtro = _queryset_filtrado_plaga(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        grafo_suelo = OrderedDict()
        for obj in CHOICE_PISO7_1:
            conteo = filtro.filter(pisopunto7__suelo__contains=obj[0],anio = anio[0]).count()
            grafo_suelo[obj[1]] = conteo

        grafo_sombra = OrderedDict()
        for obj in CHOICE_PISO7_2:
            conteo = filtro.filter(pisopunto7__sombra__contains=obj[0],anio = anio[0]).count()
            grafo_sombra[obj[1]] = conteo

        grafo_manejo = OrderedDict()
        for obj in CHOICE_PISO7_3:
            conteo = filtro.filter(pisopunto7__manejo__contains=obj[0],anio = anio[0]).count()
            grafo_manejo[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,grafo_suelo,grafo_sombra,grafo_manejo

    return render(request, template, locals())

def propuesta_piso(request, template="transformacion/piso/propuesta_piso.html"):
    filtro = _queryset_filtrado_plaga(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        tabla_propuesta = OrderedDict()
        for obj in CHOICE_PISO8:
            conteo_piso = filtro.filter(pisopunto8__piso=obj[0],anio = anio[0]).count()
            conteo_toda = filtro.filter(pisopunto8__piso=obj[0], pisopunto8__parte=1,anio = anio[0]).count()
            conteo_alguna = filtro.filter(pisopunto8__piso=obj[0], pisopunto8__parte=2,anio = anio[0]).count()
            tabla_propuesta[obj[1]] = (conteo_piso,conteo_toda,conteo_alguna)

        grafo_manejo = OrderedDict()
        for obj in CHOICE_PISO8:
            ene = filtro.filter(pisopunto8__piso=obj[0], pisopunto8__meses__contains='A',anio = anio[0]).count()
            feb = filtro.filter(pisopunto8__piso=obj[0], pisopunto8__meses__contains='B',anio = anio[0]).count()
            mar = filtro.filter(pisopunto8__piso=obj[0], pisopunto8__meses__contains='C',anio = anio[0]).count()
            abr = filtro.filter(pisopunto8__piso=obj[0], pisopunto8__meses__contains='D',anio = anio[0]).count()
            may = filtro.filter(pisopunto8__piso=obj[0], pisopunto8__meses__contains='E',anio = anio[0]).count()
            jun = filtro.filter(pisopunto8__piso=obj[0], pisopunto8__meses__contains='F',anio = anio[0]).count()
            jul = filtro.filter(pisopunto8__piso=obj[0], pisopunto8__meses__contains='G',anio = anio[0]).count()
            ago = filtro.filter(pisopunto8__piso=obj[0], pisopunto8__meses__contains='H',anio = anio[0]).count()
            sep = filtro.filter(pisopunto8__piso=obj[0], pisopunto8__meses__contains='I',anio = anio[0]).count()
            octu = filtro.filter(pisopunto8__piso=obj[0], pisopunto8__meses__contains='J',anio = anio[0]).count()
            nov = filtro.filter(pisopunto8__piso=obj[0], pisopunto8__meses__contains='K',anio = anio[0]).count()
            dic = filtro.filter(pisopunto8__piso=obj[0], pisopunto8__meses__contains='L',anio = anio[0]).count()
            grafo_manejo[obj[1]] = [ene,feb,mar,abr,may,jun,jul,ago,sep,octu,nov,dic]

        years[anio[1]] = numero_parcelas,grafo_manejo,tabla_propuesta


    return render(request, template, locals())


def equipo_piso(request, template="transformacion/piso/equipo_piso.html"):
    filtro = _queryset_filtrado_plaga(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        grafo_equipo = OrderedDict()
        for obj in CHOICE_PISO10:
            conteo = filtro.filter(pisopunto10__equipo__contains=obj[0],anio = anio[0]).count()
            grafo_equipo[obj[1]] = conteo

        grafo_formacion = OrderedDict()
        for obj in CHOICE_SI_NO:
            conteo = filtro.filter(pisopunto10__formacion=obj[0],anio = anio[0]).count()
            grafo_formacion[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,grafo_equipo,grafo_formacion


    return render(request, template, locals())

#--------- fin de salida de piso  -------------------------
# -------- comienza salidas de cosecha -------------
def _queryset_filtrado_cosecha(request):
    params = {}

    if 'ciclo' in request.session:
        params['ciclo__nombre'] = request.session['ciclo']

    if 'productor' in request.session:
        params['productor__nombre'] = request.session['productor']

    if 'organizacion_pertenece' in request.session:
        params['productor__organizacion_pertenece'] = request.session['organizacion_pertenece']

    if 'organizacion_apoyo' in request.session:
        params['productor__organizacion_apoyo__in'] = request.session['organizacion_apoyo']

    if 'pais' in request.session:
        params['productor__pais'] = request.session['pais']

    if 'departamento' in request.session:
        params['productor__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['productor__municipio'] = request.session['municipio']

    if 'parcela' in request.session:
        params['productor__tamanio_parcela__in'] = request.session['parcela']

    # if 'tipologia' in request.session:
    #     params['productor__productor__tipologia'] = request.session['tipologia']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    print "cosecha"
    print params

    return FichaCosecha.objects.filter(**params)
#----------  SALIDAS DE COSECHA --------------------------

def conversacion_cosecha(request, template="transformacion/cosecha/conversaciones_cosecha.html"):
    filtro = _queryset_filtrado_cosecha(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        dict_conversacion1 = OrderedDict()
        for obj in CHOICE_COSECHA_CONVERSACION_1:
            conteo = filtro.filter(cosechaconversacion1__conversacion1__contains=obj[0],anio = anio[0]).count()
            dict_conversacion1[obj[1]] = conteo

        dict_conversacion2 = OrderedDict()
        for obj in CHOICE_COSECHA_CONVERSACION_2:
            conteo = filtro.filter(cosechaconversacion1__conversacion2__contains=obj[0],anio = anio[0]).count()
            dict_conversacion2[obj[1]] = conteo

        dict_conversacion3 = OrderedDict()
        for obj in CHOICE_COSECHA_CONVERSACION_3:
            conteo = filtro.filter(cosechaconversacion1__conversacion3__contains=obj[0],anio = anio[0]).count()
            dict_conversacion3[obj[1]] = conteo

        dict_conversacion4 = OrderedDict()
        for obj in CHOICE_COSECHA_CONVERSACION_4:
            conteo = filtro.filter(cosechaconversacion1__conversacion4__contains=obj[0],anio = anio[0]).count()
            dict_conversacion4[obj[1]] = conteo

        dict_conversacion5 = OrderedDict()
        for obj in CHOICE_COSECHA_CONVERSACION_5:
            conteo = filtro.filter(cosechaconversacion2__conversacion5__contains=obj[0],anio = anio[0]).count()
            dict_conversacion5[obj[1]] = conteo

        list_conversacion6 = []
        for obj in filtro:
            lista = CosechaConversacion2.objects.filter(ficha=obj,ficha__anio = anio[0]).values_list('conversacion6', flat=True)
            list_conversacion6.append(list(lista))

        try:
            promedio = np.mean(list(list_conversacion6))
        except:
            promedio = 0
        try:
            desviacion_estandar = np.std(list(list_conversacion6))
        except:
            desviacion_estandar = 0
        try:
            mediano = np.median(list(list_conversacion6))
        except:
            mediano = 0
        try:
            minimo = min(list(list_conversacion6))
        except:
            minimo = 0
        try:
            maximo = max(list(list_conversacion6))
        except:
            maximo = 0

        dict_conversacion7 = OrderedDict()
        for obj in CHOICE_COSECHA_CONVERSACION_7:
            conteo = filtro.filter(cosechaconversacion2__conversacion7__contains=obj[0],anio = anio[0]).count()
            dict_conversacion7[obj[1]] = conteo

        dict_conversacion8 = OrderedDict()
        for obj in CHOICE_COSECHA_CONVERSACION_8:
            conteo = filtro.filter(cosechaconversacion2__conversacion8__contains=obj[0],anio = anio[0]).count()
            dict_conversacion8[obj[1]] = conteo

        years[anio[1]] = [numero_parcelas,dict_conversacion1,dict_conversacion2,dict_conversacion3,
                        dict_conversacion4,dict_conversacion5,dict_conversacion7,dict_conversacion8,
                        promedio,desviacion_estandar,mediano,minimo,maximo]

    return render(request, template, locals())

def datos_sanos_cosecha(request, template="transformacion/cosecha/datos_sanos_cosecha.html"):
    filtro = _queryset_filtrado_cosecha(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        danadas = generic_datos_cosecha(request,1,anio[0])

        years[anio[1]] = numero_parcelas,danadas

    return render(request, template, locals())

def datos_enfermas_cosecha(request, template="transformacion/cosecha/datos_enfermas_cosecha.html"):
    filtro = _queryset_filtrado_cosecha(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        danadas = generic_datos_cosecha(request,2,anio[0])

        years[anio[1]] = numero_parcelas,danadas

    return render(request, template, locals())

def datos_danadas_cosecha(request, template="transformacion/cosecha/datos_danadas_cosecha.html"):
    filtro = _queryset_filtrado_cosecha(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        danadas = generic_datos_cosecha(request,3,anio[0])

        years[anio[1]] = numero_parcelas,danadas

    return render(request, template, locals())

def generic_datos_cosecha(request, tipo=0,anio=0):
    filtro = _queryset_filtrado_cosecha(request)

    numero_parcelas = filtro.filter(anio = anio).count()

    punto1_plantas = filtro.filter(cosechapunto1__mazorcas=tipo,
                    cosechapunto1__contador__gt=0,anio = anio).aggregate(plantas=Sum('cosechapunto1__contador'))['plantas'] or 0
    punto2_plantas = filtro.filter(cosechapunto2__mazorcas=tipo,
                    cosechapunto2__contador__gt=0,anio = anio).aggregate(plantas=Sum('cosechapunto2__contador'))['plantas'] or 0
    punto3_plantas = filtro.filter(cosechapunto3__mazorcas=tipo,
                    cosechapunto3__contador__gt=0,anio = anio).aggregate(plantas=Sum('cosechapunto3__contador'))['plantas'] or 0

    #Numero 1
    TOTAL_PLANTAS = punto1_plantas + punto2_plantas + punto3_plantas

    punto1_mazorca_sana = filtro.filter(cosechapunto1__mazorcas=tipo,anio = anio).aggregate(sanas=Sum('cosechapunto1__total_platas'))['sanas'] or 0
    punto2_mazorca_sana = filtro.filter(cosechapunto2__mazorcas=tipo,anio = anio).aggregate(sanas=Sum('cosechapunto2__total_platas'))['sanas'] or 0
    punto3_mazorca_sana = filtro.filter(cosechapunto3__mazorcas=tipo,anio = anio).aggregate(sanas=Sum('cosechapunto3__total_platas'))['sanas'] or 0

    #Numero 2
    TOTAL_MAZORCAS_SANAS = punto1_mazorca_sana + punto2_mazorca_sana + punto3_mazorca_sana
    #Numero 3
    try:
        MAZORCA_SANA_POR_PLATA = float(TOTAL_MAZORCAS_SANAS)/float(TOTAL_PLANTAS)
    except:
        MAZORCA_SANA_POR_PLATA = 0
    #Numero 4
    try:
        suma_area_ha = filtro.filter(anio = anio).aggregate(suma=Sum('cosechaareaplantas__area_ha'))['suma']
    except:
        suma_area_ha = 0
    try:
        PROMEDIO_PLATAS_POR_MANZANA =(filtro.filter(anio = anio).aggregate(promedio=Sum('cosechaareaplantas__plantas'))['promedio'] or 0) / float(suma_area_ha)
    except:
        PROMEDIO_PLATAS_POR_MANZANA = 0
    #Numero 5
    MAZORCAS_SANAS_X_MANZANAS = (MAZORCA_SANA_POR_PLATA * PROMEDIO_PLATAS_POR_MANZANA) #* float(1.6)
    #Numero 6
    PESO_BABA = float(MAZORCAS_SANAS_X_MANZANAS) / (float(3.5) * 100) #
    #numero 7
    PESO_GRANO_SECO = float(PESO_BABA)/float(2.6) # en hondura y dominicana
    #numero 8
    PESO_GRANO_SECO_KILO_HA = PESO_GRANO_SECO * 45.45 # eso para convertir

    completo = [TOTAL_PLANTAS,TOTAL_MAZORCAS_SANAS,MAZORCA_SANA_POR_PLATA,
                          PROMEDIO_PLATAS_POR_MANZANA,MAZORCAS_SANAS_X_MANZANAS,
                          PESO_BABA,PESO_GRANO_SECO,PESO_GRANO_SECO_KILO_HA]
    return completo

def analisis_cosecha(request, template="transformacion/cosecha/analisis_cosecha.html"):
    filtro = _queryset_filtrado_cosecha(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        analisis_1 = OrderedDict()
        for obj in CHOICE_COSECHA_ANALISIS_1:
            conteo = filtro.filter(cosechaanalisis__analisis1__contains=obj[0],anio = anio[0]).count()
            analisis_1[obj[1]] = conteo

        analisis_2 = OrderedDict()
        for obj in CHOICE_COSECHA_ANALISIS_2:
            conteo = filtro.filter(cosechaanalisis__analisis2__contains=obj[0],anio = anio[0]).count()
            analisis_2[obj[1]] = conteo

        analisis_3 = OrderedDict()
        for obj in CHOICE_COSECHA_ANALISIS_3:
            conteo = filtro.filter(cosechaanalisis__analisis3__contains=obj[0],anio = anio[0]).count()
            analisis_3[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,analisis_1,analisis_2,analisis_3

    return render(request, template, locals())

#-------------- fin de salidas de cosecha primaria -------------


# ----------- comienza salidas de oosecha secundaria ---------
def _queryset_filtrado_cosecha_secundaria(request):
    params = {}

    if 'ciclo' in request.session:
        params['ciclo__nombre'] = request.session['ciclo']

    if 'productor' in request.session:
        params['productor__nombre'] = request.session['productor']

    if 'organizacion_pertenece' in request.session:
        params['productor__organizacion_pertenece'] = request.session['organizacion_pertenece']

    if 'organizacion_apoyo' in request.session:
        params['productor__organizacion_apoyo__in'] = request.session['organizacion_apoyo']

    if 'pais' in request.session:
        params['productor__pais'] = request.session['pais']

    if 'departamento' in request.session:
        params['productor__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['productor__municipio'] = request.session['municipio']

    if 'parcela' in request.session:
        params['productor__tamanio_parcela__in'] = request.session['parcela']

    # if 'tipologia' in request.session:
    #     params['productor__productor__tipologia'] = request.session['tipologia']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    return FichaCosechaSecundaria.objects.filter(**params)

def datos_sanos_cosecha_secundaria(request, template="transformacion/cosechasecundaria/datos_sanos_cosecha.html"):
    filtro = _queryset_filtrado_cosecha_secundaria(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        danadas = generic_datos_cosecha_secundaria(request,1,anio[0])

        years[anio[1]] = numero_parcelas,danadas

    return render(request, template, locals())

def datos_enfermas_cosecha_secundaria(request, template="transformacion/cosechasecundaria/datos_enfermas_cosecha.html"):
    filtro = _queryset_filtrado_cosecha_secundaria(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        danadas = generic_datos_cosecha_secundaria(request,2,anio[0])

        years[anio[1]] = numero_parcelas,danadas

    return render(request, template, locals())

def datos_danadas_cosecha_secundaria(request, template="transformacion/cosechasecundaria/datos_danadas_cosecha.html"):
    filtro = _queryset_filtrado_cosecha_secundaria(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        danadas = generic_datos_cosecha_secundaria(request,3,anio[0])

        years[anio[1]] = numero_parcelas,danadas

    return render(request, template, locals())

def generic_datos_cosecha_secundaria(request, tipo=0,anio=0):
    filtro = _queryset_filtrado_cosecha_secundaria(request)
    numero_parcelas = filtro.filter(anio = anio).count()

    punto1_plantas = filtro.filter(cosechasecundariapunto1__mazorcas=tipo,
                    cosechasecundariapunto1__contador__gt=0,anio = anio).aggregate(plantas=Sum('cosechasecundariapunto1__contador'))['plantas'] or 0
    punto2_plantas = filtro.filter(cosechasecundariapunto2__mazorcas=tipo,
                    cosechasecundariapunto2__contador__gt=0,anio = anio).aggregate(plantas=Sum('cosechasecundariapunto2__contador'))['plantas'] or 0
    punto3_plantas = filtro.filter(cosechasecundariapunto3__mazorcas=tipo,
                    cosechasecundariapunto3__contador__gt=0,anio = anio).aggregate(plantas=Sum('cosechasecundariapunto3__contador'))['plantas'] or 0

    #Numero 1
    TOTAL_PLANTAS = punto1_plantas + punto2_plantas + punto3_plantas

    punto1_mazorca_sana = filtro.filter(cosechasecundariapunto1__mazorcas=tipo,anio = anio).aggregate(sanas=Sum('cosechasecundariapunto1__total_platas'))['sanas'] or 0
    punto2_mazorca_sana = filtro.filter(cosechasecundariapunto2__mazorcas=tipo,anio = anio).aggregate(sanas=Sum('cosechasecundariapunto2__total_platas'))['sanas'] or 0
    punto3_mazorca_sana = filtro.filter(cosechasecundariapunto3__mazorcas=tipo,anio = anio).aggregate(sanas=Sum('cosechasecundariapunto3__total_platas'))['sanas'] or 0

    #Numero 2
    TOTAL_MAZORCAS_SANAS = punto1_mazorca_sana + punto2_mazorca_sana + punto3_mazorca_sana
    #Numero 3
    try:
        MAZORCA_SANA_POR_PLATA = float(TOTAL_MAZORCAS_SANAS)/float(TOTAL_PLANTAS)
    except:
        MAZORCA_SANA_POR_PLATA = 0
    #Numero 4
    try:
        suma_area_ha = filtro.filter(anio = anio).aggregate(suma=Sum('cosechasecundariaareaplantas__area_ha'))['suma']
    except:
        suma_area_ha = 0
    try:
        PROMEDIO_PLATAS_POR_MANZANA = (filtro.filter(anio = anio).aggregate(promedio=Sum('cosechasecundariaareaplantas__plantas'))['promedio'] or 0) / float(suma_area_ha)
    except:
        PROMEDIO_PLATAS_POR_MANZANA = 0
    #Numero 5
    MAZORCAS_SANAS_X_MANZANAS = (MAZORCA_SANA_POR_PLATA * PROMEDIO_PLATAS_POR_MANZANA) #* float(1.6)
    #Numero 6
    PESO_BABA = float(MAZORCAS_SANAS_X_MANZANAS) / (float(3.5) * 100)
    #numero 7
    PESO_GRANO_SECO = float(PESO_BABA)/float(2.6)
    #numero 8
    PESO_GRANO_SECO_KILO_HA = PESO_GRANO_SECO * 45.45

    completo = [TOTAL_PLANTAS,TOTAL_MAZORCAS_SANAS,MAZORCA_SANA_POR_PLATA,
                          PROMEDIO_PLATAS_POR_MANZANA,MAZORCAS_SANAS_X_MANZANAS,
                          PESO_BABA,PESO_GRANO_SECO,PESO_GRANO_SECO_KILO_HA]
    return completo


def analisis_cosecha_secundaria(request, template="transformacion/cosechasecundaria/analisis_cosecha.html"):
    filtro = _queryset_filtrado_cosecha_secundaria(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        analisis_1 = OrderedDict()
        for obj in CHOICE_COSECHA_ANALISIS_1:
            conteo = filtro.filter(cosechasecundariaanalisis__analisis1__contains=obj[0],anio = anio[0]).count()
            analisis_1[obj[1]] = conteo

        analisis_2 = OrderedDict()
        for obj in CHOICE_COSECHA_ANALISIS_2:
            conteo = filtro.filter(cosechasecundariaanalisis__analisis2__contains=obj[0],anio = anio[0]).count()
            analisis_2[obj[1]] = conteo

        analisis_3 = OrderedDict()
        for obj in CHOICE_COSECHA_ANALISIS_3:
            conteo = filtro.filter(cosechasecundariaanalisis__analisis3__contains=obj[0],anio = anio[0]).count()
            analisis_3[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,analisis_1,analisis_2,analisis_3

    return render(request, template, locals())


#--------- cierre de cosecha secundaria -------------------------

#-------------- comienza salidas de cierre ------------
def _queryset_filtrado_cierre(request):
    params = {}

    if 'ciclo' in request.session:
        params['ciclo__nombre'] = request.session['ciclo']

    if 'productor' in request.session:
        params['productor__nombre'] = request.session['productor']

    if 'organizacion_pertenece' in request.session:
        params['productor__organizacion_pertenece'] = request.session['organizacion_pertenece']

    if 'organizacion_apoyo' in request.session:
        params['productor__organizacion_apoyo__in'] = request.session['organizacion_apoyo']

    if 'pais' in request.session:
        params['productor__pais'] = request.session['pais']

    if 'departamento' in request.session:
        params['productor__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['productor__municipio'] = request.session['municipio']

    if 'parcela' in request.session:
        params['productor__tamanio_parcela__in'] = request.session['parcela']

    # if 'tipologia' in request.session:
    #     params['productor__productor__tipologia'] = request.session['tipologia']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    return FichaCierre.objects.filter(**params)

#---------- SALIDAS DE CIERRE --------------------------

def sombra_cierre(request, template="transformacion/cierre/sombra_cierre.html"):
    filtro = _queryset_filtrado_cierre(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        impacto_sombra = OrderedDict()
        for obj in CHOICE_CIERRE_1_1_IMPACTO:
            conteo = filtro.filter(cierremanejo1__campo1__contains=obj[0],anio = anio[0]).count()
            impacto_sombra[obj[1]] = conteo

        planificada_sombra = OrderedDict()
        for obj in CHOICE_CIERRE_1_1_PLANIFICADA:
            conteo = filtro.filter(cierremanejo1__campo2__contains=obj[0],anio = anio[0]).count()
            planificada_sombra[obj[1]] = conteo

        realizada_sombra = OrderedDict()
        for obj in CHOICE_CIERRE_1_1_REALIZADA:
            conteo = filtro.filter(cierremanejo1__campo3__contains=obj[0],anio = anio[0]).count()
            realizada_sombra[obj[1]] = conteo

        resultado_sombra = OrderedDict()
        for obj in CHOICE_CIERRE_1_1_RESULTADOS:
            conteo = filtro.filter(cierremanejo1__campo4__contains=obj[0],anio = anio[0]).count()
            resultado_sombra[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,impacto_sombra,planificada_sombra,realizada_sombra,resultado_sombra

    return render(request, template, locals())


def poda_cierre(request, template="transformacion/cierre/poda_cierre.html"):
    filtro = _queryset_filtrado_cierre(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        impacto_poda = OrderedDict()
        for obj in CHOICE_CIERRE_1_2_IMPACTO:
            conteo = filtro.filter(cierremanejo2__campo1__contains=obj[0],anio = anio[0]).count()
            impacto_poda[obj[1]] = conteo

        planificada_poda = OrderedDict()
        for obj in CHOICE_CIERRE_1_2_PLANIFICADA:
            conteo = filtro.filter(cierremanejo2__campo2__contains=obj[0],anio = anio[0]).count()
            planificada_poda[obj[1]] = conteo

        realizada_poda = OrderedDict()
        for obj in CHOICE_CIERRE_1_2_REALIZADA:
            conteo = filtro.filter(cierremanejo2__campo3__contains=obj[0],anio = anio[0]).count()
            realizada_poda[obj[1]] = conteo

        resultado_poda = OrderedDict()
        for obj in CHOICE_CIERRE_1_2_RESULTADOS:
            conteo = filtro.filter(cierremanejo2__campo4__contains=obj[0],anio = anio[0]).count()
            resultado_poda[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,impacto_poda,planificada_poda,realizada_poda,resultado_poda

    return render(request, template, locals())


def suelo_cierre(request, template="transformacion/cierre/suelo_cierre.html"):
    filtro = _queryset_filtrado_cierre(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        impacto_suelo = OrderedDict()
        for obj in CHOICE_CIERRE_1_3_IMPACTO:
            conteo = filtro.filter(cierremanejo3__campo1__contains=obj[0],anio = anio[0]).count()
            impacto_suelo[obj[1]] = conteo

        planificada_suelo = OrderedDict()
        for obj in CHOICE_CIERRE_1_3_PLANIFICADA:
            conteo = filtro.filter(cierremanejo3__campo2__contains=obj[0],anio = anio[0]).count()
            planificada_suelo[obj[1]] = conteo

        realizada_suelo = OrderedDict()
        for obj in CHOICE_CIERRE_1_3_REALIZADA:
            conteo = filtro.filter(cierremanejo3__campo3__contains=obj[0],anio = anio[0]).count()
            realizada_suelo[obj[1]] = conteo

        resultado_suelo = OrderedDict()
        for obj in CHOICE_CIERRE_1_3_RESULTADOS:
            conteo = filtro.filter(cierremanejo3__campo4__contains=obj[0],anio = anio[0]).count()
            resultado_suelo[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,impacto_suelo,planificada_suelo,realizada_suelo,resultado_suelo

    return render(request, template, locals())

def plaga_cierre(request, template="transformacion/cierre/plaga_cierre.html"):
    filtro = _queryset_filtrado_cierre(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        impacto_plaga = OrderedDict()
        for obj in CHOICE_CIERRE_1_4_IMPACTO:
            conteo = filtro.filter(cierremanejo4__campo1__contains=obj[0],anio = anio[0]).count()
            impacto_plaga[obj[1]] = conteo

        planificada_plaga = OrderedDict()
        for obj in CHOICE_CIERRE_1_4_PLANIFICADA:
            conteo = filtro.filter(cierremanejo4__campo2__contains=obj[0],anio = anio[0]).count()
            planificada_plaga[obj[1]] = conteo

        realizada_plaga = OrderedDict()
        for obj in CHOICE_CIERRE_1_4_REALIZADA:
            conteo = filtro.filter(cierremanejo4__campo3__contains=obj[0],anio = anio[0]).count()
            realizada_plaga[obj[1]] = conteo

        resultado_plaga = OrderedDict()
        for obj in CHOICE_CIERRE_1_4_RESULTADOS:
            conteo = filtro.filter(cierremanejo4__campo4__contains=obj[0],anio = anio[0]).count()
            resultado_plaga[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,impacto_plaga,planificada_plaga,realizada_plaga,resultado_plaga

    return render(request, template, locals())

def piso_cierre(request, template="transformacion/cierre/piso_cierre.html"):
    filtro = _queryset_filtrado_cierre(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        impacto_piso = OrderedDict()
        for obj in CHOICE_CIERRE_1_5_IMPACTO:
            conteo = filtro.filter(cierremanejo5__campo1__contains=obj[0],anio = anio[0]).count()
            impacto_piso[obj[1]] = conteo

        planificada_piso = OrderedDict()
        for obj in CHOICE_CIERRE_1_5_PLANIFICADA:
            conteo = filtro.filter(cierremanejo5__campo2__contains=obj[0],anio = anio[0]).count()
            planificada_piso[obj[1]] = conteo

        realizada_piso = OrderedDict()
        for obj in CHOICE_CIERRE_1_5_REALIZADA:
            conteo = filtro.filter(cierremanejo5__campo3__contains=obj[0],anio = anio[0]).count()
            realizada_piso[obj[1]] = conteo

        resultado_piso = OrderedDict()
        for obj in CHOICE_CIERRE_1_5_RESULTADOS:
            conteo = filtro.filter(cierremanejo5__campo4__contains=obj[0],anio = anio[0]).count()
            resultado_piso[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,impacto_piso,planificada_piso,realizada_piso,resultado_piso

    return render(request, template, locals())

def vivero_cierre(request, template="transformacion/cierre/vivero_cierre.html"):
    filtro = _queryset_filtrado_cierre(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        impacto_viviero = OrderedDict()
        for obj in CHOICE_CIERRE_1_6_IMPACTO:
            conteo = filtro.filter(cierremanejo6__campo1__contains=obj[0],anio = anio[0]).count()
            impacto_viviero[obj[1]] = conteo

        planificada_viviero = OrderedDict()
        for obj in CHOICE_CIERRE_1_6_PLANIFICADA:
            conteo = filtro.filter(cierremanejo6__campo2__contains=obj[0],anio = anio[0]).count()
            planificada_viviero[obj[1]] = conteo

        realizada_viviero = OrderedDict()
        for obj in CHOICE_CIERRE_1_6_REALIZADA:
            conteo = filtro.filter(cierremanejo6__campo3__contains=obj[0],anio = anio[0]).count()
            realizada_viviero[obj[1]] = conteo

        resultado_viviero = OrderedDict()
        for obj in CHOICE_CIERRE_1_6_RESULTADOS:
            conteo = filtro.filter(cierremanejo6__campo4__contains=obj[0],anio = anio[0]).count()
            resultado_viviero[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,impacto_viviero,planificada_viviero,realizada_viviero,resultado_viviero

    return render(request, template, locals())

def cosecha_cierre(request, template="transformacion/cierre/cosecha_cierre.html"):
    filtro = _queryset_filtrado_cierre(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        impacto_cosecha = OrderedDict()
        for obj in CHOICE_CIERRE_1_7_IMPACTO:
            conteo = filtro.filter(cierremanejo7__campo1__contains=obj[0],anio = anio[0]).count()
            impacto_cosecha[obj[1]] = conteo

        planificada_cosecha = OrderedDict()
        for obj in CHOICE_CIERRE_1_7_PLANIFICADA:
            conteo = filtro.filter(cierremanejo7__campo2__contains=obj[0],anio = anio[0]).count()
            planificada_cosecha[obj[1]] = conteo

        realizada_cosecha = OrderedDict()
        for obj in CHOICE_CIERRE_1_7_REALIZADA:
            conteo = filtro.filter(cierremanejo7__campo3__contains=obj[0],anio = anio[0]).count()
            realizada_cosecha[obj[1]] = conteo

        resultado_cosecha = OrderedDict()
        for obj in CHOICE_CIERRE_1_7_RESULTADO:
            conteo = filtro.filter(cierremanejo7__campo4__contains=obj[0],anio = anio[0]).count()
            resultado_cosecha[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,impacto_cosecha,planificada_cosecha,realizada_cosecha,resultado_cosecha

    return render(request, template, locals())

def calculos_costo_cierre(request, template="transformacion/cierre/calculos_cierre.html"):
    filtro = _queryset_filtrado_cierre(request)
    numero_parcelas = filtro.count()

    costo_mano_obra = filtro.aggregate(costo=Avg('cierrecosto1__costo'))['costo'] or 0
    area_mz = filtro.aggregate(area=Sum('cierrecosto1__area'))['area'] or 0

    dict_actividades = OrderedDict()
    for obj in ActividadesCierre.objects.all():
        avg_familiar = filtro.filter(cierreactividad__actividad=obj).aggregate(familiar=Sum('cierreactividad__familiar'))['familiar'] or 0
        avg_contratada = filtro.filter(cierreactividad__actividad=obj).aggregate(contra=Sum('cierreactividad__contratada'))['contra'] or 0
        avg_costo = filtro.filter(cierreactividad__actividad=obj).aggregate(costo=Sum('cierreactividad__costo'))['costo'] or 0
        dict_actividades[obj] = [avg_familiar,avg_contratada,avg_costo]

    suma_familiar = sum(v[0] for k,v in dict_actividades.iteritems())
    suma_contradata = sum(v[1] for k,v in dict_actividades.iteritems())
    suma_costo = sum(v[2] for k,v in dict_actividades.iteritems())

    cosecha_baba = filtro.aggregate(valor=Sum('cierrebabaroja__campo1'))['valor'] or 0
    venta_baba = filtro.aggregate(valor=Sum('cierrebabaroja__campo2'))['valor'] or 0
    precio_baba = filtro.aggregate(valor=Avg('cierrebabaroja__campo3'))['valor'] or 0
    cosecha_rojo = filtro.aggregate(valor=Sum('cierrebabaroja__campo4'))['valor'] or 0
    venta_rojo = filtro.aggregate(valor=Sum('cierrebabaroja__campo5'))['valor'] or 0
    consumo_rojo = filtro.aggregate(valor=Sum('cierrebabaroja__campo7'))['valor'] or 0
    precio_rojo = filtro.aggregate(valor=Avg('cierrebabaroja__campo6'))['valor'] or 0

    try:
        gasto_mo_familiar = suma_familiar * costo_mano_obra
    except:
        pass
    try:
        gasto_mo_contratada = suma_contradata * costo_mano_obra
    except:
        pass
    try:
        gasto_efectivo = suma_costo + gasto_mo_contratada
    except:
        pass
    try:
        costo_produccion = gasto_efectivo + gasto_mo_familiar
    except:
        pass
    try:
        ingreso_venta = (venta_baba * precio_baba) + (venta_rojo * precio_rojo)
    except:
       pass
    try:
        consumo_familiar = consumo_rojo * precio_rojo
    except:
        pass
    try:
        ingreso_bruto = ingreso_venta + consumo_familiar
    except:
       pass
    try:
        ingreso_neto_parcial = ingreso_bruto - gasto_efectivo
    except:
       pass
    try:
        retorno_mo_familiar = float(ingreso_neto_parcial) / float(suma_familiar)
    except:
        pass
    try:
        ingreso_neto = ingreso_bruto - costo_produccion
    except:
       pass
    try:
        tasa_retorno_ciclo = (float(ingreso_neto) / float(costo_produccion)) * 100
    except:
        pass
    try:
        inversion_mz = float(costo_produccion) / float(area_mz)
    except:
       pass
    try:
        ingreso_neto_mz = ingreso_neto / area_mz
    except:
        pass
    try:
        costo_qq_baba = costo_produccion / float((cosecha_baba + (cosecha_rojo*3)))
    except:
        pass


    return render(request, template, locals())

def tablas_cierre(request, template="guiascacao/cierre/tablas_cierre.html"):
    filtro = _queryset_filtrado_cierre(request)
    numero_parcelas = filtro.count()

    tabla_cierre_manejo = OrderedDict()
    for obj in ManejosCierre.objects.all():
        cont_reposo = filtro.filter(cierremanejo__manejo=obj, cierremanejo__reposo=1).count()
        cont_crecimiento = filtro.filter(cierremanejo__manejo=obj, cierremanejo__crecimiento=1).count()
        cont_floracion = filtro.filter(cierremanejo__manejo=obj, cierremanejo__floracion=1).count()
        cont_cosecha = filtro.filter(cierremanejo__manejo=obj, cierremanejo__cosecha=1).count()
        tabla_cierre_manejo[obj] = [cont_reposo,cont_crecimiento,cont_floracion,cont_cosecha]

    tabla_variedad = OrderedDict()
    for obj in CHOICE_CIERRE_CONOCIMIENTO_TEMA1:
        cont_criollas = filtro.filter(cierreconocimiento1__tema=obj[0], cierreconocimiento1__criollas=1).count()
        cont_forastero = filtro.filter(cierreconocimiento1__tema=obj[0], cierreconocimiento1__forastero=1).count()
        cont_trinitaria = filtro.filter(cierreconocimiento1__tema=obj[0], cierreconocimiento1__trinitaria=1).count()
        cont_hibridos = filtro.filter(cierreconocimiento1__tema=obj[0], cierreconocimiento1__hibridos=1).count()
        cont_clones = filtro.filter(cierreconocimiento1__tema=obj[0], cierreconocimiento1__clones=1).count()
        tabla_variedad[obj[1]] = [cont_criollas,cont_forastero,cont_trinitaria,cont_hibridos,cont_clones]

    tabla_ventajas = OrderedDict()
    for obj in CHOICE_CIERRE_CONOCIMIENTO_RESPUESTAS:
        cont_criollas = filtro.filter(cierreconocimiento2__tema=1, cierreconocimiento2__criollas__contains=obj[0]).count()
        cont_forastero = filtro.filter(cierreconocimiento2__tema=1, cierreconocimiento2__forastero__contains=obj[0]).count()
        cont_trinitaria = filtro.filter(cierreconocimiento2__tema=1, cierreconocimiento2__trinitaria__contains=obj[0]).count()
        cont_hibridos = filtro.filter(cierreconocimiento2__tema=1, cierreconocimiento2__hibridos__contains=obj[0]).count()
        cont_clones = filtro.filter(cierreconocimiento2__tema=1, cierreconocimiento2__clones__contains=obj[0]).count()
        tabla_ventajas[obj[1]] = [cont_criollas,cont_forastero,cont_trinitaria,cont_hibridos,cont_clones]

    tabla_desventajas = OrderedDict()
    for obj in CHOICE_CIERRE_CONOCIMIENTO_RESPUESTAS3:
        cont_criollas = filtro.filter(cierreconocimiento3__tema=1, cierreconocimiento3__criollas__contains=obj[0]).count()
        cont_forastero = filtro.filter(cierreconocimiento3__tema=1, cierreconocimiento3__forastero__contains=obj[0]).count()
        cont_trinitaria = filtro.filter(cierreconocimiento3__tema=1, cierreconocimiento3__trinitaria__contains=obj[0]).count()
        cont_hibridos = filtro.filter(cierreconocimiento3__tema=1, cierreconocimiento3__hibridos__contains=obj[0]).count()
        cont_clones = filtro.filter(cierreconocimiento3__tema=1, cierreconocimiento3__clones__contains=obj[0]).count()
        tabla_desventajas[obj[1]] = [cont_criollas,cont_forastero,cont_trinitaria,cont_hibridos,cont_clones]

    tabla_suelo1 = OrderedDict()
    for obj in CHOICE_CIERRE_SUELO_RESPUESTAS1:
        cont_abono = filtro.filter(cierresuelo1__tema=1, cierresuelo1__abono__contains=obj[0]).count()
        cont_hojarasca = filtro.filter(cierresuelo1__tema=1, cierresuelo1__hojarasca__contains=obj[0]).count()
        cont_organico = filtro.filter(cierresuelo1__tema=1, cierresuelo1__organico__contains=obj[0]).count()
        tabla_suelo1[obj[1]] = [cont_abono,cont_hojarasca,cont_organico]

    tabla_suelo2 = OrderedDict()
    for obj in CHOICE_CIERRE_SUELO_RESPUESTAS2:
        cont_abono = filtro.filter(cierresuelo2__tema=1, cierresuelo2__abono__contains=obj[0]).count()
        cont_hojarasca = filtro.filter(cierresuelo2__tema=1, cierresuelo2__hojarasca__contains=obj[0]).count()
        cont_organico = filtro.filter(cierresuelo2__tema=1, cierresuelo2__organico__contains=obj[0]).count()
        tabla_suelo2[obj[1]] = [cont_abono,cont_hojarasca,cont_organico]

    tabla_suelo3 = OrderedDict()
    for obj in CHOICE_CIERRE_SUELO_RESPUESTAS3:
        cont_abono = filtro.filter(cierresuelo3__tema=1, cierresuelo3__abono__contains=obj[0]).count()
        cont_hojarasca = filtro.filter(cierresuelo3__tema=1, cierresuelo3__hojarasca__contains=obj[0]).count()
        cont_organico = filtro.filter(cierresuelo3__tema=1, cierresuelo3__organico__contains=obj[0]).count()
        tabla_suelo3[obj[1]] = [cont_abono,cont_hojarasca,cont_organico]

    tabla_plaga2 = OrderedDict()
    for obj in CHOICE_CIERRE_PLAGA_RESPUESTAS2:
        cont_monilla = filtro.filter(cierreplaga2__tema=1, cierreplaga2__monilla__contains=obj[0]).count()
        cont_mazorca = filtro.filter(cierreplaga2__tema=1, cierreplaga2__mazorca__contains=obj[0]).count()
        tabla_plaga2[obj[1]] = [cont_monilla,cont_mazorca]

    tabla_plaga22 = OrderedDict()
    for obj in CHOICE_CIERRE_PLAGA_RESPUESTAS_ZOMPOPO:
        cont_zompopos = filtro.filter(cierreplaga2__tema=1, cierreplaga2__zompopos__contains=obj[0]).count()
        tabla_plaga22[obj[1]] = [cont_zompopos]

    tabla_plaga3 = OrderedDict()
    for obj in CHOICE_CIERRE_PLAGA_RESPUESTAS3:
        cont_monilla = filtro.filter(cierreplaga3__tema=1, cierreplaga3__monilla__contains=obj[0]).count()
        cont_mazorca = filtro.filter(cierreplaga3__tema=1, cierreplaga3__mazorca__contains=obj[0]).count()
        tabla_plaga3[obj[1]] = [cont_monilla,cont_mazorca]

    tabla_plaga33 = OrderedDict()
    for obj in CHOICE_CIERRE_PLAGA_RESPUESTAS_ZOMPOPO3:
        cont_zompopos = filtro.filter(cierreplaga3__tema=1, cierreplaga3__zompopos__contains=obj[0]).count()
        tabla_plaga33[obj[1]] = [cont_zompopos]

    return render(request, template, locals())

def ciclo_trabajo_cierre(request, template="transformacion/cierre/ciclo_trabajo_cierre.html"):
    filtro = _queryset_filtrado_cierre(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        pie_1 = OrderedDict()
        for obj in CHOICE_CIERRE_CICLO_TRABAJO1_RESPUESTA:
            conteo = filtro.filter(ciclotrabajocierre__pregunta1=obj[0],anio = anio[0]).count()
            pie_1[obj[1]] = conteo

        pie_2 = OrderedDict()
        for obj in CHOICE_CIERRE_CICLO_TRABAJO1_RESPUESTA:
            conteo = filtro.filter(ciclotrabajocierre__pregunta2=obj[0],anio = anio[0]).count()
            pie_2[obj[1]] = conteo

        pie_3 = OrderedDict()
        for obj in CHOICE_CIERRE_CICLO_TRABAJO1_RESPUESTA:
            conteo = filtro.filter(ciclotrabajocierre__pregunta3=obj[0],anio = anio[0]).count()
            pie_3[obj[1]] = conteo

        pie_4 = OrderedDict()
        for obj in CHOICE_CIERRE_CICLO_TRABAJO2_RESPUESTA:
            conteo = filtro.filter(ciclotrabajocierre__pregunta4=obj[0],anio = anio[0]).count()
            pie_4[obj[1]] = conteo

        pie_5 = OrderedDict()
        for obj in CHOICE_CIERRE_CICLO_TRABAJO3_RESPUESTA:
            conteo = filtro.filter(ciclotrabajocierre__pregunta5=obj[0],anio = anio[0]).count()
            pie_5[obj[1]] = conteo

        pie_6 = OrderedDict()
        for obj in CHOICE_CIERRE_CICLO_TRABAJO4_RESPUESTA:
            conteo = filtro.filter(ciclotrabajocierre__pregunta6=obj[0],anio = anio[0]).count()
            pie_6[obj[1]] = conteo

        pie_7 = OrderedDict()
        for obj in CHOICE_CIERRE_CICLO_TRABAJO5_RESPUESTA:
            conteo = filtro.filter(ciclotrabajocierre__pregunta7=obj[0],anio = anio[0]).count()
            pie_7[obj[1]] = conteo

        pie_8 = OrderedDict()
        for obj in CHOICE_SI_NO:
            conteo = filtro.filter(ciclotrabajocierre__pregunta8=obj[0],anio = anio[0]).count()
            pie_8[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,pie_1,pie_2,pie_3,pie_4,pie_5,pie_6,pie_7,pie_8


    return render(request, template, locals())

#fin de salidas de cierre

# ---------------------------- COMIENZA SALIDAS DE suelo
#filtro sobre suelo
def _queryset_filtrado_suelo(request):
    params = {}

    if 'ciclo' in request.session:
        params['ciclo__nombre'] = request.session['ciclo']

    if 'productor' in request.session:
        params['productor__nombre'] = request.session['productor']

    if 'organizacion_pertenece' in request.session:
        params['productor__organizacion_pertenece'] = request.session['organizacion_pertenece']

    if 'organizacion_apoyo' in request.session:
        params['productor__organizacion_apoyo__in'] = request.session['organizacion_apoyo']

    if 'pais' in request.session:
        params['productor__pais'] = request.session['pais']

    if 'departamento' in request.session:
        params['productor__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['productor__municipio'] = request.session['municipio']

    if 'parcela' in request.session:
        params['productor__tamanio_parcela__in'] = request.session['parcela']

    # if 'tipologia' in request.session:
    #     params['productor__productor__tipologia'] = request.session['tipologia']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    return FichaSueloFertilidad.objects.filter(**params)
##salidas de suelo

def historial_limitaciones(request, template='transformacion/suelo/historialLimitaciones.html'):
    filtro = _queryset_filtrado_suelo(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        uso_parcela = OrderedDict()
        for obj in CHOICE_SUELO_USO_PARCELA:
            conteo = filtro.filter(punto1suelo__uso_parcela=obj[0],anio = anio[0]).count()
            uso_parcela[obj[1]] = conteo

        suelo_limitante = OrderedDict()
        for obj in CHOICE_SUELO_LIMITANTES:
            conteo = filtro.filter(punto1suelo__limitante__contains=obj[0],anio = anio[0]).count()
            suelo_limitante[obj[1]] = conteo

        suelo_orientacion = OrderedDict()
        for obj in CHOICE_SUELO_ORIENTACION:
            conteo = filtro.filter(punto1suelo__orientacion__contains=obj[0],anio = anio[0]).count()
            suelo_orientacion[obj[1]] = conteo

        suelo_abonos = OrderedDict()
        for obj in CHOICE_SUELO_ABONOS:
            conteo = filtro.filter(punto1suelo__abonos__contains=obj[0],anio = anio[0]).count()
            suelo_abonos[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,uso_parcela,suelo_limitante,suelo_orientacion,suelo_abonos

    return render(request, template, locals())

def suelo_erosion(request, template='transformacion/suelo/sueloErosion.html'):
    filtro = _queryset_filtrado_suelo(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        suelo_erosion_indi = OrderedDict()
        for obj in CHOICE_SUELO_EROSION_OPCION:
            suelo_erosion_indi[obj[1]] = OrderedDict()
            for algo in CHOICE_SUELO_EROSION_RESPUESTA:
                suelo_erosion_indi[obj[1]][algo[1]] = filtro.filter(puntoasuelo__opcion=obj[0], puntoasuelo__respuesta=algo[0],anio = anio[0]).count()

        years[anio[1]] = numero_parcelas,suelo_erosion_indi

    return render(request, template, locals())

def suelo_obras(request, template='transformacion/suelo/sueloObras.html'):
    filtro = _queryset_filtrado_suelo(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        suelo_obras_conservacion = OrderedDict()
        for obj in CHOICE_SUELO_CONSERVACION_OPCION:
            suelo_obras_conservacion[obj[1]] = OrderedDict()
            for algo in CHOICE_SUELO_CONSERVACION_RESPUESTA:
                suelo_obras_conservacion[obj[1]][algo[1]] = filtro.filter(anio = anio[0],puntobsuelo__opcion=obj[0], puntobsuelo__respuesta=algo[0]).count()

        years[anio[1]] = numero_parcelas,suelo_obras_conservacion

    return render(request, template, locals())

def suelo_indicador_drenaje(request, template='transformacion/suelo/sueloIndiDrenaje.html'):
    filtro = _queryset_filtrado_suelo(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        suelo_indi_drenaje = OrderedDict()
        for obj in CHOICE_SUELO_DRENAJE_OPCION:
            suelo_indi_drenaje[obj[1]] = OrderedDict()
            for algo in CHOICE_SUELO_EROSION_RESPUESTA:
                suelo_indi_drenaje[obj[1]][algo[1]] = filtro.filter(anio = anio[0],punto2asuelo__opcion=obj[0], punto2asuelo__respuesta=algo[0]).count()

        years[anio[1]] = numero_parcelas,suelo_indi_drenaje

    return render(request, template, locals())

def suelo_obras_drenaje(request, template='transformacion/suelo/sueloObrasDrenaje.html'):
    filtro = _queryset_filtrado_suelo(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        suelo_indi_drenaje = OrderedDict()
        for obj in CHOICE_SUELO_DRENAJE_OPCION2:
            suelo_indi_drenaje[obj[1]] = OrderedDict()
            for algo in CHOICE_SUELO_CONSERVACION_RESPUESTA:
                suelo_indi_drenaje[obj[1]][algo[1]] = filtro.filter(anio = anio[0],punto2bsuelo__opcion=obj[0], punto2bsuelo__respuesta=algo[0]).count()

        years[anio[1]] = numero_parcelas,suelo_indi_drenaje

    return render(request, template, locals())

def suelo_salud_raices(request, template='transformacion/suelo/sueloSaludRaices.html'):
    filtro = _queryset_filtrado_suelo(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        suelo_indi_drenaje = OrderedDict()
        for obj in CHOICE_SUELO_OPCION_PUNTOS:
            suelo_indi_drenaje[obj[1]] = OrderedDict()
            for algo in CHOICE_SUELO_RESPUESTA_PUNTOS:
                punto1 = filtro.filter(anio = anio[0],punto3suelopunto1__opcion=obj[0], punto3suelopunto1__respuesta=algo[0]).count()
                punto2 = filtro.filter(anio = anio[0],punto3suelopunto2__opcion=obj[0], punto3suelopunto2__respuesta=algo[0]).count()
                punto3 = filtro.filter(anio = anio[0],punto3suelopunto3__opcion=obj[0], punto3suelopunto3__respuesta=algo[0]).count()
                punto_total = punto1 + punto2 + punto3
                suelo_indi_drenaje[obj[1]][algo[1]] = punto_total

        years[anio[1]] = numero_parcelas,suelo_indi_drenaje

    return render(request, template, locals())

def suelo_varios(request, template='transformacion/suelo/sueloVarios.html'):
    filtro = _queryset_filtrado_suelo(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        #erosio de suelo
        suelo_erosion_acciones = OrderedDict()
        for obj in CHOICE_PUNTO9_LIMITACION_1_ACCION:
            conteo = filtro.filter(punto9erosion__acciones__contains=obj[0],anio = anio[0]).count()
            suelo_erosion_acciones[obj[1]] = conteo

        suelo_erosion_donde = OrderedDict()
        for obj in CHOICE_PUNTO9_DONDE:
            conteo = filtro.filter(punto9erosion__donde=obj[0],anio = anio[0]).count()
            suelo_erosion_donde[obj[1]] = conteo

        #mal drenaje y encharamiento
        suelo_drenaje_acciones = OrderedDict()
        for obj in CHOICE_PUNTO9_LIMITACION_2_ACCION:
            conteo = filtro.filter(punto9drenaje__acciones__contains=obj[0],anio = anio[0]).count()
            suelo_drenaje_acciones[obj[1]] = conteo

        suelo_drenaje_donde = OrderedDict()
        for obj in CHOICE_PUNTO9_DONDE:
            conteo = filtro.filter(punto9drenaje__donde=obj[0],anio = anio[0]).count()
            suelo_drenaje_donde[obj[1]] = conteo

        #Nutrientes
        suelo_nutrientes_acciones = OrderedDict()
        for obj in CHOICE_PUNTO9_LIMITACION_3_ACCION:
            conteo = filtro.filter(punto9nutrientes__acciones__contains=obj[0],anio = anio[0]).count()
            suelo_nutrientes_acciones[obj[1]] = conteo

        suelo_nutrientes_donde = OrderedDict()
        for obj in CHOICE_PUNTO9_DONDE:
            conteo = filtro.filter(punto9nutrientes__donde=obj[0],anio = anio[0]).count()
            suelo_nutrientes_donde[obj[1]] = conteo

        #excesos
        suelo_exceso_acciones = OrderedDict()
        for obj in CHOICE_PUNTO9_LIMITACION_4_ACCION:
            conteo = filtro.filter(punto9exceso__acciones__contains=obj[0],anio = anio[0]).count()
            suelo_exceso_acciones[obj[1]] = conteo

        suelo_exceso_donde = OrderedDict()
        for obj in CHOICE_PUNTO9_DONDE:
            conteo = filtro.filter(punto9exceso__donde=obj[0],anio = anio[0]).count()
            suelo_exceso_donde[obj[1]] = conteo

        #desbalance
        suelo_desbalance_acciones = OrderedDict()
        for obj in CHOICE_PUNTO9_LIMITACION_5_ACCION:
            conteo = filtro.filter(punto9desbalance__acciones__contains=obj[0],anio = anio[0]).count()
            suelo_desbalance_acciones[obj[1]] = conteo

        suelo_desbalance_donde = OrderedDict()
        for obj in CHOICE_PUNTO9_DONDE:
            conteo = filtro.filter(punto9desbalance__donde=obj[0],anio = anio[0]).count()
            suelo_desbalance_donde[obj[1]] = conteo

        #enfermedades
        suelo_enfermedades_acciones = OrderedDict()
        for obj in CHOICE_PUNTO9_LIMITACION_6_ACCION:
            conteo = filtro.filter(punto9enfermedades__acciones__contains=obj[0],anio = anio[0]).count()
            suelo_enfermedades_acciones[obj[1]] = conteo

        suelo_enfermedades_donde = OrderedDict()
        for obj in CHOICE_PUNTO9_DONDE:
            conteo = filtro.filter(punto9enfermedades__donde=obj[0],anio = anio[0]).count()
            suelo_enfermedades_donde[obj[1]] = conteo

        years[anio[1]] = [numero_parcelas,suelo_erosion_acciones,suelo_erosion_donde,suelo_drenaje_acciones,
                            suelo_drenaje_donde,suelo_nutrientes_acciones,suelo_nutrientes_donde,
                            suelo_exceso_acciones,suelo_exceso_donde,suelo_desbalance_acciones,
                            suelo_desbalance_donde,suelo_enfermedades_acciones,suelo_enfermedades_donde]

    return render(request, template, locals())


def suelo_salidas(request, template='transformacion/suelo/sueloSalidas.html'):
    filtro = _queryset_filtrado_suelo(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        salidas = OrderedDict()
        total_n = 0
        total_p = 0
        total_k = 0
        total_s = 0
        total_ca = 0
        total_mg = 0
        for obj in CHOICE_SUELO_PRODUCTO_COSECHA:
            if obj[0] == 1:
                cantidad = filtro.filter(punto4suelocosecha__producto=obj[0],anio = anio[0]).aggregate(pro=Avg('punto4suelocosecha__cantidad'))['pro'] or 0
                n = int(cantidad) * 2
                p = int(cantidad) * 0.4
                k = int(cantidad) * 1
                s = int(cantidad) * 0.14
                ca = int(cantidad) * 0.12
                mg = int(cantidad) * 0.34
            elif obj[0] == 2:
                cantidad = filtro.filter(punto4suelocosecha__producto=obj[0],anio = anio[0]).aggregate(pro=Avg('punto4suelocosecha__cantidad'))['pro'] or 0
                n = int(cantidad) * 1.1
                p = int(cantidad) * 1
                k = int(cantidad) * 0.6
                s =  0
                ca =  0
                mg =  0
            elif obj[0] == 3:
                cantidad = filtro.filter(punto4suelocosecha__producto=obj[0],anio = anio[0]).aggregate(pro=Avg('punto4suelocosecha__cantidad'))['pro'] or 0
                n = int(cantidad) * 0.068
                p = int(cantidad) * 0.009
                k = int(cantidad) * 0.234
                s = 0
                ca = 0
                mg = 0
            total_n += n
            total_p += p
            total_k += k
            total_s += s
            total_ca += ca
            total_mg += mg
            salidas[obj[1]] = (cantidad, n, p, k, s,ca,mg)

        tabla_analisis = OrderedDict()
        for obj in DatosAnalisis.objects.all().exclude(id=9):
            valor = filtro.filter(punto6analisissuelo__variable=obj,anio = anio[0]).aggregate(pro=Avg('punto6analisissuelo__valor'))['pro']
            tabla_analisis[obj] = [valor, obj.unidad,obj.valor_critico]

        densidad_aparente = tabla_analisis.values()[0][0]  #C4
        MO = tabla_analisis.values()[1][0]
        Nitro_n = tabla_analisis.values()[2][0]
        Fosfo_p = tabla_analisis.values()[3][0]
        Pota_k = tabla_analisis.values()[4][0]
        azu_s = tabla_analisis.values()[5][0]
        cal_ca = tabla_analisis.values()[6][0]
        mag_mg = tabla_analisis.values()[7][0]

        try:
            ideal_rango = (cal_ca + mag_mg) / float(Pota_k)
        except:
            ideal_rango = 0

        try:
            disponible_N = ((10000*0.3*densidad_aparente*1000)*(Nitro_n/float(100)))*0.01*(2.2*0.7072)
        except:
            disponible_N = 0
        try:
            disponible_P = ((((1000*3000*densidad_aparente)*Fosfo_p)/float(1000000))*2.2*0.7072)
        except:
            disponible_P = 0
        try:
            disponible_K = 780*densidad_aparente*Pota_k*2.2*0.7
        except:
            disponible_K = 0
        try:
            disponible_CA = 400*densidad_aparente*cal_ca*2.2*0.7026
        except:
            disponible_CA = 0
        try:
            disponible_MG = 240*densidad_aparente*mag_mg*2.2*0.7026
        except:
            disponible_MG = 0
        try:
            disponible_S  = ((((1000*3000*1.15)*azu_s)/float(1000000))*2.2*0.7072)
        except:
            disponible_S = 0

        #----------------- salidas de versitoles --------------------
        vertisoles_N =  (disponible_N * 60) / float(100)
        vertisoles_P = (disponible_P * 50) / float(100)
        vertisoles_K = (disponible_K * 65) / float(100)
        vertisoles_CA = (disponible_CA * 65) / float(100)
        vertisoles_MG = (disponible_MG * 65) / float(100)
        vertisoles_S = (disponible_S * 70) / float(100)

        #----------salidas de Andisoles ---------------
        andisoles_N =  (disponible_N * 65) / float(100)
        andisoles_P = (disponible_P * 35) / float(100)
        andisoles_K = (disponible_K * 80) / float(100)
        andisoles_CA = (disponible_CA * 80) / float(100)
        andisoles_MG = (disponible_MG * 80) / float(100)
        andisoles_S = (disponible_S * 70) / float(100)

        #----------salidas de ultisoles -------------------
        ultisoles_N =  (disponible_N * 55) / float(100)
        ultisoles_P = (disponible_P * 40) / float(100)
        ultisoles_K = (disponible_K * 70) / float(100)
        ultisoles_CA = (disponible_CA * 70) / float(100)
        ultisoles_MG = (disponible_MG * 70) / float(100)
        ultisoles_S = (disponible_S * 70) / float(100)

        years[anio[1]] = [numero_parcelas,salidas,tabla_analisis,ideal_rango,disponible_N,ultisoles_N,
                            vertisoles_N,andisoles_N,disponible_P,ultisoles_P,vertisoles_P,andisoles_P,
                            disponible_K,ultisoles_K,vertisoles_K,andisoles_K,disponible_CA,ultisoles_CA,
                            vertisoles_CA,andisoles_CA,disponible_MG,ultisoles_MG,vertisoles_MG,
                            andisoles_MG,disponible_S,ultisoles_S,vertisoles_S,andisoles_S]


    return render(request, template, locals())


def cierre_costo_cacao(request, template="transformacion/costo/costo_cacao.html"):
    filtro = _queryset_filtrado_cierre(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        area_total = filtro.filter(anio = anio[0]).aggregate(t=Sum('cierrecosto1__area_ha'))['t']
        lista_promedio1=filtro.filter(cierrecosto1__costo_us__gt=0,anio = anio[0]).values_list('cierrecosto1__costo_us', flat=True)

        lista_total_promedio = list(lista_promedio1)

        promedio_mo_dia = np.mean(lista_total_promedio)

        tabla_costo_cacao = OrderedDict()
        for obj in CHOICE_COSTO_CACAO_ACTIVIDAD:
            tmo_familia1 = filtro.filter(costocacaocierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costocacaocierre__mo_familiar'), V(0)))['t']
            tmo_contratada1 = filtro.filter(costocacaocierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costocacaocierre__mo_contratada'), V(0)))['t']
            insumos1 = filtro.filter(costocacaocierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costocacaocierre__costo_insumo_usd'), V(0)))['t']
            tabla_costo_cacao[obj[1]] = (tmo_familia1, tmo_contratada1, insumos1)


        #totales de costo
        total_costo_mo_familiar = 0
        total_costo_mo_contratada = 0
        total_costo_insumos = 0
        for k,v in tabla_costo_cacao.items():
            total_costo_mo_familiar += v[0]
            total_costo_mo_contratada += v[1]
            total_costo_insumos +=v[2]

        #Cosecha consolidada Cacao
        CHOICE_PRODUCTOS_CACAO = (
                            (1, 'Cacao en baba'),
                            (2, 'Cacao en grano seco'),
        )
        tabla_cosecha_cacao = OrderedDict()
        for obj in CHOICE_PRODUCTOS_CACAO:
            cantidad1 = filtro.filter(datoscosechacierre__productos=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('datoscosechacierre__cantidad'), V(0)))['t']
            precio1 = filtro.filter(datoscosechacierre__productos=obj[0],anio = anio[0]).aggregate(t=Coalesce(Avg('datoscosechacierre__precio_usd'), V(0)))['t']

            total_cosecha = cantidad1 * precio1
            tabla_cosecha_cacao[obj[1]] = (cantidad1, precio1, total_cosecha)

        suma_baba = 0
        suma_seco = 0
        suma_coseha = 0
        for k,v in tabla_cosecha_cacao.items():
            if k == 'Cacao en baba':
                suma_baba += v[0] / 3.0
            if k == 'Cacao en grano seco':
                suma_seco += v[0]
            suma_coseha += v[2]
        #nuevo total seco
        total_seco = suma_baba + suma_seco
        #Terce cuadro
        try:
            valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
        except:
            valor_mo_familiar = 0
        try:
            valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
        except:
            valor_mo_contratada = 0
        try:
            valor_insumos = total_costo_insumos
        except:
            valor_insumos = 0
        try:
            costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_insumos
        except:
            costo_total_produccion = 0
        try:
            prod_cacao_seco = total_seco
        except:
            prod_cacao_seco = 0
        try:
            productividad_cacao = prod_cacao_seco / float(area_total)
        except:
            productividad_cacao = 0
        try:
            costo_prod_unitario = costo_total_produccion / float(prod_cacao_seco)
        except:
            costo_prod_unitario = 0
        try:
            ingreso_neto = suma_coseha - costo_total_produccion
        except:
            ingreso_neto = 0
        try:
            ingreso_neto_ha = ingreso_neto / area_total
        except:
            ingreso_neto_ha = 0
        try:
            tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
        except:
            tasa_retorno = 0
        try:
            ingreso_neto_parcial = suma_coseha - valor_mo_contratada - total_costo_insumos
        except:
            ingreso_neto_parcial = 0
        try:
            valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
        except:
            valor_agregado_mo_familiar = 0

        years[anio[1]] = [numero_parcelas,area_total,promedio_mo_dia,tabla_costo_cacao,total_costo_mo_familiar,
                            total_costo_mo_contratada,total_costo_insumos,tabla_cosecha_cacao,suma_baba,suma_coseha,
                            valor_mo_familiar,valor_mo_contratada,costo_total_produccion,suma_seco,prod_cacao_seco,
                            productividad_cacao,costo_prod_unitario,ingreso_neto,ingreso_neto_ha,tasa_retorno,
                            ingreso_neto_parcial,valor_agregado_mo_familiar]

    return render(request, template, locals())

def cierre_costo_musaceas(request, template="transformacion/costo/costo_musaceas.html"):
    filtro = _queryset_filtrado_cierre(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        area_total = filtro.filter(anio = anio[0]).aggregate(t=Sum('cierrecosto1__area_ha'))['t']
        lista_promedio1=filtro.filter(cierrecosto1__costo_us__gt=0,anio = anio[0]).values_list('cierrecosto1__costo_us', flat=True)
        lista_total_promedio = list(lista_promedio1)
        promedio_mo_dia = np.mean(lista_total_promedio)

        tabla_costo_musaceas = OrderedDict()
        for obj in CHOICE_COSTO_MUSACEAS_ACTIVIDAD:
            tmo_familia1 = filtro.filter(costomusaceascierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costomusaceascierre__mo_familiar'), V(0)))['t']
            tmo_contratada1 = filtro.filter(costomusaceascierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costomusaceascierre__mo_contratada'), V(0)))['t']
            insumos1 = filtro.filter(costomusaceascierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costomusaceascierre__costo_insumo_usd'), V(0)))['t']

            tabla_costo_musaceas[obj[1]] = (tmo_familia1, tmo_contratada1, insumos1)


        #totales de costo
        total_costo_mo_familiar = 0
        total_costo_mo_contratada = 0
        total_costo_insumos = 0
        for k,v in tabla_costo_musaceas.items():
            total_costo_mo_familiar += v[0]
            total_costo_mo_contratada += v[1]
            total_costo_insumos +=v[2]

        #Cosecha consolidada Cacao
        CHOICE_PRODUCTOS_MUSACEAS = (
                            (4, 'Musaceas-Banano'),
                            (5, 'Musaceas-Plátano'),
        )
        tabla_cosecha_musaceas = OrderedDict()
        for obj in CHOICE_PRODUCTOS_MUSACEAS:
            cantidad1_kg = filtro.filter(datoscosechacierre__productos=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('datoscosechacierre__cantidad_g'), V(0)))['t']
            precio1_kg = filtro.filter(datoscosechacierre__productos=obj[0],anio = anio[0]).aggregate(t=Coalesce(Avg('datoscosechacierre__precio_usd_g'), V(0)))['t']

            total_cosecha = cantidad1_kg * precio1_kg

            tabla_cosecha_musaceas[obj[1]] = (cantidad1_kg, precio1_kg, total_cosecha)

        suma_banano = 0
        suma_platano = 0
        suma_coseha = 0
        for k,v in tabla_cosecha_musaceas.items():
            if k == 'Musaceas-Banano':
                suma_banano += v[0]
            if k == 'Musaceas-Plátano':
                suma_platano += v[0]
            suma_coseha += v[2]

        #tercer tabla
        try:
            valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
        except:
            valor_mo_familiar = 0
        try:
            valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
        except:
            valor_mo_contratada = 0
        try:
            valor_insumos = total_costo_insumos
        except:
            valor_insumos = 0
        try:
            costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_insumos
        except:
            costo_total_produccion = 0
        try:
            prod_musaceas = suma_banano + suma_platano
        except:
            prod_musaceas = 0
        try:
            productividad_musaceas = prod_musaceas / float(area_total)
        except:
            productividad_musaceas = 0
        try:
            costo_prod_unitario = costo_total_produccion / float(prod_musaceas)
        except:
            costo_prod_unitario = 0
        try:
            ingreso_neto = suma_coseha - costo_total_produccion
        except:
            ingreso_neto = 0
        try:
            ingreso_neto_ha = ingreso_neto / area_total
        except:
            ingreso_neto_ha = 0
        try:
            tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
        except:
            tasa_retorno = 0
        try:
            ingreso_neto_parcial = suma_coseha - valor_mo_contratada - total_costo_insumos
        except:
            ingreso_neto_parcial = 0
        try:
            valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
        except:
            valor_agregado_mo_familiar = 0

        years[anio[1]] = [numero_parcelas,area_total,promedio_mo_dia,tabla_costo_musaceas,total_costo_mo_familiar,
                            total_costo_mo_contratada,total_costo_insumos,tabla_cosecha_musaceas,suma_banano,
                            suma_platano,suma_coseha,valor_mo_familiar,valor_mo_contratada,valor_insumos,
                            costo_total_produccion,prod_musaceas,productividad_musaceas,costo_prod_unitario,
                            ingreso_neto,ingreso_neto_ha,tasa_retorno,ingreso_neto_parcial,valor_agregado_mo_familiar
                            ]

    return render(request, template, locals())

def cierre_costo_frutales(request, template="transformacion/costo/costo_frutales.html"):
    filtro = _queryset_filtrado_cierre(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        area_total = filtro.filter(anio = anio[0]).aggregate(t=Sum('cierrecosto1__area_ha'))['t']
        lista_promedio1=filtro.filter(cierrecosto1__costo_us__gt=0,anio = anio[0]).values_list('cierrecosto1__costo_us', flat=True)
        lista_total_promedio = list(lista_promedio1)
        promedio_mo_dia = np.mean(lista_total_promedio)

        tabla_costo_frutales = OrderedDict()
        for obj in CHOICE_COSTO_FRUTALES_ACTIVIDAD:
            tmo_familia1 = filtro.filter(costofrutalescierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costofrutalescierre__mo_familiar'), V(0)))['t']
            tmo_contratada1 = filtro.filter(costofrutalescierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costofrutalescierre__mo_contratada'), V(0)))['t']
            insumos1 = filtro.filter(costofrutalescierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costofrutalescierre__costo_insumo_usd'), V(0)))['t']

            tabla_costo_frutales[obj[1]] = (tmo_familia1, tmo_contratada1, insumos1)


        #totales de costo
        total_costo_mo_familiar = 0
        total_costo_mo_contratada = 0
        total_costo_insumos = 0
        for k,v in tabla_costo_frutales.items():
            total_costo_mo_familiar += v[0]
            total_costo_mo_contratada += v[1]
            total_costo_insumos +=v[2]

        #Cosecha consolidada Cacao
        CHOICE_PRODUCTOS_FRUTALES = (
                            (6, 'Frutales-Zapote'),
                            (7, 'Frutales-Cítricos'),
                            (8, 'Frutales-Aguacate'),
                            (9, 'Frutales-Coco'),
        )
        tabla_cosecha_frutales = OrderedDict()
        for obj in CHOICE_PRODUCTOS_FRUTALES:
            cantidad1 = filtro.filter(datoscosechacierre__productos=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('datoscosechacierre__cantidad'), V(0)))['t']
            precio1 = filtro.filter(datoscosechacierre__productos=obj[0],anio = anio[0]).aggregate(t=Coalesce(Avg('datoscosechacierre__precio_usd'), V(0)))['t']

            total_cosecha = cantidad1 * precio1
            tabla_cosecha_frutales[obj[1]] = (cantidad1, precio1, total_cosecha)

        suma_zapote = 0
        suma_citricos = 0
        suma_aguacate = 0
        suma_coco = 0
        suma_coseha = 0
        for k,v in tabla_cosecha_frutales.items():
            if k == 'Frutales-Zapote':
                suma_zapote += v[0]
            if k == 'Frutales-Cítricos':
                suma_citricos += v[0]
            if k == 'Frutales-Aguacate':
                suma_aguacate += v[0]
            if k == 'Frutales-Coco':
                suma_coco += v[0]
            suma_coseha += v[2]

        total_sumas = suma_zapote + suma_citricos +suma_aguacate + suma_coco

        #tercer tabla
        try:
            valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
        except:
            valor_mo_familiar = 0
        try:
            valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
        except:
            valor_mo_contratada = 0
        try:
            valor_insumos = total_costo_insumos
        except:
            valor_insumos = 0
        try:
            costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_insumos
        except:
            costo_total_produccion = 0
        try:
            prod_frutales = total_sumas
        except:
            prod_frutales = 0
        try:
            productividad_frutales = prod_frutales / float(area_total)
        except:
            productividad_frutales = 0
        try:
            costo_prod_unitario = costo_total_produccion / float(prod_frutales)
        except:
            costo_prod_unitario = 0
        try:
            ingreso_neto = suma_coseha - costo_total_produccion
        except:
            ingreso_neto = 0
        try:
            ingreso_neto_ha = ingreso_neto / area_total
        except:
            ingreso_neto_ha = 0
        try:
            tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
        except:
            tasa_retorno = 0
        try:
            ingreso_neto_parcial = suma_coseha - valor_mo_contratada - total_costo_insumos
        except:
            ingreso_neto_parcial = 0
        try:
            valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
        except:
            valor_agregado_mo_familiar = 0

        years[anio[1]] = [numero_parcelas,area_total,promedio_mo_dia,tabla_costo_frutales,total_costo_mo_familiar,
                            total_costo_mo_contratada,total_costo_insumos,tabla_cosecha_frutales,total_sumas,
                            suma_coseha,valor_mo_familiar,valor_mo_contratada,costo_total_produccion,prod_frutales,
                            productividad_frutales,costo_prod_unitario,ingreso_neto,ingreso_neto_ha,tasa_retorno,
                            ingreso_neto_parcial,valor_agregado_mo_familiar
                            ]

    return render(request, template, locals())

def cierre_costo_maderables(request, template="transformacion/costo/costo_maderables.html"):
    filtro = _queryset_filtrado_cierre(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        area_total = filtro.filter(anio = anio[0]).aggregate(t=Sum('cierrecosto1__area_ha'))['t']
        lista_promedio1=filtro.filter(cierrecosto1__costo_us__gt=0,anio = anio[0]).values_list('cierrecosto1__costo_us', flat=True)
        lista_total_promedio = list(lista_promedio1)
        promedio_mo_dia = np.mean(lista_total_promedio)

        tabla_costo_maderables = OrderedDict()
        for obj in CHOICE_COSTO_MADERABLES_ACTIVIDAD:
            tmo_familia1 = filtro.filter(costomaderablescierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costomaderablescierre__mo_familiar'), V(0)))['t']
            tmo_contratada1 = filtro.filter(costomaderablescierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costomaderablescierre__mo_contratada'), V(0)))['t']
            insumos1 = filtro.filter(costomaderablescierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costomaderablescierre__costo_insumo_usd'), V(0)))['t']

            tabla_costo_maderables[obj[1]] = (tmo_familia1, tmo_contratada1, insumos1)

        #totales de costo
        total_costo_mo_familiar = 0
        total_costo_mo_contratada = 0
        total_costo_insumos = 0
        for k,v in tabla_costo_maderables.items():
            total_costo_mo_familiar += v[0]
            total_costo_mo_contratada += v[1]
            total_costo_insumos +=v[2]

        #Cosecha consolidada Cacao
        CHOICE_PRODUCTOS_MADERABLES = (
                            (10, 'Madera-Caoba'),
                            (11, 'Madera-Cortez'),
                            (12, 'Madera-Roble'),
                            (13, 'Madera-Melina'),
                            (14, 'Madera-Granadillo'),
                            (15, 'Madera-Leña'),
        )
        tabla_cosecha_maderables = OrderedDict()
        for obj in CHOICE_PRODUCTOS_MADERABLES:
            cantidad1 = filtro.filter(datoscosechacierre__productos=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('datoscosechacierre__cantidad'), V(0)))['t']
            precio1 = filtro.filter(datoscosechacierre__productos=obj[0],anio = anio[0]).aggregate(t=Coalesce(Avg('datoscosechacierre__precio_usd'), V(0)))['t']

            total_cosecha = cantidad1 * precio1
            tabla_cosecha_maderables[obj[1]] = (cantidad1, precio1, total_cosecha)

        suma_caoba = 0
        suma_cortez = 0
        suma_roble = 0
        suma_melina = 0
        suma_granadillo = 0
        suma_lena = 0
        suma_coseha = 0
        for k,v in tabla_cosecha_maderables.items():
            if k == 'Madera-Caoba':
                suma_caoba += v[0]
            if k == 'Madera-Cortez':
                suma_cortez += v[0]
            if k == 'Madera-Roble':
                suma_roble += v[0]
            if k == 'Madera-Melina':
                suma_melina += v[0]
            if k == 'Madera-Granadillo':
                suma_granadillo += v[0]
            if k == 'Madera-Leña':
                suma_lena += v[0]
            suma_coseha += v[2]

        total_sumas = suma_caoba + suma_cortez +suma_roble + suma_melina + suma_granadillo +suma_lena

        #tercer tabla
        try:
            valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
        except:
            valor_mo_familiar = 0
        try:
            valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
        except:
            valor_mo_contratada = 0
        try:
            valor_insumos = total_costo_insumos
        except:
            valor_insumos = 0
        try:
            costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_insumos
        except:
            costo_total_produccion = 0
        try:
            prod_maderables = total_sumas
        except:
            prod_maderables = 0
        try:
            productividad_maderables = prod_maderables / float(area_total)
        except:
            productividad_maderables = 0
        try:
            costo_prod_unitario = costo_total_produccion / float(prod_maderables)
        except:
            costo_prod_unitario = 0
        try:
            ingreso_neto = suma_coseha - costo_total_produccion
        except:
            ingreso_neto = 0
        try:
            ingreso_neto_ha = ingreso_neto / area_total
        except:
            ingreso_neto_ha = 0
        try:
            tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
        except:
            tasa_retorno = 0
        try:
            ingreso_neto_parcial = suma_coseha - valor_mo_contratada - total_costo_insumos
        except:
            ingreso_neto_parcial = 0
        try:
            valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
        except:
            valor_agregado_mo_familiar = 0

        years[anio[1]] = [numero_parcelas,area_total,promedio_mo_dia,tabla_costo_maderables,total_costo_mo_familiar,
                            total_costo_mo_contratada,total_costo_insumos,tabla_cosecha_maderables,total_sumas,
                            suma_coseha,valor_mo_familiar,valor_mo_contratada,costo_total_produccion,prod_maderables,
                            productividad_maderables,costo_prod_unitario,ingreso_neto,ingreso_neto_ha,tasa_retorno,
                            ingreso_neto_parcial,valor_agregado_mo_familiar
                            ]


    return render(request, template, locals())

def cierre_costo_arboles(request, template="transformacion/costo/costo_arboles.html"):
    filtro = _queryset_filtrado_cierre(request)
    anios = filtro.values_list('anio','anio__nombre').distinct('anio')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(anio = anio[0]).count()

        area_total = filtro.filter(anio = anio[0]).aggregate(t=Sum('cierrecosto1__area_ha'))['t']
        lista_promedio1=filtro.filter(cierrecosto1__costo_us__gt=0,anio = anio[0]).values_list('cierrecosto1__costo_us', flat=True)
        lista_total_promedio = list(lista_promedio1)
        promedio_mo_dia = np.mean(lista_total_promedio)

        tabla_costo_arboles = OrderedDict()
        for obj in CHOICE_COSTO_SOMBRA_TEMPORAL_ACTIVIDAD:
            tmo_familia1 = filtro.filter(costoarbolescierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costoarbolescierre__mo_familiar'), V(0)))['t']
            tmo_contratada1 = filtro.filter(costoarbolescierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costoarbolescierre__mo_contratada'), V(0)))['t']
            insumos1 = filtro.filter(costoarbolescierre__actividades=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('costoarbolescierre__costo_insumo_usd'), V(0)))['t']

            tabla_costo_arboles[obj[1]] = (tmo_familia1, tmo_contratada1, insumos1)


        #totales de costo
        total_costo_mo_familiar = 0
        total_costo_mo_contratada = 0
        total_costo_insumos = 0
        for k,v in tabla_costo_arboles.items():
            total_costo_mo_familiar += v[0]
            total_costo_mo_contratada += v[1]
            total_costo_insumos +=v[2]

        #Cosecha consolidada Cacao
        CHOICE_PRODUCTOS_ARBOLES = (
                            (18, 'Gandul'),
                            (19, 'Guaba'),
        )
        tabla_cosecha_arboles = OrderedDict()
        for obj in CHOICE_PRODUCTOS_ARBOLES:
            cantidad1 = filtro.filter(datoscosechacierre__productos=obj[0],anio = anio[0]).aggregate(t=Coalesce(Sum('datoscosechacierre__cantidad'), V(0)))['t']
            precio1 = filtro.filter(datoscosechacierre__productos=obj[0],anio = anio[0]).aggregate(t=Coalesce(Avg('datoscosechacierre__precio_usd'), V(0)))['t']

            total_cosecha = cantidad1 * precio1
            tabla_cosecha_arboles[obj[1]] = (cantidad1, precio1, total_cosecha)

        suma_gandul = 0
        suma_guaba = 0
        suma_coseha = 0
        for k,v in tabla_cosecha_arboles.items():
            if k == 'Gandul':
                suma_gandul += v[0]
            if k == 'Guaba':
                suma_guaba += v[0]
            suma_coseha += v[2]

        total_sumas = suma_gandul + suma_guaba

        #tercer tabla
        try:
            valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
        except:
            valor_mo_familiar = 0
        try:
            valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
        except:
            valor_mo_contratada = 0
        try:
            valor_insumos = total_costo_insumos
        except:
            valor_insumos = 0
        try:
            costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_insumos
        except:
            costo_total_produccion = 0
        try:
            prod_arboles = total_sumas
        except:
            prod_arboles = 0
        try:
            productividad_arboles = prod_arboles / float(area_total)
        except:
            productividad_arboles = 0
        try:
            costo_prod_unitario = costo_total_produccion / float(prod_arboles)
        except:
            costo_prod_unitario = 0
        try:
            ingreso_neto = suma_coseha - costo_total_produccion
        except:
            ingreso_neto = 0
        try:
            ingreso_neto_ha = ingreso_neto / area_total
        except:
            ingreso_neto_ha = 0
        try:
            tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
        except:
            tasa_retorno = 0
        try:
            ingreso_neto_parcial = suma_coseha - valor_mo_contratada - total_costo_insumos
        except:
            ingreso_neto_parcial = 0
        try:
            valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
        except:
            valor_agregado_mo_familiar = 0

        years[anio[1]] = [numero_parcelas,area_total,promedio_mo_dia,tabla_costo_arboles,total_costo_mo_familiar,
                            total_costo_mo_contratada,total_costo_insumos,tabla_cosecha_arboles,total_sumas,
                            suma_coseha,valor_mo_familiar,valor_mo_contratada,costo_total_produccion,prod_arboles,
                            productividad_arboles,costo_prod_unitario,ingreso_neto,ingreso_neto_ha,tasa_retorno,
                            ingreso_neto_parcial,valor_agregado_mo_familiar
                            ]

    return render(request, template, locals())

def contact(request):
    form_class = FormularioColabora
    if request.method == 'POST':
        form = form_class(data=request.POST)

        if form.is_valid():
            nombre = request.POST.get('nombre', '')
            correo = request.POST.get('correo', '')
            asunto = request.POST.get('asunto', '')

            template = get_template('transformacion/contact_template.txt')
            context = Context({
                'nombre': nombre,
                'correo': correo,
                'asunto': asunto,
            })
            content = template.render(context)

            email = EmailMessage(
                "Nuevo mensaje de colabora en cci",
                content,
                "alianza-cac" +'',
                ['fguharay@gmail.com'],
                headers = {'Reply-To': correo }
            )
            email.send()
            messages.success(request, 'El mensaje fue enviado, gracias, nos pondremos en contacto con usted.')
            return redirect('contactar-cacao')


    return render(request, 'transformacion/colabora.html', {'form': form_class,})

#----------  funciones utilitarias --------------------------

def crear_rangos(request, lista, start=0, stop=0, step=0):
    dict_algo = OrderedDict()
    rangos = []
    contador = 0
    rangos = [(n, n+int(step)-1) for n in range(int(start), int(stop), int(step))]

    for desde, hasta in rangos:
        dict_algo['%s a %s' % (desde,hasta)] = len([x for x in lista if desde <= x <= hasta])

    return dict_algo


def get_productor(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        personas = Persona.objects.filter(nombre__icontains = q, tipo_persona=1 )[:10]
        #print personas
        results = []
        for person in personas:
            personas_json = {}
            personas_json['id'] = person.id
            personas_json['label'] = person.nombre
            personas_json['value'] = person.nombre
            results.append(personas_json)
    else:
        results = 'fail'
    return HttpResponse(simplejson.dumps(results), content_type='application/json')
