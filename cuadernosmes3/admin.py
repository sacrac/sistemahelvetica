# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from lineabase.forms import EntrevistadoForm
from .models import *

class CacaoInline(admin.TabularInline):
    model = Cacao
    extra = 1

class MusaceasInlines(admin.TabularInline):
    model = Musaceas
    extra = 1

class FrutalesInlines(admin.TabularInline):
    model = Frutales
    extra = 1

class MaderablesInlines(admin.TabularInline):
    model = Maderables
    extra = 1

class SombraTemporalInlines(admin.TabularInline):
    model = SombraTemporal
    extra = 1

class DatosSueloInlines(admin.TabularInline):
    model = DatosSuelo
    extra = 1
    max_num = 1

class AnalisisSueloInlines(admin.TabularInline):
    model = AnalisisSuelo
    extra = 1

class TipoTexturaSueloInlines(admin.TabularInline):
    model = TipoTexturaSuelo
    extra = 1
    max_num = 1

class PlanManejoFertilizanteInlines(admin.TabularInline):
    model = PlanManejoFertilizante
    extra = 1
    max_num = 1

class AbonoAplicadoSueloInlines(admin.TabularInline):
    model = AbonoAplicadoSuelo
    extra = 1

class AbonoAplicadoFoliarInlines(admin.TabularInline):
    model = AbonoAplicadoFoliar
    extra = 1

class CostoManoObraDiaInlines(admin.TabularInline):
    model = CostoManoObraDia
    extra = 1
    max_num = 1

class CostoCacaoInlines(admin.TabularInline):
    model = CostoCacao
    extra = 1

class CostoMusaceasInlines(admin.TabularInline):
    model = CostoMusaceas
    extra = 1

class CostoFrutalesInlines(admin.TabularInline):
    model = CostoFrutales
    extra = 1

class CostoMaderablesInlines(admin.TabularInline):
    model = CostoMaderables
    extra = 1

class CostoSombraTemporalInlines(admin.TabularInline):
    model = CostoSombraTemporal
    extra = 1

class DatosCosechaInlines(admin.TabularInline):
    model = DatosCosecha
    extra = 1

# inlines de año 2
class DatosPiso1Inlines(admin.TabularInline):
    model = DatosPiso1
    extra = 1

class DatosPiso2Inlines(admin.TabularInline):
    model = DatosPiso2
    extra = 1

class DatosPiso3Inlines(admin.TabularInline):
    model = DatosPiso3
    extra = 1

class AccionesManejoPisoInlines(admin.TabularInline):
    model = AccionesManejoPiso
    extra = 1

class CuadernoMesTresAdmin(admin.ModelAdmin):
    form = EntrevistadoForm
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(CuadernoMesTresAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(CuadernoMesTresAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    list_display = ('entrevistado', 'get_org_apoyo', 'ciclo','year')
    search_fields = ['entrevistado__nombre']
    list_filter = ['year']
    inlines = [CacaoInline, MusaceasInlines,
                     FrutalesInlines,MaderablesInlines,
                     SombraTemporalInlines,DatosSueloInlines,
                     AnalisisSueloInlines,TipoTexturaSueloInlines,
                     PlanManejoFertilizanteInlines,AbonoAplicadoSueloInlines,
                     AbonoAplicadoFoliarInlines,DatosPiso1Inlines,DatosPiso2Inlines,
                     DatosPiso3Inlines,AccionesManejoPisoInlines,
                     CostoManoObraDiaInlines,CostoCacaoInlines,
                     CostoMusaceasInlines,CostoFrutalesInlines,
                     CostoMaderablesInlines,CostoSombraTemporalInlines,
                     DatosCosechaInlines]

    def get_org_apoyo(self, obj):
        try:
            nombre = obj.entrevistado.organizacion_apoyo.nombre
        except:
            nombre = "No tiene"
        return nombre
    get_org_apoyo.short_description = 'Organización de apoyo'
    get_org_apoyo.admin_order_field = 'entrevistado__organizacion_apoyo__nombre'

    class Media:
        css = {
            'all': ('/static/admin/css/admin_mes_3.css',)

        }
        js = ('/static/admin/js/admin_mes_3.js',)
# Register your models here.
admin.site.register(CuadernoMesTres, CuadernoMesTresAdmin)
admin.site.register(DatosAnalisis)
admin.site.register(ProductoSuelo)
admin.site.register(ProductoFoliar)
