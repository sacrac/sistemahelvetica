from django.conf.urls import url
from .views import *
from django.views.generic import TemplateView

urlpatterns =  [
    url(r'^consultar-rentabilidad/', consulta_ficha, name='consulta-rentabilidad'),
    #url(r'^api/productor/', get_productor, name='productor-search'),
    #url(r'^ajax/load-departamentos/', load_departamentos, name='ajax_load_departamentos'),
    #url(r'^ajax/load-municipios/', load_municipios, name='ajax_load_municipios'),
    #url(r'^ajax/load-comunidad/', load_comunidades, name='ajax_load_comunidad'),
    #url(r'^mapaherramienta/', obtener_lista_mapa_cacao, name='obtener-lista-mapa-cacao'),
    url(r'^(?P<vista>\w+)/$', get_view, name='get-view'),
]
