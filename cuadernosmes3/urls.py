from django.conf.urls import url
from .views import *

urlpatterns =  [
    url(r'^suelo/historial-suelo/', historial_limitaciones, name='historial-suelo'),
    url(r'^suelo/pendiente-suelo/', pendiente_campactacion, name='pendiente-suelo'),
    url(r'^suelo/caracteristicas-suelo/', caracteristicas, name='caracteristicas-suelo'),
    url(r'^suelo/fertilidad-suelo/', fertilidad_suelo, name='fertilidad-suelo'),
    url(r'^suelo/limitantes-suelo/', limitantes, name='limitantes-suelo'),
    url(r'^esta/establecimientos-exito/', establecimiento_exito, name='establecimientos-exito'),
    url(r'^esta/establecimientos-crecimiento/', establecimiento_crecimiento, name='establecimientos-crecimiento'),
    url(r'^costo/cacao/', costo_cacao, name='costo-cacao'),
    url(r'^costo/musaceas/', costo_musaceas, name='costo-musaceas'),
    url(r'^costo/frutales/', costo_frutales, name='costo-frutales'),
    url(r'^costo/maderables/', costo_maderables, name='costo-maderables'),
    url(r'^costo/arboles-servicios/', costo_arboles, name='costo-arboles-servicios'),
]
