# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-01-30 05:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cuadernosmes9', '0005_auto_20190129_2248'),
    ]

    operations = [
        migrations.AlterField(
            model_name='costocacao',
            name='insumos',
            field=models.CharField(max_length=250),
        ),
        migrations.AlterField(
            model_name='costofrutales',
            name='insumos',
            field=models.CharField(max_length=250),
        ),
        migrations.AlterField(
            model_name='costomaderables',
            name='insumos',
            field=models.CharField(max_length=250),
        ),
        migrations.AlterField(
            model_name='costomusaceas',
            name='insumos',
            field=models.CharField(max_length=250),
        ),
        migrations.AlterField(
            model_name='costosombratemporal',
            name='insumos',
            field=models.CharField(max_length=250),
        ),
    ]
