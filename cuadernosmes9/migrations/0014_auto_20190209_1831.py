# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-02-10 00:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cuadernosmes9', '0013_auto_20190207_1838'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='especies',
            options={'ordering': ['nombre'], 'verbose_name_plural': 'Especies'},
        ),
        migrations.AddField(
            model_name='datoscosecha',
            name='cantidad_g',
            field=models.FloatField(blank=True, editable=False, null=True),
        ),
        migrations.AddField(
            model_name='datoscosecha',
            name='precio_usd_g',
            field=models.FloatField(blank=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='datoscosecha',
            name='unidad',
            field=models.IntegerField(choices=[(1, b'qq'), (2, b'Cabeza'), (3, b'Lb'), (4, b'Kg'), (5, b'Docena'), (6, b'Cien'), (7, b'Saco'), (8, b'M3'), (9, b'Pt'), (10, b'Unidad'), (11, b'Carga')]),
        ),
    ]
