from django.conf.urls import url
from .views import *

urlpatterns =  [
    url(r'^piso/composicion-piso/', orientacion_composicion_piso, name='composicion-piso'),
    url(r'^piso/propuesta-piso/', propuesta_piso, name='propuesta-piso'),
    url(r'^estado/estado-cacao/', estado_cacao, name='estado-cacao'),
    url(r'^estado/estado-musaceas/', estado_musaceas, name='estado-musaceas'),
    url(r'^estado/estado-frutales/', estado_frutales, name='estado-frutales'),
    url(r'^estado/estado-maderables/', estado_maderables, name='estado-maderables'),
    url(r'^estado/estado-arboles/', estado_arboles_servicios, name='estado-arboles'),
    url(r'^estado/estado-problemas/', estado_problemas, name='estado-problemas'),
]
