# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.db.models import Avg, Sum, F, Value as V
from django.db.models.functions import Coalesce

from cuadernosmes3.models import *
from cuadernosmes0.choices_static import *
from cuadernosmes0.views import _queryset_filtrado_cuaderno0
from cuadernosmes6.views import _queryset_filtrado_cuaderno6
from cuadernosmes9.views import _queryset_filtrado_cuaderno9

from collections import OrderedDict, Counter
import numpy as np
from itertools import chain

# Create your views here.

def _queryset_filtrado_cuaderno3(request):
    params = {}

    if 'ciclo' in request.session:
        params['ciclo__nombre'] = request.session['ciclo']

    if 'productor' in request.session:
        params['entrevistado__nombre'] = request.session['productor']

    if 'organizacion_pertenece' in request.session:
        params['entrevistado__organizacion_pertenece'] = request.session['organizacion_pertenece']

    if 'organizacion_apoyo' in request.session:
        params['entrevistado__organizacion_apoyo__in'] = request.session['organizacion_apoyo']

    if 'pais' in request.session:
        params['entrevistado__pais'] = request.session['pais']

    if 'departamento' in request.session:
        params['entrevistado__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['entrevistado__municipio'] = request.session['municipio']

    if 'comunidad' in request.session:
        params['entrevistado__comunidad'] = request.session['comunidad']

    if 'sexo' in request.session:
        params['entrevistado__sexo'] = request.session['sexo']

    if 'parcela' in request.session:
        params['entrevistado__tamanio_parcela__in'] = request.session['parcela']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    print "parametros cuaderno 3"
    print params

    return CuadernoMesTres.objects.filter(**params)


def historial_limitaciones(request, template='cuadernotres/historialLimitaciones.html'):
    filtro = _queryset_filtrado_cuaderno3(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        uso_parcela = OrderedDict()
        for obj in CHOICE_USO_PARCELA:
            conteo = filtro.filter(datossuelo__uso_parcela=obj[0],year = anio[0]).count()
            uso_parcela[obj[1]] = conteo

        suelo_limitante = OrderedDict()
        for obj in CHOICE_LIMITANTES_PRODUCTIVOS:
            conteo = filtro.filter(datossuelo__limitantes__contains=obj[0],year = anio[0]).count()
            suelo_limitante[obj[1]] = conteo

        years[anio[1]] = numero_parcelas,uso_parcela,suelo_limitante

    return render(request, template, locals())

def pendiente_campactacion(request, template='cuadernotres/pendiente.html'):
    filtro = _queryset_filtrado_cuaderno3(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        tabla_pendiente = []
        for obj in filtro.filter(year = anio[0]):
            try:
                plano = 0 * obj.datossuelo_set.values_list('plano', flat=True)[0]
            except:
                plano = 0
            try:
                ondulado = 30 * obj.datossuelo_set.values_list('ondulado', flat=True)[0]
            except:
                ondulado = 0
            try:
                pendiente = 70 * obj.datossuelo_set.values_list('pendiente', flat=True)[0]
            except:
                pendiente = 0
            total = float(plano + ondulado + pendiente) / 100
            tabla_pendiente.append(total)

        # media arítmetica
        promedio_pendiente = np.mean(tabla_pendiente)
        # mediana
        mediana_pendiente = np.median(tabla_pendiente)
        #minimo
        try:
            minimo_pendiente = min(tabla_pendiente)
        except:
            minimo_pendiente = 0
        #maximo
        try:
            maximo_pendiente = max(tabla_pendiente)
        except:
            maximo_pendiente = 0

        #rangos
        grafo_pendiente = crear_rangos(request, tabla_pendiente, minimo_pendiente, maximo_pendiente, step=2)

        tabla_compactacion = []
        for obj in filtro.filter(year = anio[0]):
            try:
                muy_baja = 90 * obj.datossuelo_set.values_list('muy_baja', flat=True)[0]
            except:
                muy_baja = 0
            try:
                baja = 40 * obj.datossuelo_set.values_list('baja', flat=True)[0]
            except:
                baja = 0
            try:
                regular = 15 * obj.datossuelo_set.values_list('regular', flat=True)[0]
            except:
                regular = 0
            try:
                buena = 5 * obj.datossuelo_set.values_list('buena', flat=True)[0]
            except:
                buena = 0
            total = float(muy_baja + baja + regular + buena) / 100
            tabla_compactacion.append(total)

        # media arítmetica
        promedio_compactacion = np.mean(tabla_compactacion)
        # mediana
        mediana_compactacion = np.median(tabla_compactacion)
        #minimo
        try:
            minimo_compactacion = min(tabla_compactacion)
        except:
            minimo_compactacion = 0
        #maximo
        try:
            maximo_compactacion = max(tabla_compactacion)
        except:
            maximo_compactacion = 0

        #rangos
        grafo_compactacion = crear_rangos(request, tabla_compactacion, minimo_compactacion, maximo_compactacion, step=2)

        years[anio[1]] = [numero_parcelas,promedio_pendiente,mediana_pendiente,maximo_pendiente,minimo_pendiente,
                            promedio_compactacion,mediana_compactacion,maximo_compactacion,minimo_compactacion,
                            grafo_pendiente,grafo_compactacion]

    return render(request, template, locals())

def limitantes(request, template='cuadernotres/limitante_suelo.html'):
    filtro = _queryset_filtrado_cuaderno3(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        ph = filtro.filter(analisissuelo__variable__id=1, analisissuelo__valor__lte=5,year = anio[0]).count()
        densidad = filtro.filter(analisissuelo__variable__id=2, analisissuelo__valor__gte=1.5,year = anio[0]).count()
        materia = filtro.filter(analisissuelo__variable__id=3, analisissuelo__valor__lte=2,year = anio[0]).count()
        nitro =  filtro.filter(analisissuelo__variable__id=4, analisissuelo__valor__lte=0.2,year = anio[0]).count()
        fosforo =  filtro.filter(analisissuelo__variable__id=5, analisissuelo__valor__lte=10,year = anio[0]).count()
        potasio =  filtro.filter(analisissuelo__variable__id=6, analisissuelo__valor__lte=0.5,year = anio[0]).count()
        azufre =  filtro.filter(analisissuelo__variable__id=7, analisissuelo__valor__lte=12,year = anio[0]).count()
        calcio =  filtro.filter(analisissuelo__variable__id=8, analisissuelo__valor__lte=5,year = anio[0]).count()
        magnesio =  filtro.filter(analisissuelo__variable__id=9, analisissuelo__valor__lte=1.6,year = anio[0]).count()

        ph_lista = filtro.filter(analisissuelo__variable__id=1,year = anio[0]).values_list('analisissuelo__valor', flat=True)
        densidad_lista = filtro.filter(analisissuelo__variable__id=2,year = anio[0]).values_list('analisissuelo__valor', flat=True)
        materia_lista = filtro.filter(analisissuelo__variable__id=3,year = anio[0]).values_list('analisissuelo__valor', flat=True)
        nitro_lista =  filtro.filter(analisissuelo__variable__id=4,year = anio[0]).values_list('analisissuelo__valor', flat=True)
        fosforo_lista =  filtro.filter(analisissuelo__variable__id=5,year = anio[0]).values_list('analisissuelo__valor', flat=True)
        potasio_lista =  filtro.filter(analisissuelo__variable__id=6,year = anio[0]).values_list('analisissuelo__valor', flat=True)
        azufre_lista =  filtro.filter(analisissuelo__variable__id=7,year = anio[0]).values_list('analisissuelo__valor', flat=True)
        calcio_lista =  filtro.filter(analisissuelo__variable__id=8,year = anio[0]).values_list('analisissuelo__valor', flat=True)
        magnesio_lista =  filtro.filter(analisissuelo__variable__id=9,year = anio[0]).values_list('analisissuelo__valor', flat=True)

        years[anio[1]] = [numero_parcelas,ph,densidad,materia,nitro,fosforo,potasio,azufre,calcio,magnesio,
                            ph_lista,densidad_lista,materia_lista,nitro_lista,fosforo_lista,potasio_lista,
                            azufre_lista,calcio_lista,magnesio_lista]

    return render(request, template, locals())

def caracteristicas(request, template='cuadernotres/caracteristica_suelo.html'):
    filtro = _queryset_filtrado_cuaderno3(request)

    CHOICE_TIPO_SUELO = (
                                        (1,'Ultisol (rojo)'),
                                        (2, 'Andisol (volcánico)'),
                                        (3, 'Vertisol'),
                                        )

    CHOICE_TEXTURA =  (
                                       (1,'Arcilloso '),
                                        (2, 'Limoso'),
                                        (3, 'Arenoso '),
                                        (4, 'Arcilloso-Limoso  ')
                                        )

    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        grafo_tipo_suelo = OrderedDict()
        for obj in CHOICE_TIPO_SUELO:
            conteo = filtro.filter(tipotexturasuelo__tipo=obj[0],year = anio[0]).count()
            grafo_tipo_suelo[obj[1]] = conteo

        grafo_tipo_textura = OrderedDict()
        for obj in CHOICE_TEXTURA:
            conteo = filtro.filter(tipotexturasuelo__textura=obj[0],year = anio[0]).count()
            grafo_tipo_textura[obj[1]] = conteo

        tabla_analisis = OrderedDict()
        for obj in DatosAnalisis.objects.all().exclude(id=9):
            valor = filtro.filter(analisissuelo__variable=obj,year = anio[0]).aggregate(pro=Avg('analisissuelo__valor'))['pro']
            tabla_analisis[obj] = [valor, obj.unidad,obj.valor_critico]

        years[anio[1]] = numero_parcelas,grafo_tipo_suelo,grafo_tipo_textura

    return render(request, template, locals())

def fertilidad_suelo(request, template='cuadernotres/fertilidad.html'):
    filtro = _queryset_filtrado_cuaderno3(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        grafo_tipo_fertilidad = OrderedDict()
        for obj in CHOICE_TIPO_FERTILIZA:
            conteo = filtro.filter(planmanejofertilizante__tipo_fertilizacion=obj[0],year = anio[0]).count()
            grafo_tipo_fertilidad[obj[1]] = conteo

        grafo_modo_fertilidad = OrderedDict()
        for obj in CHOICE_MODO_FERTILIZA:
            conteo = filtro.filter(planmanejofertilizante__modo_fertilizacion=obj[0],year = anio[0]).count()
            grafo_modo_fertilidad[obj[1]] = conteo

        grafo_intensidad_fertilidad = OrderedDict()
        for obj in CHOICE_INTENSIDAD_FERTILIZA:
            conteo = filtro.filter(planmanejofertilizante__intensidad_fertilizacion=obj[0],year = anio[0]).count()
            grafo_intensidad_fertilidad[obj[1]] = conteo


        organico_intensivo = filtro.filter(year = anio[0],planmanejofertilizante__tipo_fertilizacion=1,planmanejofertilizante__intensidad_fertilizacion=1).count()
        organico_semi = filtro.filter(year = anio[0],planmanejofertilizante__tipo_fertilizacion=1,planmanejofertilizante__intensidad_fertilizacion=2).count()

        quimi_intensivo = filtro.filter(year = anio[0],planmanejofertilizante__tipo_fertilizacion=2,planmanejofertilizante__intensidad_fertilizacion=1).count()
        quimi_semi = filtro.filter(year = anio[0],planmanejofertilizante__tipo_fertilizacion=2,planmanejofertilizante__intensidad_fertilizacion=2).count()

        quimi_org_intensivo = filtro.filter(year = anio[0],planmanejofertilizante__tipo_fertilizacion=3,planmanejofertilizante__intensidad_fertilizacion=1).count()
        quimi__org_semi = filtro.filter(year = anio[0],planmanejofertilizante__tipo_fertilizacion=3,planmanejofertilizante__intensidad_fertilizacion=2).count()

        ninguno = filtro.filter(year = anio[0],planmanejofertilizante__tipo_fertilizacion=4,planmanejofertilizante__intensidad_fertilizacion=3).count()

        tabla_aplicar_suelo = OrderedDict()
        for obj in ProductoSuelo.objects.all():
            conteo = filtro.filter(year = anio[0],abonoaplicadosuelo__producto=obj).count()
            if conteo > 0:
                lista_dosis = filtro.filter(year = anio[0],abonoaplicadosuelo__producto=obj).values_list('abonoaplicadosuelo__dosis_ha', flat=True)
                lista_veces = filtro.filter(year = anio[0],abonoaplicadosuelo__producto=obj).values_list('abonoaplicadosuelo__frecuencia_veces', flat=True)
                tabla_aplicar_suelo[obj.nombre] = (conteo, max(lista_dosis), min(lista_dosis),max(lista_veces),min(lista_veces))

        tabla_aplicar_foliar = OrderedDict()
        for obj in ProductoFoliar.objects.all():
            conteo = filtro.filter(year = anio[0],abonoaplicadofoliar__producto=obj).count()
            if conteo > 0:
                lista_dosis = filtro.filter(year = anio[0],abonoaplicadofoliar__producto=obj).values_list('abonoaplicadofoliar__dosis_lt_ha', flat=True)
                lista_veces = filtro.filter(year = anio[0],abonoaplicadofoliar__producto=obj).values_list('abonoaplicadofoliar__frecuencia_veces', flat=True)
                tabla_aplicar_foliar[obj.nombre] = (conteo, max(lista_dosis), min(lista_dosis),max(lista_veces),min(lista_veces))

        years[anio[1]] = [numero_parcelas,grafo_tipo_fertilidad,grafo_modo_fertilidad,grafo_intensidad_fertilidad,
                            organico_intensivo,organico_semi,quimi_intensivo,quimi_semi,quimi_org_intensivo,
                            quimi__org_semi,ninguno,tabla_aplicar_suelo,tabla_aplicar_foliar]

    return render(request, template, locals())


def establecimiento_exito(request, template='cuadernotres/establecimiento.html'):
    filtro = _queryset_filtrado_cuaderno3(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()

        tabla_exito_cacao = OrderedDict()
        for obj in ClonesVariedades.objects.all():
            total = filtro.filter(cacao__clones_variedad=obj,year = anio[0]).aggregate(sembrada=Sum('cacao__cantidad_sembrada'),
                                                                                                                    establecida=Sum('cacao__cantidad_establecida'),
                                                                                                                    resiembra=Sum('cacao__cantidad_resiembra'))
            if total['sembrada'] or total['establecida'] or total['resiembra']:
                tabla_exito_cacao[obj] = total

        tabla_exito_musaceas = OrderedDict()
        for obj in TiposMusaceas.objects.all():
            total = filtro.filter(musaceas__tipo_musaceas=obj,year = anio[0]).aggregate(sembrada=Sum('musaceas__cantidad_sembrada'),
                                                                                                                    establecida=Sum('musaceas__cantidad_establecida'),
                                                                                                                    resiembra=Sum('musaceas__cantidad_resiembra'))
            if total['sembrada'] or total['establecida'] or total['resiembra']:
                tabla_exito_musaceas[obj] = total

        tabla_exito_frutales = OrderedDict()
        for obj in EspeciesFrutales.objects.all():
            total = filtro.filter(frutales__especies=obj,year = anio[0]).aggregate(sembrada=Sum('frutales__cantidad_sembrada'),
                                                                                                                    establecida=Sum('frutales__cantidad_establecida'),
                                                                                                                    resiembra=Sum('frutales__cantidad_resiembra'))
            if total['sembrada'] or total['establecida'] or total['resiembra']:
                tabla_exito_frutales[obj] = total

        tabla_exito_maderable = OrderedDict()
        for obj in EspeciesMaderables.objects.all():
            total = filtro.filter(maderables__especies=obj,year = anio[0]).aggregate(sembrada=Sum('maderables__cantidad_sembrada'),
                                                                                                                    establecida=Sum('maderables__cantidad_establecida'),
                                                                                                                    resiembra=Sum('maderables__cantidad_resiembra'))
            if total['sembrada'] or total['establecida'] or total['resiembra']:
                tabla_exito_maderable[obj] = total

        tabla_exito_arboles = OrderedDict()
        for obj in EspeciesArbolesServicios.objects.all():
            total = filtro.filter(sombratemporal__clones_variedad=obj,year = anio[0]).aggregate(sembrada=Sum('sombratemporal__cantidad_sembrada'),
                                                                                                                    establecida=Sum('sombratemporal__cantidad_establecida'),
                                                                                                                    resiembra=Sum('sombratemporal__cantidad_resiembra'))
            if total['sembrada'] or total['establecida'] or total['resiembra']:
                tabla_exito_arboles[obj] = total

        years[anio[1]] = [numero_parcelas,tabla_exito_cacao,tabla_exito_musaceas,tabla_exito_frutales,
                            tabla_exito_maderable,tabla_exito_arboles]

    return render(request, template, locals())

def establecimiento_crecimiento(request, template='cuadernotres/establecimiento_crecimiento.html'):
    filtro = _queryset_filtrado_cuaderno3(request)
    anios = filtro.values_list('year','year__nombre').distinct('year')
    years = OrderedDict()

    for anio in anios:
        numero_parcelas = filtro.filter(year = anio[0]).count()
        #Crecimiento inicial
        tabla_crecimiento_cacao = OrderedDict()
        for obj in ClonesVariedades.objects.all():
            total = filtro.filter(cacao__clones_variedad=obj,year = anio[0]).values_list('cacao__altura_plantas', flat=True)
            bueno = filtro.filter(cacao__clones_variedad=obj, cacao__estado_planta=1,year = anio[0]).count()
            regular = filtro.filter(cacao__clones_variedad=obj, cacao__estado_planta=2,year = anio[0]).count()
            malo = filtro.filter(cacao__clones_variedad=obj, cacao__estado_planta=3,year = anio[0]).count()
            if np.mean(total) >= 0.1:
                tabla_crecimiento_cacao[obj] = (np.mean(total), np.median(total), bueno, regular, malo)

        tabla_crecimiento_musaceas = OrderedDict()
        for obj in TiposMusaceas.objects.all():
            total = filtro.filter(musaceas__tipo_musaceas=obj,year = anio[0]).values_list('musaceas__altura_plantas', flat=True)
            bueno = filtro.filter(musaceas__tipo_musaceas=obj, musaceas__estado_planta=1,year = anio[0]).count()
            regular = filtro.filter(musaceas__tipo_musaceas=obj, musaceas__estado_planta=2,year = anio[0]).count()
            malo = filtro.filter(musaceas__tipo_musaceas=obj, musaceas__estado_planta=3,year = anio[0]).count()
            if np.mean(total) >= 0.1:
                tabla_crecimiento_musaceas[obj] =(np.mean(total), np.median(total), bueno, regular, malo)

        tabla_crecimiento_frutales = OrderedDict()
        for obj in EspeciesFrutales.objects.all():
            total = filtro.filter(frutales__especies=obj,year = anio[0]).values_list('frutales__altura_plantas', flat=True)
            bueno = filtro.filter(frutales__especies=obj, frutales__estado_planta=1,year = anio[0]).count()
            regular = filtro.filter(frutales__especies=obj, frutales__estado_planta=2,year = anio[0]).count()
            malo = filtro.filter(frutales__especies=obj, frutales__estado_planta=3,year = anio[0]).count()
            if np.mean(total) >= 0.1:
                tabla_crecimiento_frutales[obj] = (np.mean(total), np.median(total), bueno, regular, malo)

        tabla_crecimiento_maderable = OrderedDict()
        for obj in EspeciesMaderables.objects.all():
            total = filtro.filter(maderables__especies=obj,year = anio[0]).values_list('maderables__altura_plantas', flat=True)
            bueno = filtro.filter(maderables__especies=obj, maderables__estado_planta=1,year = anio[0]).count()
            regular = filtro.filter(maderables__especies=obj, maderables__estado_planta=2,year = anio[0]).count()
            malo = filtro.filter(maderables__especies=obj, maderables__estado_planta=3,year = anio[0]).count()
            if np.mean(total) >= 0.1:
                tabla_crecimiento_maderable[obj] = (np.mean(total), np.median(total), bueno, regular, malo)

        tabla_crecimiento_arboles = OrderedDict()
        for obj in EspeciesArbolesServicios.objects.all():
            total = filtro.filter(sombratemporal__clones_variedad=obj,year = anio[0]).values_list('sombratemporal__altura_plantas', flat=True)
            bueno = filtro.filter(sombratemporal__clones_variedad=obj, sombratemporal__estado_planta=1,year = anio[0]).count()
            regular = filtro.filter(sombratemporal__clones_variedad=obj, sombratemporal__estado_planta=2,year = anio[0]).count()
            malo = filtro.filter(sombratemporal__clones_variedad=obj, sombratemporal__estado_planta=3,year = anio[0]).count()
            if np.mean(total) >= 0.1:
                tabla_crecimiento_arboles[obj] = (np.mean(total), np.median(total), bueno, regular, malo)

        years[anio[1]] = [numero_parcelas,tabla_crecimiento_cacao,tabla_crecimiento_musaceas,tabla_crecimiento_frutales,
                            tabla_crecimiento_maderable,tabla_crecimiento_arboles
                            ]

    return render(request, template, locals())

def costo_cacao(request, template="cuadernotres/costo_cacao.html"):
    filtro0 = _queryset_filtrado_cuaderno0(request)

    filtro1 = _queryset_filtrado_cuaderno3(request)

    filtro2 = _queryset_filtrado_cuaderno6(request)

    filtro3 = _queryset_filtrado_cuaderno9(request)

    list_anios = []
    for x in filtro0.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro1.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro2.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro3.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))

    anios = list(sorted(set(list_anios)))

    years = OrderedDict()

    for anio in anios:
        numero_parcelas1 = filtro1.filter(year = anio[0]).count()
        numero_parcelas2 = filtro2.filter(year = anio[0]).count()
        numero_parcelas3 = filtro3.filter(year = anio[0]).count()

        numero_parcelas = numero_parcelas1 + numero_parcelas2 + numero_parcelas3 / 3

        area_total = filtro0.filter(year = anio[0]).aggregate(t=Sum('area_ha'))['t']
        lista_promedio1=filtro1.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)
        lista_promedio2=filtro2.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)
        lista_promedio3=filtro3.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)

        lista_total_promedio = list(chain(lista_promedio1, lista_promedio2, lista_promedio3))

        promedio_mo_dia = np.mean(lista_total_promedio)

        tabla_costo_cacao = OrderedDict()
        for obj in CHOICE_COSTO_CACAO_ACTIVIDAD:
            tmo_familia1 = filtro1.filter(costocacao__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costocacao__mo_familiar'), V(0)))['t']
            tmo_contratada1 = filtro1.filter(costocacao__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costocacao__mo_contratada'), V(0)))['t']
            insumos1 = filtro1.filter(costocacao__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costocacao__costo_insumo_usd'), V(0)))['t']

            tmo_familia2 = filtro2.filter(costocacao__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costocacao__mo_familiar'), V(0)))['t']
            tmo_contratada2 = filtro2.filter(costocacao__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costocacao__mo_contratada'),V(0)))['t']
            insumos2 = filtro2.filter(costocacao__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costocacao__costo_insumo_usd'), V(0)))['t']

            tmo_familia3 = filtro3.filter(costocacao__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costocacao__mo_familiar'), V(0)))['t']
            tmo_contratada3 = filtro3.filter(costocacao__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costocacao__mo_contratada'), 0))['t']
            insumos3 = filtro3.filter(costocacao__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costocacao__costo_insumo_usd'), V(0)))['t']


            total_mo_familiar = tmo_familia1 + tmo_familia2 + tmo_familia3
            total_mo_contratada = tmo_contratada1 + tmo_contratada2 + tmo_contratada3
            total_insumos = insumos1 + insumos2 + insumos3

            tabla_costo_cacao[obj[1]] = (total_mo_familiar, total_mo_contratada, total_insumos)


        #totales de costo
        total_costo_mo_familiar = 0
        total_costo_mo_contratada = 0
        total_costo_insumos = 0
        for k,v in tabla_costo_cacao.items():
            total_costo_mo_familiar += v[0]
            total_costo_mo_contratada += v[1]
            total_costo_insumos +=v[2]

        #Cosecha consolidada Cacao
        CHOICE_PRODUCTOS_CACAO = (
                            (1, 'Cacao en baba'),
                            (2, 'Cacao en grano seco'),
        )
        tabla_cosecha_cacao = OrderedDict()
        for obj in CHOICE_PRODUCTOS_CACAO:
            cantidad1 = filtro1.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad'), V(0)))['t']
            precio1 = filtro1.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd'), V(0)))['t']
            #total_cosecha1 = cantidad1 * precio1

            cantidad2 = filtro2.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad'), V(0)))['t']
            precio2 = filtro2.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd'), V(0)))['t']
            #total_cosecha2 = cantidad2 * precio2

            cantidad3 = filtro3.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad'), V(0)))['t']
            precio3 = filtro3.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd'), V(0)))['t']
            #total_cosecha3 = cantidad3 * precio3

            total_cantidad = cantidad1 + cantidad2 + cantidad3
            total_precio = precio1 + precio2 + precio3
            total_cosecha = total_cantidad * total_precio

            tabla_cosecha_cacao[obj[1]] = (total_cantidad, total_precio, total_cosecha)

        suma_baba = 0
        suma_seco = 0
        suma_coseha = 0
        for k,v in tabla_cosecha_cacao.items():
            if k == 'Cacao en baba':
                suma_baba += v[0] / 3.0
            if k == 'Cacao en grano seco':
                suma_seco += v[0]
            suma_coseha += v[2]

        #Terce cuadro
        try:
            valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
        except:
            valor_mo_familiar = 0
        try:
            valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
        except:
            valor_mo_contratada = 0
        try:
            valor_insumos = total_costo_insumos
        except:
            valor_insumos = 0
        try:
            costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_insumos
        except:
            costo_total_produccion = 0
        try:
            prod_cacao_seco = suma_seco
        except:
            prod_cacao_seco = 0
        try:
            productividad_cacao = prod_cacao_seco / float(area_total)
        except:
            productividad_cacao = 0
        try:
            costo_prod_unitario = costo_total_produccion / float(prod_cacao_seco)
        except:
            costo_prod_unitario = 0
        try:
            ingreso_neto = suma_coseha - costo_total_produccion
        except:
            ingreso_neto = 0
        try:
            ingreso_neto_ha = ingreso_neto / area_total
        except:
            ingreso_neto_ha = 0
        try:
            tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
        except:
            tasa_retorno = 0
        try:
            ingreso_neto_parcial = suma_coseha - valor_mo_contratada - total_costo_insumos
        except:
            ingreso_neto_parcial = 0
        try:
            valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
        except:
            valor_agregado_mo_familiar = 0

        years[anio[1]] = [numero_parcelas,area_total,promedio_mo_dia,tabla_costo_cacao,total_costo_mo_familiar,
                            total_costo_mo_contratada,total_costo_insumos,tabla_cosecha_cacao,suma_baba,
                            suma_seco,suma_coseha,valor_mo_familiar,valor_mo_contratada,costo_total_produccion,
                            prod_cacao_seco,productividad_cacao,costo_prod_unitario,ingreso_neto,ingreso_neto_ha,
                            tasa_retorno,ingreso_neto_parcial,valor_agregado_mo_familiar
                            ]

    return render(request, template, locals())

def costo_musaceas(request, template="cuadernotres/costo_musaceas.html"):
    filtro0 = _queryset_filtrado_cuaderno0(request)

    filtro1 = _queryset_filtrado_cuaderno3(request)

    filtro2 = _queryset_filtrado_cuaderno6(request)

    filtro3 = _queryset_filtrado_cuaderno9(request)

    list_anios = []
    for x in filtro0.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro1.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro2.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro3.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))

    anios = list(sorted(set(list_anios)))

    years = OrderedDict()

    for anio in anios:
        numero_parcelas1 = filtro1.filter(year = anio[0]).count()
        numero_parcelas2 = filtro2.filter(year = anio[0]).count()
        numero_parcelas3 = filtro3.filter(year = anio[0]).count()

        numero_parcelas = numero_parcelas1 + numero_parcelas2 + numero_parcelas3 / 3

        area_total = filtro0.filter(year = anio[0]).aggregate(t=Sum('area_ha'))['t']
        lista_promedio1=filtro1.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)
        lista_promedio2=filtro2.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)
        lista_promedio3=filtro3.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)

        lista_total_promedio = list(chain(lista_promedio1, lista_promedio2, lista_promedio3))

        promedio_mo_dia = np.mean(lista_total_promedio)

        tabla_costo_musaceas = OrderedDict()
        for obj in CHOICE_COSTO_MUSACEAS_ACTIVIDAD:
            tmo_familia1 = filtro1.filter(costomusaceas__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomusaceas__mo_familiar'), V(0)))['t']
            tmo_contratada1 = filtro1.filter(costomusaceas__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomusaceas__mo_contratada'), V(0)))['t']
            insumos1 = filtro1.filter(costomusaceas__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomusaceas__costo_insumo_usd'), V(0)))['t']

            tmo_familia2 = filtro2.filter(costomusaceas__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomusaceas__mo_familiar'), V(0)))['t']
            tmo_contratada2 = filtro2.filter(costomusaceas__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomusaceas__mo_contratada'),V(0)))['t']
            insumos2 = filtro2.filter(costomusaceas__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomusaceas__costo_insumo_usd'), V(0)))['t']

            tmo_familia3 = filtro3.filter(costomusaceas__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomusaceas__mo_familiar'), V(0)))['t']
            tmo_contratada3 = filtro3.filter(costomusaceas__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomusaceas__mo_contratada'), 0))['t']
            insumos3 = filtro3.filter(costomusaceas__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomusaceas__costo_insumo_usd'), V(0)))['t']


            total_mo_familiar = tmo_familia1 + tmo_familia2 + tmo_familia3
            total_mo_contratada = tmo_contratada1 + tmo_contratada2 + tmo_contratada3
            total_insumos = insumos1 + insumos2 + insumos3

            tabla_costo_musaceas[obj[1]] = (total_mo_familiar, total_mo_contratada, total_insumos)


        #totales de costo
        total_costo_mo_familiar = 0
        total_costo_mo_contratada = 0
        total_costo_insumos = 0
        for k,v in tabla_costo_musaceas.items():
            total_costo_mo_familiar += v[0]
            total_costo_mo_contratada += v[1]
            total_costo_insumos +=v[2]

        #Cosecha consolidada Cacao
        CHOICE_PRODUCTOS_MUSACEAS = (
                            (4, 'Musaceas-Banano'),
                            (5, 'Musaceas-Plátano'),
        )
        tabla_cosecha_musaceas = OrderedDict()
        for obj in CHOICE_PRODUCTOS_MUSACEAS:
            cantidad1_kg = filtro1.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad_g'), V(0)))['t']
            precio1_kg = filtro1.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd_g'), V(0)))['t']

            cantidad2_kg = filtro2.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad_g'), V(0)))['t']
            precio2_kg = filtro2.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd_g'), V(0)))['t']

            cantidad3_kg = filtro3.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad_g'), V(0)))['t']
            precio3_kg = filtro3.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd_g'), V(0)))['t']

            total_cantidad = cantidad1_kg + cantidad2_kg + cantidad3_kg
            total_precio = precio1_kg + precio2_kg + precio3_kg
            total_cosecha = total_cantidad * total_precio

            tabla_cosecha_musaceas[obj[1]] = (total_cantidad, total_precio, total_cosecha)

        suma_banano = 0
        suma_platano = 0
        suma_coseha = 0
        for k,v in tabla_cosecha_musaceas.items():
            if k == 'Musaceas-Banano':
                suma_banano += v[0]
            if k == 'Musaceas-Plátano':
                suma_platano += v[0]
            suma_coseha += v[2]

        #tercer tabla
        try:
            valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
        except:
            valor_mo_familiar = 0
        try:
            valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
        except:
            valor_mo_contratada = 0
        try:
            valor_insumos = total_costo_insumos
        except:
            valor_insumos = 0
        try:
            costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_insumos
        except:
            costo_total_produccion = 0
        try:
            prod_musaceas = suma_banano + suma_platano
        except:
            prod_musaceas = 0
        try:
            productividad_musaceas = prod_musaceas / float(area_total)
        except:
            productividad_musaceas = 0
        try:
            costo_prod_unitario = costo_total_produccion / float(prod_musaceas)
        except:
            costo_prod_unitario = 0
        try:
            ingreso_neto = suma_coseha - costo_total_produccion
        except:
            ingreso_neto = 0
        try:
            ingreso_neto_ha = ingreso_neto / area_total
        except:
            ingreso_neto_ha = 0
        try:
            tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
        except:
            tasa_retorno = 0
        try:
            ingreso_neto_parcial = suma_coseha - valor_mo_contratada - total_costo_insumos
        except:
            ingreso_neto_parcial = 0
        try:
            valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
        except:
            valor_agregado_mo_familiar = 0

        years[anio[1]] = [numero_parcelas,area_total,promedio_mo_dia,tabla_costo_musaceas,total_costo_mo_familiar,
                            total_costo_mo_contratada,total_costo_insumos,tabla_cosecha_musaceas,suma_banano,suma_platano,
                            suma_coseha,valor_mo_familiar,valor_mo_contratada,valor_insumos,costo_total_produccion,prod_musaceas,
                            productividad_musaceas,costo_prod_unitario,ingreso_neto,ingreso_neto_ha,tasa_retorno,
                            ingreso_neto_parcial,valor_agregado_mo_familiar
                            ]

    return render(request, template, locals())

def costo_frutales(request, template="cuadernotres/costo_frutales.html"):
    filtro0 = _queryset_filtrado_cuaderno0(request)

    filtro1 = _queryset_filtrado_cuaderno3(request)

    filtro2 = _queryset_filtrado_cuaderno6(request)

    filtro3 = _queryset_filtrado_cuaderno9(request)

    list_anios = []
    for x in filtro0.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro1.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro2.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro3.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))

    anios = list(sorted(set(list_anios)))

    years = OrderedDict()

    for anio in anios:
        numero_parcelas1 = filtro1.filter(year = anio[0]).count()
        numero_parcelas2 = filtro2.filter(year = anio[0]).count()
        numero_parcelas3 = filtro3.filter(year = anio[0]).count()

        numero_parcelas = numero_parcelas1 + numero_parcelas2 + numero_parcelas3 / 3

        area_total = filtro0.filter(year = anio[0]).aggregate(t=Sum('area_ha'))['t']
        lista_promedio1=filtro1.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)
        lista_promedio2=filtro2.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)
        lista_promedio3=filtro3.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)

        lista_total_promedio = list(chain(lista_promedio1, lista_promedio2, lista_promedio3))

        promedio_mo_dia = np.mean(lista_total_promedio)

        tabla_costo_frutales = OrderedDict()
        for obj in CHOICE_COSTO_FRUTALES_ACTIVIDAD:
            tmo_familia1 = filtro1.filter(costofrutales__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costofrutales__mo_familiar'), V(0)))['t']
            tmo_contratada1 = filtro1.filter(costofrutales__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costofrutales__mo_contratada'), V(0)))['t']
            insumos1 = filtro1.filter(costofrutales__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costofrutales__costo_insumo_usd'), V(0)))['t']

            tmo_familia2 = filtro2.filter(costofrutales__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costofrutales__mo_familiar'), V(0)))['t']
            tmo_contratada2 = filtro2.filter(costofrutales__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costofrutales__mo_contratada'),V(0)))['t']
            insumos2 = filtro2.filter(costofrutales__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costofrutales__costo_insumo_usd'), V(0)))['t']

            tmo_familia3 = filtro3.filter(costofrutales__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costofrutales__mo_familiar'), V(0)))['t']
            tmo_contratada3 = filtro3.filter(costofrutales__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costofrutales__mo_contratada'), 0))['t']
            insumos3 = filtro3.filter(costofrutales__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costofrutales__costo_insumo_usd'), V(0)))['t']


            total_mo_familiar = tmo_familia1 + tmo_familia2 + tmo_familia3
            total_mo_contratada = tmo_contratada1 + tmo_contratada2 + tmo_contratada3
            total_insumos = insumos1 + insumos2 + insumos3

            tabla_costo_frutales[obj[1]] = (total_mo_familiar, total_mo_contratada, total_insumos)


        #totales de costo
        total_costo_mo_familiar = 0
        total_costo_mo_contratada = 0
        total_costo_insumos = 0
        for k,v in tabla_costo_frutales.items():
            total_costo_mo_familiar += v[0]
            total_costo_mo_contratada += v[1]
            total_costo_insumos +=v[2]

        #Cosecha consolidada Cacao
        CHOICE_PRODUCTOS_FRUTALES = (
                            (6, 'Frutales-Zapote'),
                            (7, 'Frutales-Cítricos'),
                            (8, 'Frutales-Aguacate'),
                            (9, 'Frutales-Coco'),
        )
        tabla_cosecha_frutales = OrderedDict()
        for obj in CHOICE_PRODUCTOS_FRUTALES:
            cantidad1 = filtro1.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad'), V(0)))['t']
            precio1 = filtro1.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd'), V(0)))['t']

            cantidad2 = filtro2.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad'), V(0)))['t']
            precio2 = filtro2.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd'), V(0)))['t']

            cantidad3 = filtro3.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad'), V(0)))['t']
            precio3 = filtro3.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd'), V(0)))['t']

            total_cantidad = cantidad1 + cantidad2 + cantidad3
            total_precio = precio1 + precio2 + precio3
            total_cosecha = total_cantidad * total_precio

            tabla_cosecha_frutales[obj[1]] = (total_cantidad, total_precio, total_cosecha)

        suma_zapote = 0
        suma_citricos = 0
        suma_aguacate = 0
        suma_coco = 0
        suma_coseha = 0
        for k,v in tabla_cosecha_frutales.items():
            if k == 'Frutales-Zapote':
                suma_zapote += v[0]
            if k == 'Frutales-Cítricos':
                suma_citricos += v[0]
            if k == 'Frutales-Aguacate':
                suma_aguacate += v[0]
            if k == 'Frutales-Coco':
                suma_coco += v[0]
            suma_coseha += v[2]

        total_sumas = suma_zapote + suma_citricos +suma_aguacate + suma_coco

        #tercer tabla
        try:
            valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
        except:
            valor_mo_familiar = 0
        try:
            valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
        except:
            valor_mo_contratada = 0
        try:
            valor_insumos = total_costo_insumos
        except:
            valor_insumos = 0
        try:
            costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_insumos
        except:
            costo_total_produccion = 0
        try:
            prod_frutales = total_sumas
        except:
            prod_frutales = 0
        try:
            productividad_frutales = prod_frutales / float(area_total)
        except:
            productividad_frutales = 0
        try:
            costo_prod_unitario = costo_total_produccion / float(prod_frutales)
        except:
            costo_prod_unitario = 0
        try:
            ingreso_neto = suma_coseha - costo_total_produccion
        except:
            ingreso_neto = 0
        try:
            ingreso_neto_ha = ingreso_neto / area_total
        except:
            ingreso_neto_ha = 0
        try:
            tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
        except:
            tasa_retorno = 0
        try:
            ingreso_neto_parcial = suma_coseha - valor_mo_contratada - total_costo_insumos
        except:
            ingreso_neto_parcial = 0
        try:
            valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
        except:
            valor_agregado_mo_familiar = 0

        years[anio[1]] = [numero_parcelas,area_total,promedio_mo_dia,tabla_costo_frutales,total_costo_mo_familiar,
                            total_costo_mo_contratada,total_costo_insumos,tabla_cosecha_frutales,total_sumas,
                            suma_coseha,valor_mo_familiar,valor_mo_contratada,costo_total_produccion,prod_frutales,
                            productividad_frutales,costo_prod_unitario,ingreso_neto,ingreso_neto_ha,tasa_retorno,
                            ingreso_neto_parcial,valor_agregado_mo_familiar
                            ]

    return render(request, template, locals())

def costo_maderables(request, template="cuadernotres/costo_maderables.html"):
    filtro0 = _queryset_filtrado_cuaderno0(request)

    filtro1 = _queryset_filtrado_cuaderno3(request)

    filtro2 = _queryset_filtrado_cuaderno6(request)

    filtro3 = _queryset_filtrado_cuaderno9(request)

    list_anios = []
    for x in filtro0.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro1.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro2.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro3.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))

    anios = list(sorted(set(list_anios)))

    years = OrderedDict()

    for anio in anios:
        numero_parcelas1 = filtro1.filter(year = anio[0]).count()
        numero_parcelas2 = filtro2.filter(year = anio[0]).count()
        numero_parcelas3 = filtro3.filter(year = anio[0]).count()

        numero_parcelas = numero_parcelas1 + numero_parcelas2 + numero_parcelas3 / 3

        area_total = filtro0.filter(year = anio[0]).aggregate(t=Sum('area_ha'))['t']
        lista_promedio1=filtro1.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)
        lista_promedio2=filtro2.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)
        lista_promedio3=filtro3.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)

        lista_total_promedio = list(chain(lista_promedio1, lista_promedio2, lista_promedio3))

        promedio_mo_dia = np.mean(lista_total_promedio)

        tabla_costo_maderables = OrderedDict()
        for obj in CHOICE_COSTO_MADERABLES_ACTIVIDAD:
            tmo_familia1 = filtro1.filter(costomaderables__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomaderables__mo_familiar'), V(0)))['t']
            tmo_contratada1 = filtro1.filter(costomaderables__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomaderables__mo_contratada'), V(0)))['t']
            insumos1 = filtro1.filter(costomaderables__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomaderables__costo_insumo_usd'), V(0)))['t']

            tmo_familia2 = filtro2.filter(costomaderables__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomaderables__mo_familiar'), V(0)))['t']
            tmo_contratada2 = filtro2.filter(costomaderables__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomaderables__mo_contratada'),V(0)))['t']
            insumos2 = filtro2.filter(costomaderables__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomaderables__costo_insumo_usd'), V(0)))['t']

            tmo_familia3 = filtro3.filter(costomaderables__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomaderables__mo_familiar'), V(0)))['t']
            tmo_contratada3 = filtro3.filter(costomaderables__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomaderables__mo_contratada'), 0))['t']
            insumos3 = filtro3.filter(costomaderables__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costomaderables__costo_insumo_usd'), V(0)))['t']


            total_mo_familiar = tmo_familia1 + tmo_familia2 + tmo_familia3
            total_mo_contratada = tmo_contratada1 + tmo_contratada2 + tmo_contratada3
            total_insumos = insumos1 + insumos2 + insumos3

            tabla_costo_maderables[obj[1]] = (total_mo_familiar, total_mo_contratada, total_insumos)


        #totales de costo
        total_costo_mo_familiar = 0
        total_costo_mo_contratada = 0
        total_costo_insumos = 0
        for k,v in tabla_costo_maderables.items():
            total_costo_mo_familiar += v[0]
            total_costo_mo_contratada += v[1]
            total_costo_insumos +=v[2]

        #Cosecha consolidada Cacao
        CHOICE_PRODUCTOS_MADERABLES = (
                            (10, 'Madera-Caoba'),
                            (11, 'Madera-Cortez'),
                            (12, 'Madera-Roble'),
                            (13, 'Madera-Melina'),
                            (14, 'Madera-Granadillo'),
                            (15, 'Madera-Leña'),
        )
        tabla_cosecha_maderables = OrderedDict()
        for obj in CHOICE_PRODUCTOS_MADERABLES:
            cantidad1 = filtro1.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad_g'), V(0)))['t']
            precio1 = filtro1.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd_g'), V(0)))['t']

            cantidad2 = filtro2.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad_g'), V(0)))['t']
            precio2 = filtro2.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd_g'), V(0)))['t']

            cantidad3 = filtro3.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad_g'), V(0)))['t']
            precio3 = filtro3.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd_g'), V(0)))['t']

            total_cantidad = cantidad1 + cantidad2 + cantidad3
            total_precio = precio1 + precio2 + precio3
            total_cosecha = total_cantidad * total_precio

            tabla_cosecha_maderables[obj[1]] = (total_cantidad, total_precio, total_cosecha)

        suma_caoba = 0
        suma_cortez = 0
        suma_roble = 0
        suma_melina = 0
        suma_granadillo = 0
        suma_lena = 0
        suma_coseha = 0
        for k,v in tabla_cosecha_maderables.items():
            if k == 'Madera-Caoba':
                suma_caoba += v[0]
            if k == 'Madera-Cortez':
                suma_cortez += v[0]
            if k == 'Madera-Roble':
                suma_roble += v[0]
            if k == 'Madera-Melina':
                suma_melina += v[0]
            if k == 'Madera-Granadillo':
                suma_granadillo += v[0]
            if k == 'Madera-Leña':
                suma_lena += v[0]
            suma_coseha += v[2]

        total_sumas = suma_caoba + suma_cortez +suma_roble + suma_melina + suma_granadillo +suma_lena

        #tercer tabla
        try:
            valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
        except:
            valor_mo_familiar = 0
        try:
            valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
        except:
            valor_mo_contratada = 0
        try:
            valor_insumos = total_costo_insumos
        except:
            valor_insumos = 0
        try:
            costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_insumos
        except:
            costo_total_produccion = 0
        try:
            prod_maderables = total_sumas
        except:
            prod_maderables = 0
        try:
            productividad_maderables = prod_maderables / float(area_total)
        except:
            productividad_maderables = 0
        try:
            costo_prod_unitario = costo_total_produccion / float(prod_maderables)
        except:
            costo_prod_unitario = 0
        try:
            ingreso_neto = suma_coseha - costo_total_produccion
        except:
            ingreso_neto = 0
        try:
            ingreso_neto_ha = ingreso_neto / area_total
        except:
            ingreso_neto_ha = 0
        try:
            tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
        except:
            tasa_retorno = 0
        try:
            ingreso_neto_parcial = suma_coseha - valor_mo_contratada - total_costo_insumos
        except:
            ingreso_neto_parcial = 0
        try:
            valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
        except:
            valor_agregado_mo_familiar = 0

        years[anio[1]] = [numero_parcelas,area_total,promedio_mo_dia,tabla_costo_maderables,total_costo_mo_familiar,
                            total_costo_mo_contratada,total_costo_insumos,tabla_cosecha_maderables,total_sumas,
                            suma_coseha,valor_mo_familiar,valor_mo_contratada,costo_total_produccion,prod_maderables,
                            productividad_maderables,costo_prod_unitario,ingreso_neto,ingreso_neto_ha,tasa_retorno,
                            ingreso_neto_parcial,valor_agregado_mo_familiar
                            ]

    return render(request, template, locals())

def costo_arboles(request, template="cuadernotres/costo_arboles.html"):
    filtro0 = _queryset_filtrado_cuaderno0(request)

    filtro1 = _queryset_filtrado_cuaderno3(request)

    filtro2 = _queryset_filtrado_cuaderno6(request)

    filtro3 = _queryset_filtrado_cuaderno9(request)

    list_anios = []
    for x in filtro0.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro1.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro2.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))
    for x in filtro3.values_list('year','year__nombre').distinct('year'):
        list_anios.append((x[0],x[1]))

    anios = list(sorted(set(list_anios)))

    years = OrderedDict()

    for anio in anios:
        numero_parcelas1 = filtro1.filter(year = anio[0]).count()
        numero_parcelas2 = filtro2.filter(year = anio[0]).count()
        numero_parcelas3 = filtro3.filter(year = anio[0]).count()

        numero_parcelas = numero_parcelas1 + numero_parcelas2 + numero_parcelas3 / 3

        area_total = filtro0.filter(year = anio[0]).aggregate(t=Sum('area_ha'))['t']
        lista_promedio1=filtro1.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)
        lista_promedio2=filtro2.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)
        lista_promedio3=filtro3.filter(costomanoobradia__valor_mo_dia_usd__gt=0,year = anio[0]).values_list('costomanoobradia__valor_mo_dia_usd', flat=True)

        lista_total_promedio = list(chain(lista_promedio1, lista_promedio2, lista_promedio3))

        promedio_mo_dia = np.mean(lista_total_promedio)

        tabla_costo_arboles = OrderedDict()
        for obj in CHOICE_COSTO_MADERABLES_ACTIVIDAD:
            tmo_familia1 = filtro1.filter(costosombratemporal__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costosombratemporal__mo_familiar'), V(0)))['t']
            tmo_contratada1 = filtro1.filter(costosombratemporal__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costosombratemporal__mo_contratada'), V(0)))['t']
            insumos1 = filtro1.filter(costosombratemporal__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costosombratemporal__costo_insumo_usd'), V(0)))['t']

            tmo_familia2 = filtro2.filter(costosombratemporal__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costosombratemporal__mo_familiar'), V(0)))['t']
            tmo_contratada2 = filtro2.filter(costosombratemporal__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costosombratemporal__mo_contratada'),V(0)))['t']
            insumos2 = filtro2.filter(costosombratemporal__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costosombratemporal__costo_insumo_usd'), V(0)))['t']

            tmo_familia3 = filtro3.filter(costosombratemporal__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costosombratemporal__mo_familiar'), V(0)))['t']
            tmo_contratada3 = filtro3.filter(costosombratemporal__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costosombratemporal__mo_contratada'), 0))['t']
            insumos3 = filtro3.filter(costosombratemporal__actividades=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('costosombratemporal__costo_insumo_usd'), V(0)))['t']


            total_mo_familiar = tmo_familia1 + tmo_familia2 + tmo_familia3
            total_mo_contratada = tmo_contratada1 + tmo_contratada2 + tmo_contratada3
            total_insumos = insumos1 + insumos2 + insumos3

            tabla_costo_arboles[obj[1]] = (total_mo_familiar, total_mo_contratada, total_insumos)


        #totales de costo
        total_costo_mo_familiar = 0
        total_costo_mo_contratada = 0
        total_costo_insumos = 0
        for k,v in tabla_costo_arboles.items():
            total_costo_mo_familiar += v[0]
            total_costo_mo_contratada += v[1]
            total_costo_insumos +=v[2]

        #Cosecha consolidada Cacao
        CHOICE_PRODUCTOS_ARBOLES = (
                            (18, 'Gandul'),
                            (19, 'Guaba'),
        )
        tabla_cosecha_arboles = OrderedDict()
        for obj in CHOICE_PRODUCTOS_ARBOLES:
            cantidad1 = filtro1.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad'), V(0)))['t']
            precio1 = filtro1.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd'), V(0)))['t']

            cantidad2 = filtro2.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad'), V(0)))['t']
            precio2 = filtro2.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd'), V(0)))['t']

            cantidad3 = filtro3.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Sum('datoscosecha__cantidad'), V(0)))['t']
            precio3 = filtro3.filter(datoscosecha__productos=obj[0],year = anio[0]).aggregate(t=Coalesce(Avg('datoscosecha__precio_usd'), V(0)))['t']

            total_cantidad = cantidad1 + cantidad2 + cantidad3
            total_precio = precio1 + precio2 + precio3
            total_cosecha = total_cantidad * total_precio

            tabla_cosecha_arboles[obj[1]] = (total_cantidad, total_precio, total_cosecha)

        suma_gandul = 0
        suma_guaba = 0
        suma_coseha = 0
        for k,v in tabla_cosecha_arboles.items():
            if k == 'Gandul':
                suma_gandul += v[0]
            if k == 'Guaba':
                suma_guaba += v[0]
            suma_coseha += v[2]

        total_sumas = suma_gandul + suma_guaba

        #tercer tabla
        try:
            valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
        except:
            valor_mo_familiar = 0
        try:
            valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
        except:
            valor_mo_contratada = 0
        try:
            valor_insumos = total_costo_insumos
        except:
            valor_insumos = 0
        try:
            costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_insumos
        except:
            costo_total_produccion = 0
        try:
            prod_arboles = total_sumas
        except:
            prod_arboles = 0
        try:
            productividad_arboles = prod_arboles / float(area_total)
        except:
            productividad_arboles = 0
        try:
            costo_prod_unitario = costo_total_produccion / float(prod_arboles)
        except:
            costo_prod_unitario = 0
        try:
            ingreso_neto = suma_coseha - costo_total_produccion
        except:
            ingreso_neto = 0
        try:
            ingreso_neto_ha = ingreso_neto / area_total
        except:
            ingreso_neto_ha = 0
        try:
            tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
        except:
            tasa_retorno = 0
        try:
            ingreso_neto_parcial = suma_coseha - valor_mo_contratada - total_costo_insumos
        except:
            ingreso_neto_parcial = 0
        try:
            valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
        except:
            valor_agregado_mo_familiar = 0

        years[anio[1]] = [numero_parcelas,area_total,promedio_mo_dia,tabla_costo_arboles,total_costo_mo_familiar,
                            total_costo_mo_contratada,total_costo_insumos,tabla_cosecha_arboles,total_sumas,
                            suma_coseha,valor_mo_familiar,valor_mo_contratada,costo_total_produccion,prod_arboles,
                            productividad_arboles,costo_prod_unitario,ingreso_neto,ingreso_neto_ha,tasa_retorno,
                            ingreso_neto_parcial,valor_agregado_mo_familiar
                            ]

    return render(request, template, locals())

def crear_rangos(request, lista, start=0, stop=0, step=0):
    dict_algo = OrderedDict()
    rangos = []
    contador = 0
    rangos = [(n, n+int(step)-1) for n in range(int(start), int(stop), int(step))]

    for desde, hasta in rangos:
        dict_algo['%s a %s' % (desde,hasta)] = len([x for x in lista if desde <= x <= hasta])

    return dict_algo
