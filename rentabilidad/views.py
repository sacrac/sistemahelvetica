# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.db.models import Avg, Sum, Count,  Value as V, F
from django.db.models.functions import Coalesce

from .models import *
from .forms import ConsultaFichaForm
from .choices_static import *
from configuracion.models import CHOICES_ANIO, DatosCosto
from cuadernosmes0.models import (ClonesVariedades, TiposMusaceas,
                                                                    EspeciesFrutales, EspeciesMaderables,
                                                                    EspeciesArbolesServicios)
from dal import autocomplete
import json as simplejson
from collections import OrderedDict
import numpy as np

# Create your views here.

def _queryset_filtrado(request):
    params = {}

    if 'fecha' in request.session:
       params['year__in'] = request.session['fecha']

    if 'pais' in request.session:
       params['entrevistado__pais'] = request.session['pais']

    if 'modelo' in request.session:
       params['modelo'] = request.session['modelo']

    if 'parcela' in request.session:
        params['preguntas1__tamanio'] = request.session['parcela']

    if 'riego' in request.session:
        params['preguntas2__riego'] = request.session['riego']

    if 'saf' in request.session:
        params['preguntas2__tipo_sistema'] = request.session['saf']

    if 'valoracion' in request.session:
        params['preguntas1__valoracion'] = request.session['valoracion']

    if 'manejo' in request.session:
        params['preguntas1__manejo'] = request.session['manejo']

    if 'intensidad' in request.session:
        params['preguntas1__intensidad'] = request.session['intensidad']

    if 'sexo' in request.session:
        params['entrevistado__sexo'] = request.session['sexo']

    if 'productor' in request.session:
        params['entrevistado__nombre'] = request.session['productor']

    if 'organizacion_apoyo' in request.session:
        params['entrevistado__organizacion_apoyo'] = request.session['organizacion_apoyo']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    return EncuestaRentabilidad.objects.filter(**params).select_related()

def consulta_ficha(request, template='rentabilidad/consultarRentabilidad.html'):
    if request.method == 'POST':
        form = ConsultaFichaForm(request.POST)
        if form.is_valid():
            request.session['fecha'] = form.cleaned_data['fecha']
            request.session['pais'] = form.cleaned_data['pais']
            request.session['modelo'] = form.cleaned_data['modelo']
            request.session['parcela'] = form.cleaned_data['parcela']
            request.session['riego'] = form.cleaned_data['riego']
            request.session['saf'] = form.cleaned_data['saf']
            request.session['valoracion'] = form.cleaned_data['valoracion']
            request.session['manejo'] = form.cleaned_data['manejo']
            request.session['intensidad'] = form.cleaned_data['intensidad']
            request.session['sexo'] = form.cleaned_data['sexo']
            request.session['productor'] = form.cleaned_data['productor']
            request.session['organizacion_apoyo'] = form.cleaned_data['organizacion_apoyo']
            centinela = 1
        else:
            centinela = 0
    else:
        form = ConsultaFichaForm()
        centinela = 0

        if 'fecha' in request.session:
            try:
                del request.session['fecha']
                del request.session['pais']
                del request.session['modelo']
                del request.session['parcela']
                del request.session['riego']
                del request.session['saf']
                del request.session['valoracion']
                del request.session['manejo']
                del request.session['intensidad']
                del request.session['sexo']
                del request.session['productor']
                del request.session['organizacion_apoyo']
            except:
                pass

    return render(request, template, {'form': form, 'centinela': centinela})

def mapa_rentabilidad(request, template='rentabilidad/mapa_renta.html'):
    filtro = _queryset_filtrado(request)#.select_related('preguntas1','preguntas2','preguntas3')
    
    ubicacion = filtro.values_list('entrevistado__nombre',
                                   'entrevistado__latitud',
                                   'entrevistado__longitud')

    return render(request, template, {'mapa': ubicacion,
                                    'num_familias': filtro.count()})

def descripcion_muestra(request, template='rentabilidad/muestra.html'):
    filtro = _queryset_filtrado(request)#.select_related('preguntas1','preguntas2','preguntas3')

    ubicacion = []
    hombres_totales = 0
    mujeres_totales = 0
    suma_totales = 0
    p = Pais.objects.select_related()
    d = Departamento.objects.select_related()
    m = Municipio.objects.select_related()

    for obj in p.filter(id=request.session['modelo'].pais_id):
        for depart in d.filter(pais=obj):
            for muni in m.filter(departamento=depart):
                contar_m = filtro.filter(entrevistado__municipio=muni, entrevistado__sexo=1).count()
                contar_h = filtro.filter(entrevistado__municipio=muni, entrevistado__sexo=2).count()
                suma = contar_m + contar_h
                mujeres_totales += contar_m
                hombres_totales += contar_h
                suma_totales +=suma
                ubicacion.append([obj,depart,muni, contar_h, contar_m, suma])

    metrics = {
            'total': Count('id'),
        }
    parcela_lista = list(
            Preguntas1.objects.filter(encuesta__in=filtro)
            .values('tamanio')
            .annotate(**metrics)
            .order_by('-total')
        )

    riego_lista = list(
            Preguntas2.objects.filter(encuesta__in=filtro)
            .values('riego')
            .annotate(**metrics)
            .order_by('-total')
        )

    valoracion_lista = list(
            Preguntas1.objects.filter(encuesta__in=filtro)
            .values('valoracion')
            .annotate(**metrics)
            .order_by('-total')
        )

    manejo_lista = list(
            Preguntas1.objects.filter(encuesta__in=filtro)
            .values('manejo')
            .annotate(**metrics)
            .order_by('-total')
        )

    intensidad_lista = list(
            Preguntas1.objects.filter(encuesta__in=filtro)
            .values('intensidad')
            .annotate(**metrics)
            .order_by('-total')
        )

    pendiente_lista = list(
            Preguntas3.objects.filter(encuesta__in=filtro)
            .values('pendiente')
            .annotate(**metrics)
            .order_by('-total')
        )

    # parcela = {}
    # for obj in CHOICE_TAMANO_PARCELA:
    #     conteo = filtro.filter(preguntas1__tamanio=obj[0]).count()
    #     #conteo = a[]
    #     parcela[obj[1]] = conteo

    # riego = {}
    # for obj in CHOICE_RIEGO:
    #     conteo = filtro.filter(preguntas2__riego=obj[0]).count()
    #     riego[obj[1]] = conteo

    # valoracion = {}
    # for obj in CHOICE_VALORACION_SUELO:
    #     conteo = filtro.filter(preguntas1__valoracion=obj[0]).count()
    #     valoracion[obj[1]] = conteo

    # manejo = {}
    # for obj in CHOICE_TIPO_MANEJO:
    #     conteo = filtro.filter(preguntas1__manejo=obj[0]).count()
    #     manejo[obj[1]] = conteo

    # intensidad = {}
    # for obj in CHOICE_INTENSIDAD_MANEJO:
    #     conteo = filtro.filter(preguntas1__intensidad=obj[0]).count()
    #     intensidad[obj[1]] = conteo

    # pendiente = {}
    # for obj in CHOICE_PENDIENTE:
    #     conteo = filtro.filter(preguntas3__pendiente=obj[0]).count()
    #     pendiente[obj[1]] = conteo

    tipo_saf_lista = list(
            Preguntas2.objects.filter(encuesta__in=filtro)
            .values('tipo_sistema')
            .annotate(**metrics)
            .order_by('-total')
        )

    # tipo_saf = {}
    # for obj in CHOICE_TIPO_SISTEMA_AGROFORESTAL:
    #     conteo = filtro.filter(preguntas2__tipo_sistema=obj[0]).count()
    #     tipo_saf[obj[1]] = conteo
    descripcion_saf_lista = list(
            Preguntas2.objects.filter(encuesta__in=filtro)
            .values('descripcion')
            .annotate(**metrics)
            .order_by('-total')
        )

    # descripcion_saf = {}
    # for obj in CHOICE_DESCRIPCION_SISTEMA_AGRO:
    #     conteo = filtro.filter(preguntas2__descripcion=obj[0]).count()
    #     if conteo >=1:
    #         descripcion_saf[obj[1]] = conteo

    return render(request, template, {'ubicacion': ubicacion,
                                                                'hombres_totales': hombres_totales,
                                                                'mujeres_totales': mujeres_totales,
                                                                'suma_totales': suma_totales,
                                                                #'parcela': parcela,
                                                                'riego':riego_lista,
                                                                'valoracion': valoracion_lista,
                                                                'manejo': manejo_lista,
                                                                'intensidad': intensidad_lista,
                                                                'pendiente': pendiente_lista,
                                                                'tipo_saf': tipo_saf_lista,
                                                                'descripcion_saf': descripcion_saf_lista,
                                                                'parcela_lista':parcela_lista,
                                                                'num_familias': filtro.count()})

def disenio_parcelas(request, template='rentabilidad/disenio_parcelas.html'):
    filtro = _queryset_filtrado(request)

    numero_total_parcela = filtro.filter(preguntas3__area__gte=0.1).count()

    cacao = {}
    clones_qs = ClonesVariedades.objects.all().select_related()
    for obj in clones_qs:
        datos = []
        cantidad = filtro.filter(inventariocacao__clon=obj).values_list('inventariocacao__cantidad', flat=True)
        numero_famlia_objeto = filtro.filter(inventariocacao__clon=obj).count()

        if len(cantidad) >=1:
            for x in cantidad:
                datos.append(x*1.4)

            promedio = np.mean(datos)
            mediana = np.median(datos)
            try:
                minimo = min(datos)
            except:
                minimo = 0
            try:
                maximo = max(datos)
            except:
                maximo = 0
            cacao[obj] = [minimo, maximo, promedio,mediana,numero_famlia_objeto]

    cafe = {}
    cafe_qs = CafeClonesVariedad.objects.all().select_related()
    for obj in cafe_qs:
        datos = []
        cantidad = filtro.filter(inventariocafe__clon=obj).values_list('inventariocafe__cantidad', flat=True)
        numero_famlia_objeto = filtro.filter(inventariocafe__clon=obj).count()
        if len(cantidad) >=1:
            for x in cantidad:
                datos.append(x*1.4)
            promedio = np.mean(datos)
            mediana = np.median(datos)
            try:
                minimo = min(datos)
            except:
                minimo = 0
            try:
                maximo = max(datos)
            except:
                maximo = 0
            cafe[obj] = [minimo, maximo, promedio,mediana,numero_famlia_objeto]

    musaceas = {}
    musaceas_qs = TiposMusaceas.objects.all().select_related()
    for obj in musaceas_qs:
        datos = []
        cantidad = filtro.filter(inventariomusaceas__clon=obj).values_list('inventariomusaceas__cantidad', flat=True)
        numero_famlia_objeto = filtro.filter(inventariomusaceas__clon=obj).count()
        if len(cantidad) >=1:
            for x in cantidad:
                datos.append(x*1.4)
            promedio = np.mean(datos)
            mediana = np.median(datos)
            try:
                minimo = min(datos)
            except:
                minimo = 0
            try:
                maximo = max(datos)
            except:
                maximo = 0
            musaceas[obj] = [minimo, maximo, promedio,mediana,numero_famlia_objeto]

    frutales = {}
    frutales_qs = EspeciesFrutales.objects.all().select_related()
    for obj in frutales_qs:
        datos = []
        cantidad = filtro.filter(inventariofrutales__clon=obj).values_list('inventariofrutales__cantidad', flat=True)
        numero_famlia_objeto = filtro.filter(inventariofrutales__clon=obj).count()
        if len(cantidad) >=1:
            for x in cantidad:
                datos.append(x*1.4)
            promedio = np.mean(datos)
            mediana = np.median(datos)
            try:
                minimo = min(datos)
            except:
                minimo = 0
            try:
                maximo = max(datos)
            except:
                maximo = 0

            frutales[obj] = [minimo, maximo, promedio,mediana,numero_famlia_objeto]

    maderables = {}
    madera_qs = EspeciesMaderables.objects.all().select_related()
    for obj in madera_qs:
        datos = []
        cantidad = filtro.filter(inventariomaderables__clon=obj).values_list('inventariomaderables__cantidad', flat=True)
        numero_famlia_objeto = filtro.filter(inventariomaderables__clon=obj).count()
        if len(cantidad) >=1:
            for x in cantidad:
                datos.append(x*1.4)

            promedio = np.mean(datos)
            mediana = np.median(datos)
            try:
                minimo = min(datos)
            except:
                minimo = 0
            try:
                maximo = max(datos)
            except:
                maximo = 0
            maderables[obj] = [minimo, maximo, promedio,mediana,numero_famlia_objeto]

    arboles = {}
    arboles_qs = EspeciesArbolesServicios.objects.all().select_related()
    for obj in arboles_qs:
        datos = []
        cantidad = filtro.filter(inventarioarboles__clon=obj).values_list('inventarioarboles__cantidad', flat=True)
        numero_famlia_objeto = filtro.filter(inventarioarboles__clon=obj).count()
        if len(cantidad) >=1:
            for x in cantidad:
                datos.append(x*1.4)

            promedio = np.mean(datos)
            mediana = np.median(datos)
            try:
                minimo = min(datos)
            except:
                minimo = 0
            try:
                maximo = max(datos)
            except:
                maximo = 0
            arboles[obj] = [minimo, maximo, promedio,mediana,numero_famlia_objeto]

    granos = {}
    granos_qs = GranosBasicos.objects.all().select_related()
    for obj in granos_qs:
        datos = []
        cantidad = filtro.filter(inventariogranosbasicos__clon=obj).values_list('inventariogranosbasicos__cantidad', flat=True)
        numero_famlia_objeto = filtro.filter(inventariogranosbasicos__clon=obj).count()
        if len(cantidad) >=1:
            for x in cantidad:
                datos.append(x*1.4)

            promedio = np.mean(datos)
            mediana = np.median(datos)
            try:
                minimo = min(datos)
            except:
                minimo = 0
            try:
                maximo = max(datos)
            except:
                maximo = 0
            granos[obj] = [minimo, maximo, promedio,mediana,numero_famlia_objeto]

    return render(request, template, {'cacao': cacao,
                                                                'cafe':cafe,
                                                                'musaceas':musaceas,
                                                                'frutales':frutales,
                                                                'maderables': maderables,
                                                                'arboles': arboles,
                                                                'granos': granos,
                                                                'numero_total_parcela': numero_total_parcela,
                                                                'num_familias': filtro.count()})

def costos(request, template='rentabilidad/costo_parcelas.html'):
    filtro = _queryset_filtrado(request)

    numero_total_parcela = filtro.filter(preguntas3__area__gte=0.1).count()
    areas_parcelas = filtro.filter(preguntas3__area__gte=0.1).values_list('preguntas3__area_ha', flat=True)
    promedio_areas = sum(areas_parcelas)
    costo_mano_obra = filtro.filter(valormanoobra__valor_usd__gte=1).values_list('valormanoobra__valor_usd', flat=True)
    min_costo = min(costo_mano_obra or [0])
    promedio_costo = np.mean(costo_mano_obra)
    median_costo = np.median(costo_mano_obra)
    max_costo = max(costo_mano_obra or [0])
    #=================================================
    #TODO: al resultado de costos y ingresos multiplicar por 1.4
    #cacao
    cacao_mo = []
    cacao_insumos = []
    cacao_total = []
    cacao_establecimiento_mo = []
    cacao_establecimiento_insumo = []
    cacao_establecimiento_total = []

    cacao_desarrollo_mo = []
    cacao_desarrollo_insumo = []
    cacao_desarrollo_total = []

    cacao_produccion_mo = []
    cacao_produccion_insumo = []
    cacao_produccion_total = []

    vmo = ValorManoObra.objects.select_related()
    ce = CacaoEstablecimiento.objects.select_related()
    cd = CacaoDesarrollo.objects.select_related()
    cp = CacaoProduccion.objects.select_related()
    #cafe
    cafe_mo = []
    cafe_insumos = []
    cafe_total = []
    cafe_establecimiento_mo = []
    cafe_establecimiento_insumo = []
    cafe_establecimiento_total = []

    cafe_desarrollo_mo = []
    cafe_desarrollo_insumo = []
    cafe_desarrollo_total = []

    cafe_produccion_mo = []
    cafe_produccion_insumo = []
    cafe_produccion_total = []

    cf_e = CafeEstablecimiento.objects.select_related()
    cf_d = CafeDesarrollo.objects.select_related()
    cf_p = CafeProduccion.objects.select_related()

    #musaceas
    musaceas_mo = []
    musaceas_insumos = []
    musaceas_total = []
    musaceas_establecimiento_mo = []
    musaceas_establecimiento_insumo = []
    musaceas_establecimiento_total = []

    musaceas_produccion_mo = []
    musaceas_produccion_insumo = []
    musaceas_produccion_total = []
    m_e = MusaceasEstablecimiento.objects.select_related()
    m_p = MusaceasProduccion.objects.select_related()

    #frutales
    frutales_mo = []
    frutales_insumos = []
    frutales_total = []
    frutales_establecimiento_mo = []
    frutales_establecimiento_insumo = []
    frutales_establecimiento_total = []

    frutales_desarrollo_mo = []
    frutales_desarrollo_insumo = []
    frutales_desarrollo_total = []

    frutales_produccion_mo = []
    frutales_produccion_insumo = []
    frutales_produccion_total = []
    f_e = FrutalesEstablecimiento.objects.select_related()
    f_d = FrutalesDesarrollo.objects.select_related()
    f_p = FrutalesProduccion.objects.select_related()

    #maderables
    maderables_mo = []
    maderables_insumos = []
    maderables_total = []
    maderables_establecimiento_mo = []
    maderables_establecimiento_insumo = []
    maderables_establecimiento_total = []

    maderables_desarrollo_mo = []
    maderables_desarrollo_insumo = []
    maderables_desarrollo_total = []

    maderables_produccion_mo = []
    maderables_produccion_insumo = []
    maderables_produccion_total = []

    ma_e = MaderaEstablecimiento.objects.select_related()
    ma_d = MaderaDesarrollo.objects.select_related()
    ma_p = MaderaProduccion.objects.select_related()

    #arboles de servicios
    arboles_mo = []
    arboles_insumos = []
    arboles_total = []
    arboles_establecimiento_mo = []
    arboles_establecimiento_insumo = []
    arboles_establecimiento_total = []

    arboles_desarrollo_mo = []
    arboles_desarrollo_insumo = []
    arboles_desarrollo_total = []

    arboles_produccion_mo = []
    arboles_produccion_insumo = []
    arboles_produccion_total = []

    a_e = ArbolesServicioEstablecimiento.objects.select_related()
    a_d = ArbolesServicioDesarrollo.objects.select_related()
    a_p = ArbolesServicioProduccion.objects.select_related()

    #granos basicos
    granos_mo = []
    granos_insumos = []
    granos_total = []
    granos_produccion_mo = []
    granos_produccion_insumo = []
    granos_produccion_total = []

    gr_p = GranosBasicosProduccion.objects.select_related()

    for obj in filtro:
        valor = vmo.filter(encuesta=obj).aggregate(s=Sum('valor_usd'))["s"]

        cacao_mo_establecimiento = ce.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_cacao_establecimiento = ce.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        cacao_costo_total_establecimiento = ce.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if cacao_mo_establecimiento >= 0.1:
            cacao_establecimiento_mo.append(cacao_mo_establecimiento*1.4)
        if insumo_cacao_establecimiento >= 0.1:
            cacao_establecimiento_insumo.append(insumo_cacao_establecimiento*1.4)
        if cacao_costo_total_establecimiento >= 0.1:
            cacao_establecimiento_total.append(cacao_costo_total_establecimiento*1.4)

        #desarrollo
        cacao_mo_desarrollo = cd.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_cacao_desarrollo = cd.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        cacao_costo_total_desarrollo = cd.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if cacao_mo_desarrollo >= 0.1:
            cacao_desarrollo_mo.append(cacao_mo_desarrollo*1.4)
        if insumo_cacao_desarrollo >= 0.1:
            cacao_desarrollo_insumo.append(insumo_cacao_desarrollo*1.4)
        if cacao_costo_total_desarrollo >= 0.1:
            cacao_desarrollo_total.append(cacao_costo_total_desarrollo*1.4)

        #produccion
        cacao_mo_produccion = cp.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_cacao_produccion = cp.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        cacao_costo_total_produccion = cp.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if cacao_mo_produccion >= 0.1:
            cacao_produccion_mo.append(cacao_mo_produccion*1.4)
        if insumo_cacao_produccion >= 0.1:
            cacao_produccion_insumo.append(insumo_cacao_produccion*1.4)
        if cacao_costo_total_produccion >= 0.1:
            cacao_produccion_total.append(cacao_costo_total_produccion*1.4)

        cacao_mo = [np.median(cacao_establecimiento_mo),
                                    min(cacao_establecimiento_mo or [0]),
                                    max(cacao_establecimiento_mo or [0]),
                                    np.median(cacao_desarrollo_mo),
                                    min(cacao_desarrollo_mo or [0]),
                                    max(cacao_desarrollo_mo or [0]),
                                    np.median(cacao_produccion_mo),
                                    min(cacao_produccion_mo or [0]),
                                    max(cacao_produccion_mo or [0]),]
        cacao_insumos = [np.median(cacao_establecimiento_insumo),
                                        min(cacao_establecimiento_insumo or [0]),
                                        max(cacao_establecimiento_insumo or [0]),
                                        np.median(cacao_desarrollo_insumo),
                                        min(cacao_desarrollo_insumo or [0]),
                                        max(cacao_desarrollo_insumo or [0]),
                                        np.median(cacao_produccion_insumo),
                                        min(cacao_produccion_insumo or [0]),
                                        max(cacao_produccion_insumo or [0])]
        cacao_total = [np.median(cacao_establecimiento_total),
                                min(cacao_establecimiento_total or [0]),
                                max(cacao_establecimiento_total or [0]),
                                np.median(cacao_desarrollo_total),
                                min(cacao_desarrollo_total or [0]),
                                max(cacao_desarrollo_total or [0]),
                                np.median(cacao_produccion_total),
                                min(cacao_produccion_total or [0]),
                                max(cacao_produccion_total or [0])]

        cafe_mo_produccion = cf_e.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_cafe_produccion = cf_e.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        cafe_costo_total_produccion = cf_e.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if cafe_mo_produccion >= 0.1:
            cafe_establecimiento_mo.append(cafe_mo_produccion*1.4)
        if insumo_cafe_produccion >= 0.1:
            cafe_establecimiento_insumo.append(insumo_cafe_produccion*1.4)
        if cafe_costo_total_produccion >= 0.1:
            cafe_establecimiento_total.append(cafe_costo_total_produccion*1.4)

        cafe_mo_desarrollo = cf_d.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_cafe_desarrollo = cf_d.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        cafe_costo_total_desarrollo = cf_d.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if cafe_mo_desarrollo >= 0.1:
            cafe_desarrollo_mo.append(cafe_mo_desarrollo*1.4)
        if insumo_cafe_desarrollo >= 0.1:
            cafe_desarrollo_insumo.append(insumo_cafe_desarrollo*1.4)
        if cafe_costo_total_desarrollo >= 0.1:
            cafe_desarrollo_total.append(cafe_costo_total_desarrollo*1.4)

        cafe_mo_produccion = cf_p.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_cafe_produccion = cf_p.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        cafe_costo_total_produccion = cf_p.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if cafe_mo_produccion >= 0.1:
            cafe_produccion_mo.append(cafe_mo_produccion*1.4)
        if insumo_cafe_produccion >= 0.1:
            cafe_produccion_insumo.append(insumo_cafe_produccion*1.4)
        if cafe_costo_total_produccion >= 0.1:
            cafe_produccion_total.append(cafe_costo_total_produccion*1.4)

        cafe_mo = [np.median(cafe_establecimiento_mo),
                                    min(cafe_establecimiento_mo or [0]),
                                    max(cafe_establecimiento_mo or [0]),
                                    np.median(cafe_desarrollo_mo),
                                    min(cafe_desarrollo_mo or [0]),
                                    max(cafe_desarrollo_mo or [0]),
                                    np.median(cafe_produccion_mo),
                                    min(cafe_produccion_mo or [0]),
                                    max(cafe_produccion_mo or [0]),]
        cafe_insumos = [np.median(cafe_establecimiento_insumo),
                                                    min(cafe_establecimiento_insumo or [0]),
                                                    max(cafe_establecimiento_insumo or [0]),
                                                    np.median(cafe_desarrollo_insumo),
                                                    min(cafe_desarrollo_insumo or [0]),
                                                    max(cafe_desarrollo_insumo or [0]),
                                                    np.median(cafe_produccion_insumo),
                                                    min(cafe_produccion_insumo or [0]),
                                                    max(cafe_produccion_insumo or [0])]
        cafe_total = [np.median(cafe_establecimiento_total),
                                                    min(cafe_establecimiento_total or [0]),
                                                    max(cafe_establecimiento_total or [0]),
                                                    np.median(cafe_desarrollo_total),
                                                    min(cafe_desarrollo_total or [0]),
                                                    max(cafe_desarrollo_total or [0]),
                                                    np.median(cafe_produccion_total),
                                                    min(cafe_produccion_total or [0]),
                                                    max(cafe_produccion_total or [0])]

        musaceas_mo_establecimiento = m_e.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_musaceas_establecimiento = m_e.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        musaceas_costo_total_establecimiento = m_e.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if musaceas_mo_establecimiento >= 0.1:
            musaceas_establecimiento_mo.append(musaceas_mo_establecimiento*1.4)
        if insumo_musaceas_establecimiento >= 0.1:
            musaceas_establecimiento_insumo.append(insumo_musaceas_establecimiento*1.4)
        if musaceas_costo_total_establecimiento >= 0.1:
            musaceas_establecimiento_total.append(musaceas_costo_total_establecimiento*1.4)

        musaceas_mo_produccion = m_p.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_musaceas_produccion = m_p.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        musaceas_costo_total_produccion = m_p.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if musaceas_mo_produccion >= 0.1:
            musaceas_produccion_mo.append(musaceas_mo_produccion*1.4)
        if insumo_musaceas_produccion >= 0.1:
            musaceas_produccion_insumo.append(insumo_musaceas_produccion*1.4)
        if musaceas_costo_total_produccion >= 0.1:
            musaceas_produccion_total.append(musaceas_costo_total_produccion*1.4)

        musaceas_mo = [np.median(musaceas_establecimiento_mo),
                                    min(musaceas_establecimiento_mo or [0]),
                                    max(musaceas_establecimiento_mo or [0]),
                                    np.median(musaceas_produccion_mo),
                                    min(musaceas_produccion_mo or [0]),
                                    max(musaceas_produccion_mo or [0]),]
        musaceas_insumos = [np.median(musaceas_establecimiento_insumo),
                                                min(musaceas_establecimiento_insumo or [0]),
                                                max(musaceas_establecimiento_insumo or [0]),
                                                np.median(musaceas_produccion_insumo),
                                                min(musaceas_produccion_insumo or [0]),
                                                max(musaceas_produccion_insumo or [0])]
        musaceas_total = [np.median(musaceas_establecimiento_total),
                                            min(musaceas_establecimiento_total or [0]),
                                            max(musaceas_establecimiento_total or [0]),
                                            np.median(musaceas_produccion_total),
                                            min(musaceas_produccion_total or [0]),
                                            max(musaceas_produccion_total or [0])]

        frutales_mo_establecimiento = f_e.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_frutales_establecimiento = f_e.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        frutales_costo_total_establecimiento = f_e.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if frutales_mo_establecimiento >= 0.1:
            frutales_establecimiento_mo.append(frutales_mo_establecimiento*1.4)
        if insumo_frutales_establecimiento >= 0.1:
            frutales_establecimiento_insumo.append(insumo_frutales_establecimiento*1.4)
        if frutales_costo_total_establecimiento >= 0.1:
            frutales_establecimiento_total.append(frutales_costo_total_establecimiento*1.4)

        frutales_mo_desarrollo = f_d.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_frutales_desarrollo = f_d.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        frutales_costo_total_desarrollo = f_d.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if frutales_mo_desarrollo >= 0.1:
            frutales_desarrollo_mo.append(frutales_mo_desarrollo*1.4)
        if insumo_frutales_desarrollo >= 0.1:
            frutales_desarrollo_insumo.append(insumo_frutales_desarrollo*1.4)
        if frutales_costo_total_desarrollo >= 0.1:
            frutales_desarrollo_total.append(frutales_costo_total_desarrollo*1.4)

        frutales_mo_produccion = f_p.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_frutales_produccion = f_p.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        frutales_costo_total_produccion = f_p.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if frutales_mo_produccion >= 0.1:
            frutales_produccion_mo.append(frutales_mo_produccion*1.4)
        if insumo_frutales_produccion >= 0.1:
            frutales_produccion_insumo.append(insumo_frutales_produccion*1.4)
        if frutales_costo_total_produccion >= 0.1:
            frutales_produccion_total.append(frutales_costo_total_produccion*1.4)

        frutales_mo = [np.median(frutales_establecimiento_mo),
                                    min(frutales_establecimiento_mo or [0]),
                                    max(frutales_establecimiento_mo or [0]),
                                    np.median(frutales_desarrollo_mo),
                                    min(frutales_desarrollo_mo or [0]),
                                    max(frutales_desarrollo_mo or [0]),
                                    np.median(frutales_produccion_mo),
                                    min(frutales_produccion_mo or [0]),
                                    max(frutales_produccion_mo or [0]),]
        frutales_insumos = [np.median(frutales_establecimiento_insumo),
                                            min(frutales_establecimiento_insumo or [0]),
                                            max(frutales_establecimiento_insumo or [0]),
                                            np.median(frutales_desarrollo_insumo),
                                            min(frutales_desarrollo_insumo or [0]),
                                            max(frutales_desarrollo_insumo or [0]),
                                            np.median(frutales_produccion_insumo),
                                            min(frutales_produccion_insumo or [0]),
                                            max(frutales_produccion_insumo or [0])]
        frutales_total = [np.median(frutales_establecimiento_total),
                                    min(frutales_establecimiento_total or [0]),
                                    max(frutales_establecimiento_total or [0]),
                                    np.median(frutales_desarrollo_total),
                                    min(frutales_desarrollo_total or [0]),
                                    max(frutales_desarrollo_total or [0]),
                                    np.median(frutales_produccion_total),
                                    min(frutales_produccion_total or [0]),
                                    max(frutales_produccion_total or [0])]

        maderable_mo_establecimiento = ma_e.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_maderable_establecimiento = ma_e.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        maderables_costo_total_establecimiento = ma_e.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if maderable_mo_establecimiento >= 0.1:
            maderables_establecimiento_mo.append(maderable_mo_establecimiento*1.4)
        if insumo_maderable_establecimiento >= 0.1:
            maderables_establecimiento_insumo.append(insumo_maderable_establecimiento*1.4)
        if maderables_costo_total_establecimiento >= 0.1:
            maderables_establecimiento_total.append(maderables_costo_total_establecimiento*1.4)

        maderable_mo_desarrollo = ma_d.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_maderable_desarrollo = ma_d.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        maderables_costo_total_desarrollo = ma_d.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if maderable_mo_desarrollo >= 0.1:
            maderables_desarrollo_mo.append(maderable_mo_desarrollo*1.4)
        if insumo_maderable_desarrollo >= 0.1:
            maderables_desarrollo_insumo.append(insumo_maderable_desarrollo*1.4)
        if maderables_costo_total_desarrollo >= 0.1:
            maderables_desarrollo_total.append(maderables_costo_total_desarrollo*1.4)

        maderable_mo_produccion = ma_p.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_maderable_produccion = ma_p.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        maderables_costo_total_produccion = ma_p.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if maderable_mo_produccion >= 0.1:
            maderables_produccion_mo.append(maderable_mo_produccion*1.4)
        if insumo_maderable_produccion >= 0.1:
            maderables_produccion_insumo.append(insumo_maderable_produccion*1.4)
        if maderables_costo_total_produccion >= 0.1:
            maderables_produccion_total.append(maderables_costo_total_produccion*1.4)

        maderables_mo = [np.median(maderables_establecimiento_mo),
                                    min(maderables_establecimiento_mo or [0]),
                                    max(maderables_establecimiento_mo or [0]),
                                    np.median(maderables_desarrollo_mo),
                                    min(maderables_desarrollo_mo or [0]),
                                    max(maderables_desarrollo_mo or [0]),
                                    np.median(maderables_produccion_mo),
                                    min(maderables_produccion_mo or [0]),
                                    max(maderables_produccion_mo or [0]),]
        maderables_insumos = [np.median(maderables_establecimiento_insumo),
                                                    min(maderables_establecimiento_insumo or [0]),
                                                    max(maderables_establecimiento_insumo or [0]),
                                                    np.median(maderables_desarrollo_insumo),
                                                    min(maderables_desarrollo_insumo or [0]),
                                                    max(maderables_desarrollo_insumo or [0]),
                                                    np.median(maderables_produccion_insumo),
                                                    min(maderables_produccion_insumo or [0]),
                                                    max(maderables_produccion_insumo or [0])]
        maderables_total = [np.median(maderables_establecimiento_total),
                                                    min(maderables_establecimiento_total or [0]),
                                                    max(maderables_establecimiento_total or [0]),
                                                    np.median(maderables_desarrollo_total),
                                                    min(maderables_desarrollo_total or [0]),
                                                    max(maderables_desarrollo_total or [0]),
                                                    np.median(maderables_produccion_total),
                                                    min(maderables_produccion_total or [0]),
                                                    max(maderables_produccion_total or [0])]

        arboles_mo_establecimiento = a_e.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_arboles_establecimiento = a_e.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        arboles_costo_total_establecimiento = a_e.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if arboles_mo_establecimiento >= 0.1:
            arboles_establecimiento_mo.append(arboles_mo_establecimiento*1.4)
        if insumo_arboles_establecimiento >= 0.1:
            arboles_establecimiento_insumo.append(insumo_arboles_establecimiento*1.4)
        if arboles_costo_total_establecimiento >= 0.1:
            arboles_establecimiento_total.append(arboles_costo_total_establecimiento*1.4)

        arboles_mo_desarrollo = a_d.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_arboles_desarrollo = a_d.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        arboles_costo_total_desarrollo = a_d.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if arboles_mo_desarrollo >= 0.1:
            arboles_desarrollo_mo.append(arboles_mo_desarrollo*1.4)
        if insumo_arboles_desarrollo >= 0.1:
            arboles_desarrollo_insumo.append(insumo_arboles_desarrollo*1.4)
        if arboles_costo_total_desarrollo >= 0.1:
            arboles_desarrollo_total.append(arboles_costo_total_desarrollo*1.4)

        arboles_mo_produccion = a_p.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_arboles_produccion = a_p.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        arboles_costo_total_produccion = a_p.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if arboles_mo_produccion >= 0.1:
            arboles_produccion_mo.append(arboles_mo_produccion*1.4)
        if insumo_arboles_produccion >= 0.1:
            arboles_produccion_insumo.append(insumo_arboles_produccion*1.4)
        if arboles_costo_total_produccion >= 0.1:
            arboles_produccion_total.append(arboles_costo_total_produccion*1.4)

        arboles_mo = [np.median(arboles_establecimiento_mo),
                                    min(arboles_establecimiento_mo or [0]),
                                    max(arboles_establecimiento_mo or [0]),
                                    np.median(arboles_desarrollo_mo),
                                    min(arboles_desarrollo_mo or [0]),
                                    max(arboles_desarrollo_mo or [0]),
                                    np.median(arboles_produccion_mo),
                                    min(arboles_produccion_mo or [0]),
                                    max(arboles_produccion_mo or [0]),]
        arboles_insumos = [np.median(arboles_establecimiento_insumo),
                                                    min(arboles_establecimiento_insumo or [0]),
                                                    max(arboles_establecimiento_insumo or [0]),
                                                    np.median(arboles_desarrollo_insumo),
                                                    min(arboles_desarrollo_insumo or [0]),
                                                    max(arboles_desarrollo_insumo or [0]),
                                                    np.median(arboles_produccion_insumo),
                                                    min(arboles_produccion_insumo or [0]),
                                                    max(arboles_produccion_insumo or [0])]
        arboles_total = [np.median(arboles_establecimiento_total),
                                                    min(arboles_establecimiento_total or [0]),
                                                    max(arboles_establecimiento_total or [0]),
                                                    np.median(arboles_desarrollo_total),
                                                    min(arboles_desarrollo_total or [0]),
                                                    max(arboles_desarrollo_total or [0]),
                                                    np.median(arboles_produccion_total),
                                                    min(arboles_produccion_total or [0]),
                                                    max(arboles_produccion_total or [0])]

        granos_mo_produccion = gr_p.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor))['costo_mo']
        insumo_granos_produccion = gr_p.filter(encuesta=obj).aggregate(s=Coalesce(Sum('costo_insumo_usd'),V(0)))["s"]
        granos_costo_total_produccion = gr_p.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if granos_mo_produccion >= 0.1:
            granos_produccion_mo.append(granos_mo_produccion*1.4)
        if insumo_granos_produccion >= 0.1:
            granos_produccion_insumo.append(insumo_granos_produccion*1.4)
        if granos_costo_total_produccion >= 0.1:
            granos_produccion_total.append(granos_costo_total_produccion*1.4)

        granos_mo = [np.median(granos_produccion_mo),
                                min(granos_produccion_mo or [0]),
                                max(granos_produccion_mo or [0]),]
        granos_insumos = [np.median(granos_produccion_insumo),
                                                    min(granos_produccion_insumo or [0]),
                                                    max(granos_produccion_insumo or [0])]
        granos_total = [np.median(granos_produccion_total),
                                                    min(granos_produccion_total or [0]),
                                                    max(granos_produccion_total or [0])]



    return render(request, template, {'costo_cacao':cacao_mo,
                                                                'costo_cacao_insumos': cacao_insumos,
                                                                'costo_cacao_insumos_total':cacao_total,
                                                                'costo_cafe':cafe_mo,
                                                                'costo_cafe_insumos':cafe_insumos,
                                                                'costo_cafe_insumos_total':cafe_total,
                                                                'costo_musaceas':musaceas_mo,
                                                                'costo_musaceas_insumos':musaceas_insumos,
                                                                'costo_musaceas_insumos_total':musaceas_total,
                                                                'costo_frutales':frutales_mo,
                                                                'costo_frutales_insumos':frutales_insumos,
                                                                'costo_frutales_insumos_total':frutales_total,
                                                                'costo_maderables':maderables_mo,
                                                                'costo_maderables_insumos':maderables_insumos,
                                                                'costo_maderables_insumos_total':maderables_total,
                                                                'costo_arboles':arboles_mo,
                                                                'costo_arboles_insumos':arboles_insumos,
                                                                'costo_arboles_insumos_total':arboles_total,
                                                                'costo_granos':granos_mo,
                                                                'costo_granos_insumos':granos_insumos,
                                                                'costo_granos_insumos_total':granos_total,
                                                                'numero_total_parcela': numero_total_parcela,
                                                                'areas_parcelas':areas_parcelas,
                                                                'costo_mano_obra': costo_mano_obra,
                                                                'promedio_areas': promedio_areas,
                                                                'min_costo':min_costo,
                                                                'promedio_costo': promedio_costo,
                                                                'median_costo':median_costo,
                                                                'max_costo': max_costo,
                                                                'num_familias': filtro.count()})

def ingresos(request, template='rentabilidad/ingresos_parcelas.html'):
    filtro = _queryset_filtrado(request)
    numero_total_parcela = filtro.filter(preguntas3__area__gte=0.1).count()
    areas_parcelas = filtro.filter(preguntas3__area__gte=0.1).values_list('preguntas3__area_ha', flat=True)
    promedio_areas = sum(areas_parcelas)

    ingresos = OrderedDict()
    frecuencia_cacao = 0
    lista_cacao = []

    frecuencia_cafe = 0
    lista_cafe = []

    frecuencia_musaceas = 0
    lista_musaceas = []

    frecuencia_frutales = 0
    lista_frutales = []

    frecuencia_maderables_D = 0
    lista_maderables_D = []

    frecuencia_maderables = 0
    lista_maderables = []

    frecuencia_arboles = 0
    lista_arboles = []

    frecuencia_granos = 0
    lista_granos = []

    for obj in filtro:
        cacao = CosechaCacao.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        if cacao >=1:
            frecuencia_cacao += 1
            lista_cacao.append(cacao*1.4)
        ingresos['cacao'] = [frecuencia_cacao, np.mean(lista_cacao), np.median(lista_cacao), min(lista_cacao or [0]),max(lista_cacao or [0])]

        cafe = CosechaCafe.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        if cafe >=1:
            frecuencia_cafe += 1
            lista_cafe.append(cafe*1.4)
        ingresos['cafe'] = [frecuencia_cafe, np.mean(lista_cafe), np.median(lista_cafe), min(lista_cafe or [0]),max(lista_cafe or [0])]

        musaceas = CosechaMusaceas.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        if musaceas >=1:
            frecuencia_musaceas += 1
            lista_musaceas.append(musaceas*1.4)
        ingresos['musaceas'] = [frecuencia_musaceas, np.mean(lista_musaceas), np.median(lista_musaceas), min(lista_musaceas or [0]),max(lista_musaceas or [0])]

        frutales = CosechaFrutales.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        if frutales >=1:
            frecuencia_frutales += 1
            lista_frutales.append(frutales*1.4)
        ingresos['frutales'] = [frecuencia_frutales, np.mean(lista_frutales), np.median(lista_frutales), min(lista_frutales or [0]),max(lista_frutales or [0])]

        print "********* ver ingresos maderables :TODO ********"
        maderablesD = CosechaMaderableDesarrollo.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        if maderablesD >= 1:
            frecuencia_maderables_D += 1
            lista_maderables_D.append(maderablesD*1.4)
        maderables = CosechaMaderable.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        ingresos['maderables desarrollo'] = [frecuencia_maderables_D, np.mean(lista_maderables_D), np.median(lista_maderables_D), min(lista_maderables_D or [0]),max(lista_maderables_D or [0])]
        if maderables >= 1:
            frecuencia_maderables += 1
            lista_maderables.append(maderables*1.4)
        ingresos['maderables produccion'] = [frecuencia_maderables, np.mean(lista_maderables), np.median(lista_maderables), min(lista_maderables or [0]),max(lista_maderables or [0])]

        arboles = CosechaArbolesServicios.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        if arboles >=1:
            frecuencia_arboles += 1
            lista_arboles.append(arboles*1.4)
        ingresos['arboles de servicios'] = [frecuencia_arboles, np.mean(lista_arboles), np.median(lista_arboles), min(lista_arboles or [0]),max(lista_arboles or [0])]

        granos = CosechaGranos.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        if granos >=1:
            frecuencia_granos += 1
            lista_granos.append(granos*1.4)
        ingresos['granos basico'] = [frecuencia_granos, np.mean(lista_granos), np.median(lista_granos), min(lista_granos or [0]),max(lista_granos or [0])]


    return render(request, template, {'numero_total_parcela': numero_total_parcela,
                                                                'areas_parcelas':areas_parcelas,
                                                                'ingresos':ingresos,
                                                                'promedio_areas': promedio_areas,
                                                                'num_familias': filtro.count()})

def modelado(request, template='rentabilidad/modelado_rentabilidad.html'):
    filtro = _queryset_filtrado(request)

    modelado_costo = OrderedDict()
    for obj in DatosCosto.objects.filter(costo_id=request.session['modelo'].id).order_by('id'):
        modelado_costo[obj.anio] = [obj.get_cacao_display(), obj.get_cafe_display(),
                                                            obj.get_musaceas_display(), obj.get_frutales_display(),
                                                            obj.get_maderables_display(), obj.get_arboles_display(),
                                                            obj.get_granos_display()]

    return render(request, template,{'modelado_costo': modelado_costo,
                                                                'num_familias': filtro.count()})


def rentabilidad_saf(request, template='rentabilidad/rentabilidad_saf.html'):
    if request.method == 'POST':
        form = ConsultaFichaForm(request.POST)
        if form.is_valid():
            request.session['fecha'] = form.cleaned_data['fecha']
            request.session['pais'] = form.cleaned_data['pais']
            request.session['modelo'] = form.cleaned_data['modelo']
            request.session['parcela'] = form.cleaned_data['parcela']
            request.session['riego'] = form.cleaned_data['riego']
            request.session['saf'] = form.cleaned_data['saf']
            request.session['valoracion'] = form.cleaned_data['valoracion']
            request.session['manejo'] = form.cleaned_data['manejo']
            request.session['intensidad'] = form.cleaned_data['intensidad']
            request.session['sexo'] = form.cleaned_data['sexo']
            request.session['productor'] = form.cleaned_data['productor']
            centinela = 1
        else:
            centinela = 0
    else:
        form = ConsultaFichaForm()
        centinela = 0

    filtro = _queryset_filtrado(request)

    numero_total_parcela = filtro.filter(preguntas3__area__gte=0.1).count()
    areas_parcelas = filtro.filter(preguntas3__area__gte=0.1).values_list('preguntas3__area_ha', flat=True)
    promedio_areas = sum(areas_parcelas)

    modelado_ingreso = OrderedDict()
    modelado_inversion = OrderedDict()
    modelado_tir = OrderedDict()
    modelado_npv = OrderedDict()
    modelado_sistema = OrderedDict()

    lista_cacao_establecimiento = []
    lista_cacao_desarrollo = []
    lista_cacao_produccion = []

    lista_cafe_establecimiento = []
    lista_cafe_desarrollo = []
    lista_cafe_produccion = []

    lista_musaceas_establecimiento = []
    lista_musaceas_produccion = []

    lista_frutales_establecimiento = []
    lista_frutales_desarrollo = []
    lista_frutales_produccion = []

    lista_maderables_establecimiento = []
    lista_maderables_desarrollo = []
    lista_maderables_produccion = []

    lista_arboles_establecimiento = []
    lista_arboles_desarrollo = []
    lista_arboles_produccion = []

    lista_granos_produccion = []

    #listas de ingresos de los 7 rubros
    lista_cacao_ingresos = []
    lista_cafe_ingresos = []
    lista_musaceas_ingresos = []
    lista_frutales_ingresos = []
    lista_maderables_D_ingresos = [] #maderable desarrollo
    lista_maderables_ingresos = []  #maderables produccion
    lista_arboles_ingresos = []
    lista_granos_ingresos = []

    for obj in filtro:
        #cacao
        valor = ValorManoObra.objects.filter(encuesta=obj).aggregate(s=Avg('valor_usd'))["s"]

        cacao_obj = CosechaCacao.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        if cacao_obj >=1:
            lista_cacao_ingresos.append(cacao_obj*1.4)

        cacao_costo_total_establecimiento = CacaoEstablecimiento.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if cacao_costo_total_establecimiento >= 0.1:
            lista_cacao_establecimiento.append(cacao_costo_total_establecimiento*1.4)

        cacao_costo_total_desarrollo = CacaoDesarrollo.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if cacao_costo_total_desarrollo >= 0.1:
            lista_cacao_desarrollo.append(cacao_costo_total_desarrollo*1.4)

        cacao_costo_total_produccion = CacaoProduccion.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if cacao_costo_total_produccion >= 0.1:
            lista_cacao_produccion.append(cacao_costo_total_produccion*1.4)
        #cafe
        cafe_obj = CosechaCafe.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        if cafe_obj >=1:
            lista_cafe_ingresos.append(cafe_obj*1.4)
        cafe_costo_total_establecimiento = CafeEstablecimiento.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if cafe_costo_total_establecimiento >= 0.1:
            lista_cafe_establecimiento.append(cafe_costo_total_establecimiento*1.4)

        cafe_costo_total_desarrollo = CafeDesarrollo.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if cafe_costo_total_desarrollo >= 0.1:
            lista_cafe_desarrollo.append(cafe_costo_total_desarrollo*1.4)

        cafe_costo_total_produccion = CafeProduccion.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if cafe_costo_total_produccion >= 0.1:
            lista_cafe_produccion.append(cafe_costo_total_produccion*1.4)
        #musaceas
        musaceas_obj = CosechaMusaceas.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        if musaceas_obj >=1:
            lista_musaceas_ingresos.append(musaceas_obj*1.4)

        musaceas_costo_total_establecimiento = MusaceasEstablecimiento.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if musaceas_costo_total_establecimiento >= 0.1:
            lista_musaceas_establecimiento.append(musaceas_costo_total_establecimiento*1.4)
        musaceas_costo_total_produccion = MusaceasProduccion.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if musaceas_costo_total_produccion >= 0.1:
            lista_musaceas_produccion.append(musaceas_costo_total_produccion*1.4)
        #frutales
        frutales_obj = CosechaFrutales.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        if frutales_obj >=1:
            lista_frutales_ingresos.append(frutales_obj*1.4)

        frutales_costo_total_establecimiento = FrutalesEstablecimiento.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if frutales_costo_total_establecimiento >= 0.1:
            lista_frutales_establecimiento.append(frutales_costo_total_establecimiento*1.4)
        frutales_costo_total_desarrollo = FrutalesDesarrollo.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if frutales_costo_total_desarrollo >= 0.1:
            lista_frutales_desarrollo.append(frutales_costo_total_desarrollo*1.4)
        frutales_costo_total_produccion = FrutalesProduccion.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if frutales_costo_total_produccion >= 0.1:
            lista_frutales_produccion.append(frutales_costo_total_produccion*1.4)
        #maderables
        maderables_obj = CosechaMaderable.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        maderables_obj_D = CosechaMaderableDesarrollo.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        if maderables_obj_D >=1:
            print "***** :TODO lista maderable desarrollo****"
            lista_maderables_D_ingresos.append(maderables_obj_D*1.4)
        if maderables_obj >=1:
            lista_maderables_ingresos.append(maderables_obj*1.4)

        maderables_costo_total_establecimiento = MaderaEstablecimiento.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if maderables_costo_total_establecimiento >= 0.1:
            lista_maderables_establecimiento.append(maderables_costo_total_establecimiento*1.4)
        maderables_costo_total_desarrollo = MaderaDesarrollo.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if maderables_costo_total_desarrollo >= 0.1:
            lista_maderables_desarrollo.append(maderables_costo_total_desarrollo*1.4)
        maderables_costo_total_produccion = MaderaProduccion.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if maderables_costo_total_produccion >= 0.1:
            lista_maderables_produccion.append(maderables_costo_total_produccion*1.4)
        #arboles
        arboles_obj = CosechaArbolesServicios.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        if arboles_obj >=1:
            lista_arboles_ingresos.append(arboles_obj*1.4)
        arboles_costo_total_establecimiento = ArbolesServicioEstablecimiento.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if arboles_costo_total_establecimiento >= 0.1:
            lista_arboles_establecimiento.append(arboles_costo_total_establecimiento*1.4)
        arboles_costo_total_desarrollo = ArbolesServicioDesarrollo.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if arboles_costo_total_desarrollo >= 0.1:
            lista_arboles_desarrollo.append(arboles_costo_total_desarrollo*1.4)
        arboles_costo_total_produccion = ArbolesServicioProduccion.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if arboles_costo_total_produccion >= 0.1:
            lista_arboles_produccion.append(arboles_costo_total_produccion*1.4)
        #granos basicos
        granos_obj = CosechaGranos.objects.filter(encuesta=obj).aggregate(suma=Sum('ingreso'))['suma']
        if granos_obj >=1:
            lista_granos_ingresos.append(granos_obj*1.4)
        granos_costo_total_produccion = GranosBasicosProduccion.objects.filter(encuesta=obj) \
                                            .annotate(costo=Sum(F('mo_familiar_usd') + F('mo_contratada_usd'))) \
                                            .aggregate(costo_mo=Sum(F('costo') * valor + F('costo_insumo_usd')))['costo_mo']
        if granos_costo_total_produccion >= 0.1:
            lista_granos_produccion.append(granos_costo_total_produccion*1.4)


    for obj in DatosCosto.objects.filter(costo_id=request.session['modelo'].id).order_by('id'):
        #cacao
        if obj.cacao == 1:# establecimiento
            cacao_v = 0 - np.median(lista_cacao_establecimiento)
            if np.isnan(cacao_v):
                cacao = 0
            else:
                cacao = 0 - np.median(lista_cacao_establecimiento)
            inversion_cacao_v = np.median(lista_cacao_establecimiento)
            if np.isnan(inversion_cacao_v):
                inversion_cacao = 0
            else:
                inversion_cacao = np.median(lista_cacao_establecimiento)
        if obj.cacao == 2:# desarrollo
            cacao_v = 0 - np.median(lista_cacao_desarrollo)
            if np.isnan(cacao_v):
                cacao = 0
            else:
                 cacao = 0 - np.median(lista_cacao_desarrollo)
            inversion_cacao_v = np.median(lista_cacao_desarrollo)
            if np.isnan(inversion_cacao_v):
                inversion_cacao = 0
            else:
                inversion_cacao = np.median(lista_cacao_desarrollo)
        if obj.cacao == 3:#produccion
            ingreso_cacao = np.median(list(lista_cacao_ingresos))
            if np.isnan(ingreso_cacao):
                cacao = 0
                inversion_cacao = 0
            else:
                cacao = ingreso_cacao - np.median(lista_cacao_produccion)
                inversion_cacao = np.median(lista_cacao_produccion)
        if obj.cacao == 4:#no hay
            cacao = 0
            inversion_cacao = 0
        #cafe
        if obj.cafe == 1:
            cafe_v = 0 - np.median(lista_cafe_establecimiento)
            if np.isnan(cafe_v):
                cafe = 0
            else:
                cafe = 0 - np.median(lista_cafe_establecimiento)
            inversion_cafe_v = np.median(lista_cafe_establecimiento)
            if np.isnan(inversion_cafe_v):
                inversion_cafe = 0
            else:
                inversion_cafe = np.median(lista_cafe_establecimiento)
        if obj.cafe == 2:
            cafe_v = 0 - np.median(lista_cafe_desarrollo)
            if np.isnan(cafe_v):
                cafe = 0
            else:
                cafe = 0 - np.median(lista_cafe_desarrollo)
            inversion_cafe_v = np.median(lista_cafe_desarrollo)
            if np.isnan(inversion_cafe_v):
                inversion_cafe = 0
            else:
                inversion_cafe = np.median(lista_cafe_desarrollo)
        if obj.cafe == 3:
            ingreso_cafe = np.median(list(lista_cafe_ingresos))
            if np.isnan(ingreso_cafe):
                cafe = 0
                inversion_cafe = 0
            else:
                cafe = ingreso_cafe - np.median(lista_cafe_produccion)
                inversion_cafe = np.median(lista_cafe_produccion)
        if obj.cafe == 4:
            cafe = 0
            inversion_cafe = 0
        #musaceas
        if obj.musaceas == 1:
            musaceas_v = 0 - np.median(lista_musaceas_establecimiento)
            if np.isnan(musaceas_v):
                musaceas = 0
            else:
                musaceas = 0 - np.median(lista_musaceas_establecimiento)
            inversion_musaceas_v = np.median(lista_musaceas_establecimiento)
            if np.isnan(inversion_musaceas_v):
                inversion_musaceas = 0
            else:
                inversion_musaceas = np.median(lista_musaceas_establecimiento)
        if obj.musaceas == 2:
            musaceas = 0
            inversion_musaceas = 0
        if obj.musaceas == 3:
            ingreso_musaceas = np.median(list(lista_musaceas_ingresos))
            if np.isnan(ingreso_musaceas):
                musaceas = 0
                inversion_musaceas = 0
            else:
                musaceas = ingreso_musaceas - np.median(lista_musaceas_produccion)
                inversion_musaceas = np.median(lista_musaceas_produccion)
        if obj.musaceas == 4:
            musaceas = 0
            inversion_musaceas = 0
        #frutales
        if obj.frutales == 1:
            frutales_v = 0 - np.median(lista_frutales_establecimiento)
            if np.isnan(frutales_v):
                frutales = 0
            else:
                frutales = 0 - np.median(lista_frutales_establecimiento)
            inversion_frutales_v = np.median(lista_frutales_establecimiento)
            if np.isnan(inversion_frutales_v):
                inversion_frutales = 0
            else:
                inversion_frutales = np.median(lista_frutales_establecimiento)
        if obj.frutales == 2:
            frutales_v = 0 - np.median(lista_frutales_desarrollo)
            if np.isnan(frutales_v):
                frutales = 0
            else:
                frutales = 0 - np.median(lista_frutales_desarrollo)
            inversion_frutales_v = np.median(lista_frutales_desarrollo)
            if np.isnan(inversion_frutales_v):
                inversion_frutales = 0
            else:
                inversion_frutales = np.median(lista_frutales_desarrollo)
        if obj.frutales == 3:
            ingreso_frutales = np.median(list(lista_frutales_ingresos))
            if np.isnan(ingreso_frutales):
                frutales = 0
                inversion_frutales = 0
            else:
                frutales = ingreso_frutales - np.median(lista_frutales_produccion)
                inversion_frutales = np.median(lista_frutales_produccion)
        if obj.frutales == 4:
            frutales = 0
            inversion_frutales = 0
        #maderables
        if obj.maderables == 1:
            maderables_v = 0 - np.median(lista_maderables_establecimiento)
            if np.isnan(maderables_v):
                maderables = 0
            else:
                maderables = 0 - np.median(lista_maderables_establecimiento)
            inversion_maderables_v = np.median(lista_maderables_establecimiento)
            if np.isnan(inversion_maderables_v):
                inversion_maderables = 0
            else:
                inversion_maderables = np.median(lista_maderables_establecimiento)
        if obj.maderables == 2:
            # :TODO
            ingreso_maderables_D = np.median(list(lista_maderables_D_ingresos))
            #maderables_v = 0 - np.median(lista_maderables_desarrollo)
            if np.isnan(ingreso_maderables_D):
                maderables = 0 - np.median(lista_maderables_desarrollo)
                if np.isnan(maderables): 
                    maderables = 0
            else:
                maderables = ingreso_maderables_D - np.median(lista_maderables_desarrollo)
            inversion_maderables_v = np.median(lista_maderables_desarrollo)
            if np.isnan(inversion_maderables_v):
                inversion_maderables = 0
            else:
                inversion_maderables = np.median(lista_maderables_desarrollo)
        if obj.maderables == 3:
            ingreso_maderables = np.median(list(lista_maderables_ingresos))
            if np.isnan(ingreso_maderables):
                maderables = 0
                inversion_maderables = 0
            else:
                maderables = ingreso_maderables - np.median(lista_maderables_produccion)
                inversion_maderables = np.median(lista_maderables_produccion)
        if obj.maderables == 4:
            maderables = 0
            inversion_maderables = 0
        #arboles
        if obj.arboles == 1:
            arboles_v = 0 - np.median(lista_arboles_establecimiento)
            if np.isnan(arboles_v):
                arboles = 0
            else:
                arboles = 0 - np.median(lista_arboles_establecimiento)
            inversion_arboles_v = np.median(lista_arboles_establecimiento)
            if np.isnan(inversion_arboles_v):
                inversion_arboles = 0
            else:
                inversion_arboles = np.median(lista_arboles_establecimiento)
        if obj.arboles == 2:
            arboles_v = 0 - np.median(lista_arboles_desarrollo)
            if np.isnan(arboles_v):
                arboles = 0
            else:
                arboles = 0 - np.median(lista_arboles_desarrollo)
            inversion_arboles_v = np.median(lista_arboles_desarrollo)
            if np.isnan(inversion_arboles_v):
                inversion_arboles = 0
            else:
                inversion_arboles = np.median(lista_arboles_desarrollo)
        if obj.arboles == 3:
            ingreso_arboles = np.median(list(lista_arboles_ingresos))
            if np.isnan(ingreso_arboles):
                arboles = 0
                inversion_arboles = 0
            else:
                arboles = ingreso_arboles - np.median(lista_arboles_produccion)
                inversion_arboles = np.median(lista_arboles_produccion)
        if obj.arboles == 4:
            arboles = 0
            inversion_arboles = 0
        #granos
        if obj.granos == 1:
            granos = 0
            inversion_granos = 0
        if obj.granos == 2:
            granos = 0
            inversion_granos = 0
        if obj.granos == 3:
            ingreso_granos = np.median(list(lista_granos_ingresos))
            if np.isnan(ingreso_granos):
                granos = 0
                inversion_granos = 0
            else:
                granos = ingreso_granos - np.median(lista_granos_produccion)
                inversion_granos = np.median(lista_granos_produccion)
        if obj.granos == 4:
            granos = 0
            inversion_granos = 0

        ingreso_total = cacao + cafe + musaceas + frutales + maderables + arboles + granos
        modelado_ingreso[obj.anio] = [cacao,cafe,musaceas,frutales,maderables,arboles,granos,ingreso_total]

        inversion_total = inversion_cacao + inversion_cafe + inversion_musaceas + inversion_frutales + inversion_maderables + inversion_arboles + inversion_granos
        modelado_inversion[obj.anio] = [inversion_cacao, inversion_cafe,inversion_musaceas,inversion_frutales,inversion_maderables,inversion_arboles,inversion_granos,inversion_total]


        #sistema_total = inversion_total + ingreso_total + tir_total
        #modelado_sistema[obj.anio] = [inversion_total,ingreso_total,tir_total,sistema_total]

    tir_cacao = []
    tir_cafe =[]
    tir_musaceas =[]
    tir_frutales = []
    tir_maderables = []
    tir_arboles = []
    tir_granos = []
    tir_total = []
    for k,v in modelado_ingreso.items():
        try:
            tir_cacao.append(int(v[0]))
        except:
            tir_cacao.append(0)
        try:
            tir_cafe.append(int(v[1]))
        except:
            tir_cafe.append(0)
        try:
            tir_musaceas.append(int(v[2]))
        except:
            tir_musaceas.append(0)
        try:
            tir_frutales.append(int(v[3]))
        except:
            tir_frutales.append(0)
        try:
            tir_maderables.append(int(v[4]))
        except:
            tir_maderables.append(0)
        try:
            tir_arboles.append(int(v[5]))
        except:
            tir_arboles.append(0)
        try:
            tir_granos.append(int(v[6]))
        except:
            tir_granos.append(0)
        try:
            tir_total.append(int(v[7]))
        except:
            tir_total.append(0)

    modelado_tir['Cacao'] = np.irr(tir_cacao) * 100
    modelado_tir['Cafe'] = np.irr(tir_cafe) * 100
    modelado_tir['Musaceas'] = np.irr(tir_musaceas) * 100
    modelado_tir['Frutales'] = np.irr(tir_frutales) * 100
    modelado_tir['Maderables'] = np.irr(tir_maderables) * 100
    modelado_tir['Arboles Servicios'] = np.irr(tir_arboles) * 100
    modelado_tir['Granos Basicos'] = np.irr(tir_granos) * 100
    modelado_tir['TIR Sistema'] = np.irr(tir_total) * 100

    modelado_npv['Cacao'] = np.npv(0.1, tir_cacao)
    modelado_npv['Cafe'] = np.npv(0.1, tir_cafe)
    modelado_npv['Musaceas'] = np.npv(0.1, tir_musaceas)
    modelado_npv['Frutales'] = np.npv(0.1, tir_frutales)
    modelado_npv['Maderables'] = np.npv(0.1, tir_maderables)
    modelado_npv['Arboles Servicios'] = np.npv(0.1, tir_arboles)
    modelado_npv['Granos Basicos'] = np.npv(0.1, tir_granos)
    modelado_npv['VAN Sistema'] = np.npv(0.1, tir_total)

    return render(request, template,{'modelado_ingreso': modelado_ingreso,
                                                              'modelado_inversion': modelado_inversion,
                                                              'modelado_tir': modelado_tir,
                                                              'modelado_sistema': modelado_npv,
                                                              'numero_total_parcela':numero_total_parcela,
                                                              'promedio_areas': promedio_areas,
                                                              'form':form,
                                                              'num_familias': filtro.count()})


def get_productor(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        if request.user.is_superuser:
            personas = Entrevistados.objects.filter(nombre__icontains = q )[:10]
        else:
            personas = Entrevistados.objects.filter(nombre__icontains = q, user=request.user)[:10]
        #print personas
        results = []
        for person in personas:
            personas_json = {}
            personas_json['id'] = person.id
            personas_json['label'] = person.nombre
            personas_json['value'] = person.nombre
            results.append(personas_json)
    else:
        results = 'fail'
    return HttpResponse(simplejson.dumps(results), content_type='application/json')

def load_departamentos(request):
    pais_id = request.GET.get('pais')
    lugares = Departamento.objects.filter(pais_id=pais_id).order_by('nombre')
    return render(request, 'ficha/lugares_dropdown_list_options.html', {'lugares': lugares})

def load_municipios(request):
    departamento_id = request.GET.get('departamento')
    lugares = Municipio.objects.filter(departamento_id=departamento_id).order_by('nombre')
    return render(request, 'ficha/lugares_dropdown_list_options.html', {'lugares': lugares})

def load_comunidades(request):
    municipio_id = request.GET.get('municipio')
    lugares = Comunidad.objects.filter(municipio_id=municipio_id).order_by('nombre')
    return render(request, 'ficha/lugares_dropdown_list_options.html', {'lugares': lugares})


def get_view(request, vista):
    if vista in VALID_VIEWS:
        return VALID_VIEWS[vista](request)
    else:
        raise ViewDoesNotExist("Tried %s in module %s Error: View not defined in VALID_VIEWS." % (vista, 'lineabase.views'))

VALID_VIEWS = {
        'descripcion': descripcion_muestra,
        'parcela': disenio_parcelas,
        'costos': costos,
        'ingresos': ingresos,
        'modelado': modelado,
        'rentabilidad': rentabilidad_saf,
        'mapa_rentabilidad': mapa_rentabilidad,
    }
