# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class Cuadernosmes0Config(AppConfig):
    name = 'cuadernosmes0'
    verbose_name = "Cuadernos mes 0"
