# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.contrib.auth.models import User
from .choices_static import *
from lineabase.models import (Entrevistados,
                                                         Encuestadores,
                                                         Organizacion)

from multiselectfield import MultiSelectField
from djmoney.money import Money
from djmoney.contrib.exchange.models import convert_money
# Create your models here.

@python_2_unicode_compatible
class Ciclo(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Ciclos"

@python_2_unicode_compatible
class Years(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Años"


@python_2_unicode_compatible
class CuadernoMesCero(models.Model):
    year = models.ForeignKey(Years, on_delete=models.CASCADE, verbose_name='Año')
    fecha = models.DateField()
    ciclo = models.ForeignKey(Ciclo, on_delete=models.CASCADE)
    entrevistado = models.ForeignKey(Entrevistados,
                                                                   on_delete=models.CASCADE,
                                                                   verbose_name='Nombre productor/a')
    encuestador = models.ForeignKey(Encuestadores,
                                                                   on_delete=models.CASCADE,
                                                                   verbose_name='Nombre del técnico')
    # organizacion = models.ForeignKey(Organizacion, on_delete=models.CASCADE,
    #                                                                 verbose_name='Nombre de la organización',
    #                                                                 null=True, blank=True)
    area = models.FloatField('Área de la parcela')
    unidad = models.IntegerField(choices=CHOICES_UNIDADES)
    sistema_agro = MultiSelectField('Tipo de sistema agroforestal (SC)',
                                choices=CHOICE_TIPO_SISTEMA_AGRO, null=True)
    #Datos de siembra
    surco_cacao = models.FloatField('Número de surco/hilera cacao')
    surco_platano = models.FloatField('Número de surco/hilera de plátano')
    surco_frutales = models.FloatField('Número de surco/hilera de frutales')
    surco_sombra = models.FloatField('Número de surco/hilera de Árboles de servicio')
    surco_maderable = models.FloatField('Número de surco/hilera de maderables')
    orientacion = models.IntegerField(choices=CHOICE_ORIENTACION)
    #croquis
    croquis = models.ImageField(upload_to='croquis/', null=True, blank=True)

    years = models.IntegerField(editable=False, verbose_name='Año')
    area_ha = models.FloatField(editable=False, null=True, blank=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.years = self.fecha.year
        if self.unidad == 1:
            self.area_ha = self.area / 1.4
        if self.unidad == 2:
            self.area_ha = self.area / 1
        if self.unidad == 3:
            self.area_ha = self.area / 16
        if self.unidad == 4:
            self.area_ha = float(self.area) / 10000

        super(CuadernoMesCero, self).save(*args, **kwargs)

    def __str__(self):
        return self.entrevistado.nombre

    class Meta:
        verbose_name = 'Cuaderno Mes 0 año 1'
        verbose_name_plural = 'Cuadernos de los meses 0 año 1'

@python_2_unicode_compatible
class ClonesVariedades(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Cacao Clones o Variedad'
        ordering = ['nombre']

class Cacao(models.Model):
    cuadenor_cero = models.ForeignKey(CuadernoMesCero,
                                                                        on_delete=models.CASCADE)
    fecha = models.DateField('fecha de siembra')

    class Meta:
        verbose_name_plural = 'Cacao'

class CacaoMatriz(models.Model):
    cacao_parent = models.ForeignKey(Cacao, on_delete=models.CASCADE)
    clones_variedad = models.ForeignKey(ClonesVariedades, on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    altura = models.FloatField('Altura de platas en mt')
    distancia_surco = models.FloatField('Distancia surcos en mt')
    distancia_planta = models.FloatField('Distancia plantas en mt')
    arreglo = models.IntegerField(choices=CHOICE_ARREGLO)

    class Meta:
        verbose_name_plural = 'Cacao'

# musaceas
@python_2_unicode_compatible
class TiposMusaceas(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Tipo de musaceas'
        ordering = ['nombre']

class Musaceas(models.Model):
    cuadenor_cero = models.ForeignKey(CuadernoMesCero,
                                                                        on_delete=models.CASCADE)
    fecha = models.DateField('fecha de siembra')

    class Meta:
        verbose_name_plural = 'Musaceas'

class MusaceasMatriz(models.Model):
    musacea_parent = models.ForeignKey(Musaceas, on_delete=models.CASCADE)
    tipo_musacea = models.ForeignKey(TiposMusaceas, on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    altura = models.FloatField('Altura de platas en mt')
    distancia_surco = models.FloatField('Distancia surcos en mt')
    distancia_planta = models.FloatField('Distancia plantas en mt')
    arreglo = models.IntegerField(choices=CHOICE_ARREGLO)

    class Meta:
        verbose_name_plural = 'Musaceas'

#Frutales
@python_2_unicode_compatible
class EspeciesFrutales(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Especies frutales'
        ordering = ['nombre']

class Frutales(models.Model):
    cuadenor_cero = models.ForeignKey(CuadernoMesCero,
                                                                        on_delete=models.CASCADE)
    fecha = models.DateField('fecha de siembra')

    class Meta:
        verbose_name_plural = 'Frutales'

class FrutalesMatriz(models.Model):
    frutales_parent = models.ForeignKey(Frutales, on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesFrutales, on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    altura = models.FloatField('Altura de platas en mt')
    distancia_surco = models.FloatField('Distancia surcos en mt')
    distancia_planta = models.FloatField('Distancia plantas en mt')
    arreglo = models.IntegerField(choices=CHOICE_ARREGLO)

    class Meta:
        verbose_name_plural = 'Frutales'

#maderables
@python_2_unicode_compatible
class EspeciesMaderables(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Especies Maderables'
        ordering = ['nombre']

class Maderables(models.Model):
    cuadenor_cero = models.ForeignKey(CuadernoMesCero,
                                                                        on_delete=models.CASCADE)
    fecha = models.DateField('fecha de siembra')

    class Meta:
        verbose_name_plural = 'Maderables'

class MaderablesMatriz(models.Model):
    maderable_parent = models.ForeignKey(Maderables, on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesMaderables, on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    altura = models.FloatField('Altura de platas en mt')
    distancia_surco = models.FloatField('Distancia surcos en mt')
    distancia_planta = models.FloatField('Distancia plantas en mt')
    arreglo = models.IntegerField(choices=CHOICE_ARREGLO)

    class Meta:
        verbose_name_plural = 'Maderables'

#sombra temporal
@python_2_unicode_compatible
class EspeciesArbolesServicios(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Especies árboles de servicios'
        ordering = ['nombre']

class SombraTemporal(models.Model):
    cuadenor_cero = models.ForeignKey(CuadernoMesCero,
                                                                        on_delete=models.CASCADE)
    fecha = models.DateField('fecha de siembra')

    class Meta:
        verbose_name_plural = 'Árboles de servicios'

class SombraTemporalMatriz(models.Model):
    sombra_parent = models.ForeignKey(SombraTemporal, on_delete=models.CASCADE)
    clones_variedad = models.ForeignKey(EspeciesArbolesServicios, on_delete=models.CASCADE)
    cantidad_sembrada = models.FloatField()
    altura = models.FloatField('Altura de platas en mt')
    distancia_surco = models.FloatField('Distancia surcos en mt')
    distancia_planta = models.FloatField('Distancia plantas en mt')
    arreglo = models.IntegerField(choices=CHOICE_ARREGLO)

    class Meta:
        verbose_name_plural = 'Árboles de servicios'

#Hoyo o huaca para la siembra
class HuacaSiembra(models.Model):
    cuadenor_cero = models.ForeignKey(CuadernoMesCero,
                                                                        on_delete=models.CASCADE)
    tipo_planta = models.IntegerField(choices=CHOICE_TIPO_PLANTA)
    largo = models.FloatField('Largo en cm')
    ancho = models.FloatField('Ancho en cm')
    profundidad = models.FloatField('Profundidad en cm')
    uso_abono = models.IntegerField(choices=CHOICE_SI_NO)
    tipo_abono = MultiSelectField(choices=CHOICE_TIPO_ABONO,
                            verbose_name='Tipo de abono')

    class Meta:
        verbose_name_plural = 'Hoyo o Huaca para la siembra'

#Riego en la parcela
class RiegoParcela(models.Model):
    cuadenor_cero = models.ForeignKey(CuadernoMesCero,
                                                                        on_delete=models.CASCADE)
    planes_riego = models.IntegerField(choices=CHOICE_SI_NO, verbose_name='Hay planes para utilizar riego')
    tipo_riego = models.IntegerField(choices=CHOICE_TIPO_RIEGO, null=True, blank=True, verbose_name='Qué tipo de riego')
    fuente_de_agua = models.IntegerField(choices=CHOICE_FUENTE_AGUA, null=True, blank=True)
    movimiento_de_agua = models.IntegerField(choices=CHOICE_MOVIMIENTO_AGUA, null=True, blank=True)
    plan_de_riego = models.FloatField(null=True, blank=True, verbose_name='Plan de riego (meses)')
    frecuencia_de_riego = models.FloatField(null=True, blank=True, verbose_name='Frecuencia de riego (veces por semana)')
    galones = models.FloatField('Galones por cada riego de la parcela', null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Riego en la parcela'


#ENTRDADAS PARA CUADERNO AÑO 2,3,4 MES O SON INDEPENDIENTE YA QUE NADA QUE VER CON MES CERO AÑO 1

CHOICES_ANOS_2_3_4 = (
        (15, "Año 2"),
        (16, "Año 3"),
        (17, "Año 4"),
    )

@python_2_unicode_compatible
class CuadernoMesDosTres(models.Model):
    year = models.IntegerField(choices=CHOICES_ANOS_2_3_4,verbose_name='Año')
    fecha = models.DateField()
    ciclo = models.ForeignKey(Ciclo, on_delete=models.CASCADE)
    entrevistado = models.ForeignKey(Entrevistados,
                                   on_delete=models.CASCADE,
                                   verbose_name='Nombre productor/a')
    encuestador = models.ForeignKey(Encuestadores,
                                   on_delete=models.CASCADE,
                                   verbose_name='Nombre del técnico')
    area = models.FloatField('Área de la parcela')
    unidad = models.IntegerField(choices=CHOICES_UNIDADES)

    years = models.IntegerField(editable=False, verbose_name='Año')
    area_ha = models.FloatField(editable=False, null=True, blank=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.years = self.fecha.year
        if self.unidad == 1:
            self.area_ha = self.area / 1.4
        if self.unidad == 2:
            self.area_ha = self.area / 1
        if self.unidad == 3:
            self.area_ha = self.area / 16
        if self.unidad == 4:
            self.area_ha = float(self.area) / 10000

        super(CuadernoMesDosTres, self).save(*args, **kwargs)

    def __str__(self):
        return self.entrevistado.nombre

    class Meta:
        verbose_name = 'Cuaderno Mes 0 años 2,3,4'
        verbose_name_plural = 'Cuadernos de los meses 0 años 2,3,4'

class TCacaoPunto1(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                    on_delete=models.CASCADE)
    clones_variedad = models.ForeignKey(ClonesVariedades,
                                      on_delete=models.CASCADE,
                                      related_name='cacao_punto1_mes')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    ramificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena ramificación")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Cacao punto 1"

class TMusaceasPunto1(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                    on_delete=models.CASCADE)
    musaceas = models.ForeignKey(TiposMusaceas,
                                on_delete=models.CASCADE,
                                related_name='musaceas_punto1_mes')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    foliar = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena área foliar y hijos")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Musaceas punto 1"

class TFrutalesPunto1(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                    on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesFrutales,
                                on_delete=models.CASCADE,
                                related_name='frutales_punto1_mes')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena crecimiento")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Frutales punto 1"

class TMaderablesPunto1(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                    on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesMaderables,
                                on_delete=models.CASCADE,
                                related_name='maderable_punto1_mes')
    altura = models.FloatField('Altura en mt')
    grosor = models.FloatField('Grosor (DAP) cm')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buen fuste y  crecimiento")

    class Meta:
        verbose_name_plural = "Maderables punto 1"

class TArbolesPunto1(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                    on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesArbolesServicios,
                                on_delete=models.CASCADE,
                                related_name='arboles_punto1_mes')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buen fuste y  crecimiento")

    class Meta:
        verbose_name_plural = "Arboles de servicio punto 1"


#Estacion punto 2
class TCacaoPunto2(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                  on_delete=models.CASCADE)
    clones_variedad = models.ForeignKey(ClonesVariedades,
                                      on_delete=models.CASCADE,
                                      related_name='cacao_punto2_mes')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    ramificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena ramificación")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Cacao punto 2"

class TMusaceasPunto2(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                    on_delete=models.CASCADE)
    musaceas = models.ForeignKey(TiposMusaceas,
                                on_delete=models.CASCADE,
                                related_name='musaceas_punto2_mes')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    foliar = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena área foliar y hijos")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Musaceas punto 2"

class TFrutalesPunto2(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                  on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesFrutales,
                                on_delete=models.CASCADE,
                                related_name='frutales_punto2_mes')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena crecimiento")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Frutales punto 2"

class TMaderablesPunto2(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                  on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesMaderables,
                                on_delete=models.CASCADE,
                                related_name='maderables_punto2_mes')
    altura = models.FloatField('Altura en mt')
    grosor = models.FloatField('Grosor (DAP) cm')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buen fuste y  crecimiento")

    class Meta:
        verbose_name_plural = "Maderables punto 2"

class TArbolesPunto2(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                  on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesArbolesServicios,
                                on_delete=models.CASCADE,
                                related_name='arboles_punto2_mes')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buen fuste y  crecimiento")

    class Meta:
        verbose_name_plural = "Arboles de servicio punto 2"

#Estacion punto 3
class TCacaoPunto3(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                  on_delete=models.CASCADE)
    clones_variedad = models.ForeignKey(ClonesVariedades,
                                      on_delete=models.CASCADE,
                                      related_name='cacao_punto3_mes')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    ramificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena ramificación")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Cacao punto 3"

class TMusaceasPunto3(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                  on_delete=models.CASCADE)
    musaceas = models.ForeignKey(TiposMusaceas,
                                on_delete=models.CASCADE,
                                related_name='musaceas_punto3_mes')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    foliar = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena área foliar y hijos")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Musaceas punto 3"

class TFrutalesPunto3(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                  on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesFrutales,
                                on_delete=models.CASCADE,
                                related_name='frutales_punto3_mes')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buena crecimiento")
    floracion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Floracion")
    fructificacion = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Fructificación")

    class Meta:
        verbose_name_plural = "Frutales punto 3"

class TMaderablesPunto3(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                  on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesMaderables,
                                on_delete=models.CASCADE,
                                related_name='maderables_punto3_mes')
    altura = models.FloatField('Altura en mt')
    grosor = models.FloatField('Grosor (DAP) cm')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buen fuste y  crecimiento")

    class Meta:
        verbose_name_plural = "Maderables punto 3"

class TArbolesPunto3(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                  on_delete=models.CASCADE)
    especies = models.ForeignKey(EspeciesArbolesServicios,
                                on_delete=models.CASCADE,
                                related_name='arboles_punto3_mes')
    altura = models.FloatField('Altura en mt')
    plaga = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de plaga")
    enfermedad = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Daño de enfermedades")
    nutricional = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Deficiencia nutricional")
    crecimiento = models.IntegerField(choices=CHOICE_SI_NO, verbose_name="Buen fuste y  crecimiento")

    class Meta:
        verbose_name_plural = "Arboles de servicio punto 3"

#plagas y enfermedades del momento
class TPlagasEnfermedades(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                    on_delete=models.CASCADE)
    cacao = MultiSelectField(choices=CHOICE_PLAGA_ENFERMEDAD_CACA0)
    musaceas = MultiSelectField(choices=CHOICE_PLAGA_ENFERMEDAD_MUSACEAS)
    frutales = MultiSelectField(choices=CHOICE_PLAGA_ENFERMEDAD_FRUTALES)
    maderables = MultiSelectField(choices=CHOICE_PLAGA_ENFERMEDAD_MADERABLES)
    arboles_servicios = MultiSelectField(choices=CHOICE_ARBOLES_MADERABLES)

    class Meta:
        verbose_name_plural = 'Plagas y enfermedades principales del momento'

#costo de los 5 productos
class TCostoManoObraDia(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                    on_delete=models.CASCADE)
    valor_mo_dia = models.FloatField('Valor de mano de obra por día')
    valor_mo_dia_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.valor_mo_dia_usd = convert_money(Money(self.valor_mo_dia, str(self.get_unidad_display())), 'USD')
        super(TCostoManoObraDia, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Costo de mano de obra por dia"

#costos
class TCostoCacao(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                    on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_CACAO_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada DP')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(TCostoCacao, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de cacao'

class TCostoMusaceas(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                    on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_MUSACEAS_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(TCostoMusaceas, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de musaceas'

class TCostoFrutales(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                    on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_FRUTALES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(TCostoFrutales, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de frutales'

class TCostoMaderables(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                    on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_MADERABLES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(TCostoMaderables, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de maderables'

class TCostoSombraTemporal(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                    on_delete=models.CASCADE)
    fecha = models.DateField()
    actividades = models.IntegerField(choices=CHOICE_COSTO_SOMBRA_TEMPORAL_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar')
    mo_familiar_usd = models.FloatField(editable=False, null=True, blank=True)
    mo_contratada = models.FloatField('Uso de MO contratada')
    mo_contratada_usd = models.FloatField(editable=False, null=True, blank=True)
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos')
    costo_insumo_usd = models.FloatField(editable=False, null=True, blank=True)
    unidad = models.IntegerField(choices=CHOICE_MONEDA_LOCAL, verbose_name="Moneda")

    def save(self, *args, **kwargs):
        self.mo_familiar_usd = convert_money(Money(self.mo_familiar, str(self.get_unidad_display())), 'USD')
        self.mo_contratada_usd = convert_money(Money(self.mo_contratada, str(self.get_unidad_display())), 'USD')
        self.costo_insumo_usd = convert_money(Money(self.costo_insumo, str(self.get_unidad_display())), 'USD')
        super(TCostoSombraTemporal, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos costos de árboles servicios'

#datos de cosecha
class TDatosCosecha(models.Model):
    cuaderno_mes_dos = models.ForeignKey(CuadernoMesDosTres,
                                    on_delete=models.CASCADE)
    fecha = models.DateField()
    productos = models.IntegerField(choices=CHOICE_PRODUCTOS)
    cantidad = models.FloatField()
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_DATOS)
    precio = models.FloatField('Precio/Unidad')
    precio_usd = models.FloatField(editable=False, null=True, blank=True)
    moneda = models.IntegerField(choices=CHOICE_MONEDA_LOCAL)
    comprador = MultiSelectField('Comprador',
                            choices=CHOICE_COMPRADOR_DATOS)

    cantidad_g = models.FloatField(editable=False, null=True, blank=True)
    precio_usd_g = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.precio_usd = convert_money(Money(self.precio, str(self.get_moneda_display())), 'USD')
        #musaceas-banano id(4) unidad cabeza(2)  1 cabeza= 6 kg banano
        if self.productos == 4 and self.unidad == 2:
            self.cantidad_g = self.cantidad * 6
            self.precio_usd_g = float(self.precio_usd) / 6.0
        #musaceas-platano id(5) unidad cabeza(2)  1 Cabeza = 5.2 kg de plátano
        if self.productos == 5 and self.unidad == 2:
            self.cantidad_g = self.cantidad * 5.2
            self.precio_usd_g = float(self.precio_usd) / 5.2
        #madera-caoba id(10) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 10 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-cortez id(11) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 11 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-roble id(12) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 12 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-melina id(13) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 13 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-granadillo id(14) unidad (9) Conversión 200 pt = 1 m3
        if self.productos == 14 and self.unidad == 9:
            self.cantidad_g = self.cantidad / float(200)
            self.precio_usd_g = float(self.precio_usd) * 200
        #madera-leña id(14) unidad Carga (11) Conversión 1 carga =*0.1 m3
        if self.productos == 15 and self.unidad == 11:
            self.cantidad_g = self.cantidad * 0.1
            self.precio_usd_g = float(self.precio_usd) / 0.1
        super(TDatosCosecha, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos de cosecha'

