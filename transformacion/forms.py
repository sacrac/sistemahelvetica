# -*- coding: utf-8 -*-
from django import forms
#from lookups import ProductorLookup, TecnicoLookup
#import selectable.forms as selectable
from lineabase.models import Organizacion, OrganizacionApoyo
from .models import (FichaPodaSombra, FichaSueloFertilidad,
                                       FichaPisoPlaga,FichaCosecha,
                                       FichaCierre, FichaCosechaSecundaria)
from cuadernosmes0.models import Ciclo
from lugar.models import (Pais, Departamento,
                                                 Municipio, Comunidad)
from dal import autocomplete
#from mapeo.models import Persona, Organizaciones
#from comunicacion.lugar.models import Pais, Departamento, Municipio, Comunidad

class ProductorSombraPodaAdminForm(forms.ModelForm):

    class Meta:
        model = FichaPodaSombra
        fields = ('__all__')
        widgets = {
            'entrevistado': autocomplete.ModelSelect2(url='entrevistados-autocomplete')
        }

class FichaSueloFertilidadAdminForm(forms.ModelForm):

    class Meta:
        model = FichaSueloFertilidad
        fields = ('__all__')
        widgets = {
            'entrevistado': autocomplete.ModelSelect2(url='entrevistados-autocomplete')
        }

class FichaPisoPlagaAdminForm(forms.ModelForm):

    class Meta:
        model = FichaPisoPlaga
        fields = ('__all__')
        widgets = {
            'entrevistado': autocomplete.ModelSelect2(url='entrevistados-autocomplete')
        }

class FichaCosechaAdminForm(forms.ModelForm):

    class Meta:
        model = FichaCosecha
        fields = ('__all__')
        widgets = {
            'entrevistado': autocomplete.ModelSelect2(url='entrevistados-autocomplete')
        }

class FichaCosechaSecundariaAdminForm(forms.ModelForm):

    class Meta:
        model = FichaCosechaSecundaria
        fields = ('__all__')
        widgets = {
            'entrevistado': autocomplete.ModelSelect2(url='entrevistados-autocomplete')
        }

class ProductorCierreAdminForm(forms.ModelForm):

    class Meta:
        model = FichaCierre
        fields = ('__all__')
        widgets = {
            'entrevistado': autocomplete.ModelSelect2(url='entrevistados-autocomplete')
        }

def fecha_choice():
    years = []
    for en in FichaPodaSombra.objects.order_by('fecha_visita').values_list('fecha_visita', flat=True):
        years.append((en.year,en.year))
    for en in FichaSueloFertilidad.objects.order_by('fecha_visita').values_list('fecha_visita', flat=True):
        years.append((en.year,en.year))
    for en in FichaPisoPlaga.objects.order_by('fecha_visita').values_list('fecha_visita', flat=True):
        years.append((en.year,en.year))
    for en in FichaCosecha.objects.order_by('fecha_visita').values_list('fecha_visita', flat=True):
        years.append((en.year,en.year))
    for en in FichaCierre.objects.order_by('fecha_visita').values_list('fecha_visita', flat=True):
        years.append((en.year,en.year))
    for en in FichaCosechaSecundaria.objects.order_by('fecha_visita').values_list('fecha_visita', flat=True):
        years.append((en.year,en.year))
    return list(sorted(set(years)))

def ciclo_choice():
    ciclos = []
    for en in Ciclo.objects.all():
        ciclos.append((en.nombre,en.nombre))
    return list(sorted(set(ciclos)))


CHOICE_SEXO1 = (
    ('', '-------'),
    (1, 'Hombre'),
    (2, 'Mujer')
)

CHOICE_TIPOLOGIA1 = (('', '-------------------'),
                    (1, 'Pequeño campesino de montaña'),
                    (2, 'Pequeño campesino diversificado'),
                    (3, 'Finquero cacaotero'),
                    (4, 'Finquero ganadero cacaotero'),
                    (5, 'Finquero cafetalero'),
                    (6, 'Finquero ganadero cafetalero'),
                    (7, 'Finquero ganadero'),
                )

CHOICE_TAMANO_PARCELA = (
    (1, 'Pequeña (0.1 a 2 mz)'),
    (2, 'Mediana (2.1 a 5 mz)'),
    (3, 'Grande (Más de 5 mz)'),
)

# class ConsultaSombraForm(forms.Form):
#     #fecha = forms.MultipleChoiceField(choices=fecha_choice(), label="Años", required=False)
#     ciclo = forms.MultipleChoiceField(choices=ciclo_choice(), required=False)
#     productor = forms.CharField(max_length=250, required=False)
#     organizacion = forms.ModelChoiceField(queryset=Organizaciones.objects.all(), required=False)
#     pais = forms.ModelChoiceField(queryset=Pais.objects.all(), required=False)
#     departamento = forms.ModelChoiceField(queryset=Departamento.objects.all(), required=False)
#     municipio = forms.ModelChoiceField(queryset=Municipio.objects.all(), required=False)
#     comunidad = forms.ModelChoiceField(queryset=Comunidad.objects.all(), required=False)
#     sexo = forms.ChoiceField(choices=CHOICE_SEXO1, required=False)
#     tipologia = forms.ChoiceField(choices=CHOICE_TIPOLOGIA1, required=False)

class ConsultaTransformacionForm(forms.Form):
    def __init__(self, *args, **kwargs):
          super(ConsultaTransformacionForm, self).__init__(*args, **kwargs)
          self.fields['ciclo'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['organizacion_pertenece'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['organizacion_apoyo'].widget.attrs.update({'class': 'select2 form-control select2-multiple select2-hidden-accessible','data-placeholder':'Escoge....', 'tabindex':'-1','area-hidden': 'true'})
          self.fields['pais'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['productor'].widget.attrs.update({'class': 'form-control'})
          self.fields['departamento'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['municipio'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          #self.fields['comunidad'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['sexo'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['parcela'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above',
                                                                                'data-placeholder':'Escoge....', 'tabindex':'-1','area-hidden': 'true'})

          if 'pais' in self.data:
            try:
                pais_id = int(self.data.get('pais'))
                self.fields['departamento'].queryset = Departamento.objects.filter(pais_id=pais_id).order_by('nombre')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset

          if 'departamento' in self.data:
            try:
                pais_id = int(self.data.get('departamento'))
                self.fields['municipio'].queryset = Municipio.objects.filter(departamento_id=pais_id).order_by('nombre')
            except (ValueError, TypeError):
                pass

    ciclo = forms.ChoiceField(choices=ciclo_choice(), required=False)
    productor = forms.CharField(max_length=250, required=False)
    organizacion_pertenece = forms.ModelChoiceField(queryset=Organizacion.objects.all(), required=False)
    organizacion_apoyo = forms.ModelMultipleChoiceField(queryset=OrganizacionApoyo.objects.all(), required=False)
    pais = forms.ModelChoiceField(queryset=Pais.objects.all(), required=False)
    departamento = forms.ModelChoiceField(queryset=Departamento.objects.none(), required=False)
    municipio = forms.ModelChoiceField(queryset=Municipio.objects.none(), required=False)
    #comunidad = forms.ModelChoiceField(queryset=Comunidad.objects.all(), required=False)
    sexo = forms.ChoiceField(choices=CHOICE_SEXO1, required=False)
    parcela = forms.MultipleChoiceField(choices=CHOICE_TAMANO_PARCELA, required=False)


class FormularioColabora(forms.Form):
    nombre = forms.CharField(max_length=250,required=True)
    correo = forms.EmailField(required=True)
    asunto = forms.CharField(required=True,widget=forms.Textarea)
