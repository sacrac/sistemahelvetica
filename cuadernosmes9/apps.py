# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class Cuadernosmes9Config(AppConfig):
    name = 'cuadernosmes9'
    verbose_name = "Cuadernos mes 9"
